import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryoflabourComponent } from './summaryoflabour.component';

describe('SummaryoflabourComponent', () => {
  let component: SummaryoflabourComponent;
  let fixture: ComponentFixture<SummaryoflabourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryoflabourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryoflabourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
