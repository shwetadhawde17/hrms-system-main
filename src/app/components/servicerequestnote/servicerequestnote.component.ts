import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ComponentsComponent } from '../components.component';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { data, param } from 'jquery';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { async } from '@angular/core/testing';
import { result } from 'lodash';
import { error } from 'console';
@Component({
  selector: 'app-servicerequestnote',
  templateUrl: './servicerequestnote.component.html',
  styleUrls: ['./servicerequestnote.component.scss']
})
export class ServicerequestnoteComponent implements OnInit {
  isActiveTab: number = 0;
  createServiceRequestView: FormGroup;
  createServiceRequest: FormGroup;
  isCreate: number = 0;
  CompanyList: any;
  PlantList: any;
  mlaList: any = [];
  ProjectList: any
  labourList: any = [];
  venderList: any;
  idcActivityID: any;
  ServiceRequestNoteList: any = [];
  updateRow: any;
  deleteRow: any;
  displayedColumns: string[] = [];
  dataSource!: MatTableDataSource<serviceRequestNote>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  submitted: any;
  SRNList: any;
  // isApproved: number = 0;
  // Approved: number = 2;
  constructor(private spinnerService: NgxSpinnerService, private fb: FormBuilder, private commomFunction: ComponentsComponent, private dialog: MatDialog, private commonService: CommonService, private apiCallService: ApiCallService) {
    this.createServiceRequest = this.fb.group({
      SRNID: null,
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      projCode: [null, [Validators.required]],
      SRN: [null],
      bpCode: [null, [Validators.required]],
      srnDate: [null, [Validators.required]],
      isApproved:[null],
      CreateBy: [null],
      CreateOn: [null],
      UpdateBy: [null],
      UpdateOn: [null],
      SrnDetails: new FormArray([this.addLabourGroup()])
    })
    this.createServiceRequestView = this.fb.group({
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      projCode: [null, [Validators.required]]
    })
  }
  // Table View Filter 
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany();
    this.spinnerService.hide();
  }
  //mat Dialogs
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  // labour Array
  get labourDetailsArray(): any {
    return <FormArray>this.createServiceRequest.get('SrnDetails') as FormArray;
  }
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.labourDetailsArray['controls'][i]['controls'][controlName].hasError(errorName)
  }
  private addLabourGroup(): FormGroup {
    return this.fb.group({
      LabourDetailsID: [null, [Validators.required]],
      Description: [null, [Validators.required]],
      Quantity: [null, [Validators.required]],
      UnitCode: [null, [Validators.required]],
      Rate: [null, [Validators.required]],
      Amount: [null, [Validators.required]],
      SequneceNo: 1,
      SRNID: null
    })
  }
  // add array
  addLaborCode() {
    this.labourDetailsArray.push(this.addLabourGroup());
  }
  // delete form array 
  deletearr(i: any) {
    this.labourDetailsArray.removeAt(i);
  }
  // Tab Change active unactive
  tabIsActive(value: any, change: any) {
    if (value != change) {
      this.updateRow = null, value = 0;
      this.createServiceRequest.reset();
      // this.dataSource = new MatTableDataSource();
      //   this.ServiceRequestNoteList = [];
      while (this.labourDetailsArray.length !== 1) {
        this.labourDetailsArray.removeAt(0)
      }
    }
    this.isActiveTab = value;
    this.isCreate = value;
    this.createServiceRequest.enable();
  }
  // Create Service Request Note select company to plant and Labour get 
  async onChangeCmp(cmpCode: any) {
    console.log('OnChangeCompany', cmpCode);
    if (cmpCode) {
      this.PlantList = await this.commomFunction.getPlant(cmpCode);
      this.venderList = await this.commomFunction.getVendor(cmpCode);
    } else {
      this.dataSource = new MatTableDataSource()
      this.ServiceRequestNoteList = [];
      this.createServiceRequest.controls['plantCode'].setValue(null); this.PlantList = [];
      this.createServiceRequest.controls['bpCode'].setValue(null); this.venderList = [];
      // this.createServiceRequest.controls['projCode'].setValue(null); this.ProjectList = [];
      this.createServiceRequestView.controls['plantCode'].setValue(null); this.PlantList = [];
      this.createServiceRequest.controls['projCode'].setValue(null); this.ProjectList = [];
      this.createServiceRequestView.controls['projCode'].setValue(null); this.ProjectList = [];
      // this.createServiceRequest.reset();
    }
  }

  // Create Service Request Note select plant to Project 
  async onChangePlant(cmpCode: any, plantCode: any) {
    if (cmpCode && plantCode) {
      console.log(cmpCode, plantCode);
      this.ProjectList = await this.commomFunction.getproject(cmpCode, plantCode);
      console.log('OnChangeCompany', this.ProjectList);
    }
    else {
      this.createServiceRequest.controls['projCode'].setValue(null); this.ProjectList = [];
      this.createServiceRequestView.controls['projCode'].setValue(null); this.ProjectList = [];
      // this.labourDetailsArray.reset()
      this.dataSource = new MatTableDataSource()
      this.ServiceRequestNoteList = [];
    }
  }

  // Create Service Request Note get Labour ID
  async onChangeProject() {
      this.labourList = await this.commomFunction.getSOMLA(this.createServiceRequest.value.cmpCode, this.createServiceRequest.value.plantCode, this.createServiceRequest.value.projCode, 2);
  }

  // View Service Request Note
  async viewServiceRequestNote() {
    if (this.createServiceRequestView.valid) {
      this.spinnerService.show();
      this.ServiceRequestNoteList = await this.commomFunction.getServiceRequestNote(this.createServiceRequestView.value.cmpCode, this.createServiceRequestView.value.plantCode, this.createServiceRequestView.value.projCode)
      console.log("||||||||||||||||||", this.ServiceRequestNoteList);
      this.displayedColumns = ['action','vendorName','srn'];
      if (this.ServiceRequestNoteList.length > 0) {
        this.dataSource = new MatTableDataSource(this.ServiceRequestNoteList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide();
      } else {
        this.dataSource = new MatTableDataSource();
        this.ServiceRequestNoteList = [];
        this.spinnerService.hide();
      }
    }
  }

  //select Labour Details ID patchvalue other field
  onChangeLabourDetailsID(e: any, i: any) {
    console.log(">>>>>>>>>>>>>>>>", this.labourList);
    let mlaList = this.labourList.filter((item: any) => e == item.mlaid);
        console.log(">>>>>>>>>>>>>>>>",mlaList[0]);
    this.labourDetailsArray['controls'][i].patchValue({
      Description: mlaList[0].description,
      Quantity: mlaList[0].quantity.toLocaleString(),
      Rate: mlaList[0].rate.toLocaleString(),
      UnitCode: mlaList[0].standardrUnitCode,
      Amount: (mlaList[0].rate * mlaList[0].quantity).toLocaleString()
    });
  }

  // add Service Request Note
 async addServicerequest() {
console.log("aaaaaaaaaaaaaaaa",this.createServiceRequest.value)
    this.isActiveTab = 1;
    this.createServiceRequest.patchValue({ SRNID: this.createServiceRequest.value.SRNID,SRN:this.createServiceRequest.value.SRN })
    // Patch CompanyCode plantCode ProjectCode to Floor Array
    this.labourDetailsArray.value.forEach((element: any, index: any) => {
      (<FormGroup>this.labourDetailsArray.at(index)).patchValue({
        cmpCode: this.createServiceRequest.value.cmpCode,
        plantCode: this.createServiceRequest.value.plantCode,
        projCode: this.createServiceRequest.value.projCode,
        bpCode: this.createServiceRequest.value.bpCode,
        SequneceNo: 1,
        SRNID: this.createServiceRequest.value.SRNID,
      })
    })
    if (this.createServiceRequest.invalid) {
      this.submitted = true;
      this.createServiceRequest.markAllAsTouched()
      return;
    }
    let JSON = this.createServiceRequest.value;
    let Api = null;
    let head = {};
    JSON.createBy = environment.createBy; // Loged IN User ID
    JSON.updateBy = environment.updateBy; // Loged IN User ID
    if (this.isCreate == 1) {    
      Api = this.apiCallService.updateServiceRequestNote, head = { srnid: this.updateRow.srnid}
    } 
    //  else if(this.isCreate == 0){
    //   Api = this.apiCallService.updateForApproval;   head = { srnid: this.updateRow.srnid}
    // }
    else {
      Api = this.apiCallService.addServiceRequestNote;
    }
    var data = { data: JSON, head: head }
    console.log("sssssssssssssss", data)
    Api(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");
          this.createServiceRequestView.patchValue({
            cmpCode:this.createServiceRequest.value.cmpCode,
            plantCode:this.createServiceRequest.value.plantCode,
            projCode:this.createServiceRequest.value.projCode
          })
          this.ServiceRequestNoteList = await this.commomFunction.getServiceRequestNote(JSON.cmpCode,JSON.plantCode,JSON.projCode);
          this.displayedColumns = ['action','vendorName','srn'];
          this.tabIsActive(0, 0);
          if (this.ServiceRequestNoteList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.ServiceRequestNoteList);
            console.log(this.dataSource);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }else{
            this.ServiceRequestNoteList= [];
          }
          // this.createServiceRequestView.reset();
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }

  // Send To approver
 async sendApprover(row:any){
    this.updateRow=row
    let JSON = this.createServiceRequest.value;
    let Api = null;
    let head = {};
    JSON.createBy = environment.createBy;  // Loged In User ID
    JSON.updateBy = environment.updateBy;  // Loged In User ID
    Api = this.apiCallService.updateForApproval;  head = { srnid: this.updateRow.srnid}
    var data = {data: JSON, head: head}
  console.log("????????????",data)    
  Api(data).subscribe({
    next: async (result:any)=>{
      if(result){
        this.commonService.showSuccess("Send To Approval Succefully!");
        this.ServiceRequestNoteList = await this.commomFunction.getServiceRequestNote(this.updateRow.cmpCode, this.updateRow.plantCode, this.updateRow.projCode)
        this.tabIsActive(0,0);
        if(this.ServiceRequestNoteList.length>0){
          this.dataSource = new MatTableDataSource(this.ServiceRequestNoteList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      }      
    }, error: (error:any) => {
      this.commonService.showError(error);
    }
  })
  }

  // Edit Service Request Note 
  async editServiceRequestNote(row: any,isEdit='edit') {
    this.updateRow = row;
    if(this.labourDetailsArray.length>0){
      while ( this.labourDetailsArray.length !==0){
      this.labourDetailsArray.removeAt(0)

      }
    }
    console.log("kkkkkkkkkkkkk", this.updateRow)

    // Use add form for update patch values
    this.createServiceRequest.patchValue({
      SRNID: this.updateRow.srnid,
      SRN:this.updateRow.srn,
      cmpCode: this.updateRow.cmpCode,
      plantCode: this.updateRow.plantCode,
      projCode: this.updateRow.projCode,
      bpCode: this.updateRow.bpCode,
      srnDate: this.updateRow.srnDate,
      isApproved:this.updateRow.isApproved
    });
    console.log("kkkkkkkkkkkkk", this.updateRow.srnDetails[0])
    if (this.updateRow.srnDetails.length > 0) {
      this.updateRow.srnDetails.forEach((item: any, index: any) => {
        this.addLaborCode();
        (<FormGroup>this.labourDetailsArray.at(index)).patchValue({
          cmpCode: this.labourDetailsArray.value.cmpCode,
          plantCode: this.labourDetailsArray.value.plantCode,
          projCode: this.updateRow.projCode,
          BpCode: this.labourDetailsArray.value.bpCode,
          LabourDetailsID: item.labourDetailsID,
          Description: item.description,
          Quantity: item.quantity.toLocaleString(),
          UnitCode: item.standardrUnitCode,
          Rate: item.rate.toLocaleString(),
          Amount: (item.quantity * item.rate).toLocaleString(),
          SRNID:item.srnid,
        })
      })
    }
    this.tabIsActive(1, 1);
    if(isEdit =='edit'){this.createServiceRequest.enable()
      // this.isCreate = 1;
    }else{
      this.createServiceRequest.disable()
      this.isCreate = 2
    }

  }
  // Open Dialog Box
  deleteServiceRequestNote(row: any) {
    this.deleteRow = row;
    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  // Delete Service Request Note  
  deleteSRN() {
    var head = { cmpCode:this.deleteRow.cmpCode,srnid:this.deleteRow.srnid}
    var data = {head:head,param:null}
    console.log('dffff',data)
    this.apiCallService.deleteServiceRequestNote(data).subscribe({
      next: async(result:any) => {
        if(result){
          this.commonService.showSuccess("Record Deleted Succefully!");
          this.ServiceRequestNoteList = await this.commomFunction.getServiceRequestNote(this.deleteRow.cmpCode, this.deleteRow.plantCode, this.deleteRow.projCode)
          this.tabIsActive(0,0);
          if( this.ServiceRequestNoteList.length>0){
            this.dataSource = new MatTableDataSource(this.ServiceRequestNoteList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort
          }
        }
      }, error:(error:any)=>{
        this.commonService.showError(error)
      }
    })

   
  }

  addCreateServiceRequestView() {

  }
}
export interface serviceRequestNote {
  action: string,
  vendorName: string,
  srn: string
}
