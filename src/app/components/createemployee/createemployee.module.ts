import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateemployeeRoutingModule } from './createemployee-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateemployeeComponent } from './createemployee.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [CreateemployeeComponent],
  imports: [
    CommonModule,
    FormsModule,
     ReactiveFormsModule,
    CreateemployeeRoutingModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    NgxSpinnerModule,
    
  ]
})
export class CreateemployeeModule { }
