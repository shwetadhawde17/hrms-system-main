import { Component, OnInit, Type, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ComponentsComponent } from '../components.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { JsonToXlsxService } from 'src/app/services/json-to-xlsx.service';

@Component({
  selector: 'app-summaryoflabour',
  templateUrl: './summaryoflabour.component.html',
  styleUrls: ['./summaryoflabour.component.scss']
})
export class SummaryoflabourComponent implements OnInit {
  createSummaryLabour: FormGroup;
  CompanyList: any;
  PlantList: any;
  ProjectList: any;
  masterList: any = []
  displayedColumns: string[] = []
  ISMasterList = [{ Type: 1, name: 'Material' }, { Type: 2, name: 'Labour' }, { Type: 3, name: 'Apply supply' }];
  dataSource!: MatTableDataSource<serviceRequestNote>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private fb: FormBuilder, private commomFunction: ComponentsComponent, private spinnerService: NgxSpinnerService, private JsonToXlsxService: JsonToXlsxService) {
    this.createSummaryLabour = this.fb.group({
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      projCode: [null, [Validators.required]],
      master: [null, [Validators.required]],
    })
  }
  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany()
  }
  // select company get plant 
  async onChangeCmp(cmpCode: any) {
    if (cmpCode) {
      this.PlantList = await this.commomFunction.getPlant(cmpCode);
    }
    else {
      this.createSummaryLabour.controls['plantCode'].setValue(null); this.PlantList = [];
      this.createSummaryLabour.controls['projCode'].setValue(null); this.ProjectList = [];
      this.createSummaryLabour.controls['master'].setValue(null);
      this.masterList = []
      this.dataSource = new MatTableDataSource()
    }
  }
  // select plant get project 
  async onChangePlant(cmpCode: any, plantCode: any) {
    if(cmpCode && plantCode){
      this.ProjectList = await this.commomFunction.getproject(cmpCode, plantCode)
    }else{
      this.createSummaryLabour.controls['projCode'].setValue(null); this.ProjectList = [];
      this.createSummaryLabour.controls['master'].setValue(null);
      this.masterList = [];
      this.dataSource = new MatTableDataSource()
    }
  } 
  
  //  mat table datasource filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  // Master Data Slect Table Data get 
  async viewSummaryMLA() {
    if (this.createSummaryLabour.valid) {
      this.spinnerService.show();
      this.masterList = await this.commomFunction.getSOMLA(this.createSummaryLabour.value.cmpCode, this.createSummaryLabour.value.plantCode, this.createSummaryLabour.value.projCode, this.createSummaryLabour.value.master)
      this.displayedColumns = ['action', 'mlaID', 'description', 'unit', 'rate', 'quantity', 'amount'];
      if (this.masterList.length > 0) {
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.masterList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide();
      } else {
        this.dataSource = new MatTableDataSource();
        this.masterList = []
        this.spinnerService.hide();
      }
    }else{
      this.masterList = [];
      this.dataSource = new MatTableDataSource()
      this.spinnerService.hide();
    }
  }
  // download Excel 
  downloadExcel(row: any) {
    console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<",row)
    let arr = []
    for (var i = 0; i < row.length; i++) {
      arr.push({Sr_No:1+i, mlaID: row[i].mlaid, description: row[i].description, standardrUnitCode: row[i].standardrUnitCode, quantity: row[i].quantity.toLocaleString(), rate: row[i].rate.toLocaleString(), Amount: (row[i].quantity * row[i].rate).toLocaleString()})
    }
    let ColumeName = ['Sr_No','MLAID', 'Description', 'UnitCode', 'Rate', 'Quntity', 'Amount'];
    this.JsonToXlsxService.exportExcelWithoutHead(arr, ColumeName, "Master List")
  }
}

export interface serviceRequestNote {
  mlaID: string,
  description: string,
  unit: string,
  rate: number,
  quantity: number,
  amount: number

}




