import { TestBed } from '@angular/core/testing';

import { JsonToXlsxService } from './json-to-xlsx.service';

describe('JsonToXlsxService', () => {
  let service: JsonToXlsxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JsonToXlsxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
