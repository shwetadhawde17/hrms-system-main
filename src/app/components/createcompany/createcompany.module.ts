import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreatecompanyRoutingModule } from './createcompany-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreatecompanyComponent } from './createcompany.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from 'src/app/Shared/material.module';
@NgModule({
  declarations: [CreatecompanyComponent],
  imports: [
    CommonModule,
     FormsModule, 
     ReactiveFormsModule,
    CreatecompanyRoutingModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    NgxSpinnerModule    
      ]
})
export class CreatecompanyModule { }
