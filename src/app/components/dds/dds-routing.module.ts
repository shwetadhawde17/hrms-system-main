import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DDSComponent } from './dds.component';

const routes: Routes = [{path:'',component:DDSComponent,children:[]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DDSRoutingModule { }
