import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrestartestimationComponent } from './prestartestimation.component';

describe('PrestartestimationComponent', () => {
  let component: PrestartestimationComponent;
  let fixture: ComponentFixture<PrestartestimationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrestartestimationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrestartestimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
