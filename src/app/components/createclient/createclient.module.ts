import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateclientRoutingModule } from './createclient-routing.module';
import { CreateclientComponent } from './createclient.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
@NgModule({
  declarations: [CreateclientComponent],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule,
    CreateclientRoutingModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    NgxSpinnerModule,
  ]
})
export class CreateclientModule { }
