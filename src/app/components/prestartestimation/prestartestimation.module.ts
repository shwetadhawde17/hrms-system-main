import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrestartestimationRoutingModule } from './prestartestimation-routing.module';
import { PrestartestimationComponent } from './prestartestimation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [PrestartestimationComponent],
  imports: [
    CommonModule,
    PrestartestimationRoutingModule,
    FormsModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ]
})
export class PrestartestimationModule { }
