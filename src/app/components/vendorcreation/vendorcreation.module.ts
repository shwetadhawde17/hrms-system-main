import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorcreationRoutingModule } from './vendorcreation-routing.module';
import { VendorcreationComponent } from './vendorcreation.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsRoutingModule } from '../components-routing.module';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [VendorcreationComponent],
  imports: [
    CommonModule,
    VendorcreationRoutingModule,
    MatDialogModule,
    HttpClientModule,
    CommonModule,
    ComponentsRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule,

  ]
})
export class VendorcreationModule { }
