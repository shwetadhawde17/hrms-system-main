import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoquploadComponent } from './boqupload.component';

const routes: Routes = [
  { path: '', component: BoquploadComponent, children: [] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoquploadRoutingModule { }
