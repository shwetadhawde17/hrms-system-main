import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { ComponentsComponent } from '../components.component';
import * as XLSX from 'xlsx';
import { JsonToXlsxService } from 'src/app/services/json-to-xlsx.service';
import { ApiCallService } from 'src/app/services/api-call.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-project-material-labour',
  templateUrl: './project-material-labour.component.html',
  styleUrls: ['./project-material-labour.component.scss']
})
export class ProjectMaterialLabourComponent implements OnInit {
  ProjectMaterialLabour: FormGroup;
  ViewProjectMaterialLabour: FormGroup;
  UploadMaterial: FormGroup;
  isActiveTab: any;
  isCreate: any;
  updateRow: any;
  CompanyList: any;
  PlantList: any;
  ProjectList: any;

  LabourList: any;
  LabourJSON: any = [];
  SortedLabourList: any = [];

  MaterialList: any = [];
  MaterialJSON: any = [];
  SortedMaterialList: any = [];
  ActualMaterialList: any = [];

  ApplySupplyList: any;
  ApplySupplyJSON: any = [];
  SortedApplySypplyList: any = [];

  isdisabled: boolean = true;
  // UnitList: any;
  rateAllocationList: any = [];
  displayedColumns: string[] = [];
  IsMasterList = ['Material', 'Labour', 'Apply Supply']
  dataSource!: MatTableDataSource<rateAllocation>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(FormGroupDirective) myForm: any;
  UnitList: any;

  constructor(private fb: FormBuilder, private commomFunction: ComponentsComponent, private commonService: CommonService, private apiCallService: ApiCallService, private JsonToXlsxService: JsonToXlsxService, private spinnerService: NgxSpinnerService) {

    this.ProjectMaterialLabour = fb.group({
      cmpCode: [null, Validators.required],
      plantCode: [null, Validators.required],
      projCode: [null, Validators.required],
      isMaster: [null, Validators.required],
      FileUpload: [null],
      // material details Array
      material: this.fb.array([]),
      // labour details Array
      labour: this.fb.array([]),
      // applySupply details Array
      applySupply: this.fb.array([])
    })
    this.ViewProjectMaterialLabour = fb.group({
      isMaster: [null, Validators.required],
      cmpCode: [null, Validators.required],
      plantCode: [null, Validators.required],
      projCode: [null, Validators.required]
    })

    this.UploadMaterial = fb.group({
      FileUpload: [null, Validators.required]
    })
  }

  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany();
    this.isdisabled = true;
    this.ProjectMaterialLabour.valueChanges.subscribe(val => {
      // 
      // if (this.ProjectMaterialLabour.controls['yourControlName'].value === 'someValue') {
      //    // 
      // }
    });
    this.ProjectMaterialLabour.controls['material'].valueChanges.subscribe((value) => {
      // 
      // this.onChangeCmp(value)
    });
  }



  // mat table datasource filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {
    if (value != change) { this.updateRow = null, value = 0 }
    this.isActiveTab = value;
    this.isCreate = value;
  }
  // ********************************************************
  //                    Material Master
  // ********************************************************
  get materialArrayError(): any {
    return <FormArray>this.ProjectMaterialLabour.get('material') as FormArray;
  }
  public materialErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.materialArrayError['controls'][i]['controls'][controlName].hasError(errorName)
  }
  // material details form Array function
  materialdetailsGroup(): FormGroup {
    return this.fb.group({
      materialId: [null, Validators.required],
      SubMaterialId: [null, Validators.required],
      actualMaterialId: [null, Validators.required],
      unit: [null, Validators.required],
      rate: [null, Validators.required]
    });
  }
  //get details Form Array
  getmaterialdetails(): FormArray {
    return <FormArray>this.ProjectMaterialLabour.get('material') as FormArray;
  }
  // add material Form Array
  addmaterialdetails() {
    this.getmaterialdetails().push(this.materialdetailsGroup());
  }
  //remove Sub material Form Array
  removeMaterial(j: number) {
    this.getmaterialdetails().removeAt(j);
  }
  // ********************************************************
  //                    Labour Master
  // ********************************************************
  get labourArrayError(): any {
    return <FormArray>this.ProjectMaterialLabour.get('labour') as FormArray;
  }
  public labourErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.labourArrayError['controls'][i]['controls'][controlName].hasError(errorName)
  }
  // labour details form Array function
  labourdetailsGroup(): FormGroup {
    return this.fb.group({
      subLabourId: [null, Validators.required],
      labourId: [null, Validators.required],
      unit: [null, Validators.required],
      rate: [null, Validators.required]
    });
  }
  //get details Form Array
  getlabourdetails(): FormArray {
    return <FormArray>this.ProjectMaterialLabour.get('labour') as FormArray;
  }
  // add labour Form Array
  addlabourdetails() {
    this.getlabourdetails().push(this.labourdetailsGroup());
  }
  //remove Sub labour Form Array
  removelabour(j: number) {
    this.getlabourdetails().removeAt(j);
  }

  // ********************************************************
  //                    Apply Supply Master
  // ********************************************************
  get applysupplyArrayError(): any {
    return <FormArray>this.ProjectMaterialLabour.get('applySupply') as FormArray;
  }
  // public applysupplyErrorArray = (controlName: string, errorName: string, i: any) => {
  //   return this.applysupplyArrayError['controls'][i]['controls'][controlName].hasError(errorName)
  // }
  // applysupply details form Array function
  applysupplydetailsGroup(): FormGroup {
    return this.fb.group({
      applysupplyId: [null, Validators.required],
      applySupplyDetailsID: [null, Validators.required],
      unit: [null, Validators.required],
      rate: [null, [Validators.required, Validators.maxLength(10)]],

    });
  }
  //get details Form Array
  getapplysupplydetails(): FormArray {
    return <FormArray>this.ProjectMaterialLabour.get('applySupply') as FormArray;
  }
  // add applysupply Form Array
  addapplysupplydetails() {
    this.getapplysupplydetails().push(this.applysupplydetailsGroup());
  }
  //remove Sub applysupply Form Array
  removeapplysupply(j: number) {
    this.getapplysupplydetails().removeAt(j);
  }


  AddMaster(event: any) {
    if (event == 'Material') {
      this.addmaterialdetails();
      if (this.getlabourdetails()) {
        while (this.getlabourdetails().length !== 0) {
          this.getlabourdetails().removeAt(0)
        }
      }
      if (this.getapplysupplydetails()) {
        while (this.getapplysupplydetails().length !== 0) {
          this.getapplysupplydetails().removeAt(0)
        }
      }
    } else if (event == 'Labour') {
      this.addlabourdetails();
      if (this.getmaterialdetails()) {
        while (this.getmaterialdetails().length !== 0) {
          this.getmaterialdetails().removeAt(0)
        }
      }
      if (this.getapplysupplydetails()) {
        while (this.getapplysupplydetails().length !== 0) {
          this.getapplysupplydetails().removeAt(0)
        }
      }
    } else if (event == 'Apply Supply') {
      this.addapplysupplydetails();
      if (this.getmaterialdetails()) {
        while (this.getmaterialdetails().length !== 0) {
          this.getmaterialdetails().removeAt(0)
        }
      }
      if (this.getlabourdetails()) {
        while (this.getlabourdetails().length !== 0) {
          this.getlabourdetails().removeAt(0)
        }
      }
    } else {
      if (this.getmaterialdetails()) {
        while (this.getmaterialdetails().length !== 0) {
          this.getmaterialdetails().removeAt(0)
        }
      }
      if (this.getlabourdetails()) {
        while (this.getlabourdetails().length !== 0) {
          this.getlabourdetails().removeAt(0)
        }
      }
      if (this.getapplysupplydetails()) {
        while (this.getapplysupplydetails().length !== 0) {
          this.getapplysupplydetails().removeAt(0)
        }
      }
    }
  }

  async onChangeCompany(cmpCode: any) {
    if (cmpCode) {
      this.PlantList = await this.commomFunction.getPlant(cmpCode);
      this.MaterialList = await this.commomFunction.getMaterial(cmpCode);

      // if (this.MaterialList) { this.SortedMaterialList = await this.commonService.sortMaterial(this.MaterialList); } else { this.SortedMaterialList = [] }
      this.LabourList = await this.commomFunction.getLabour(cmpCode);

      // if (this.LabourList) { this.SortedLabourList = await this.commonService.sortLabour(this.LabourList); } else { this.SortedLabourList = [] }
      this.ApplySupplyList = await this.commomFunction.getApplySupply(cmpCode);

      // if (this.ApplySupplyList) { this.SortedApplySupplyList = await this.commonService.sortApplySupply(this.ApplySupplyList); } else { this.SortedApplySupplyList = [] }
      // this.UnitList = await this.commomFunction.getunitMaster(cmpCode);
    } else {
      this.ProjectMaterialLabour.controls['plantCode'].setValue(null); this.PlantList = [];
      this.ProjectMaterialLabour.controls['projCode'].setValue(null); this.ProjectList = [];
      this.ProjectMaterialLabour.controls['plantCode'].setValue(null); this.LabourList = [];
    }
  }
  async onChangePlant(cmpCode: any, plantCode: any) {
    if (cmpCode && plantCode) {
      this.ProjectList = await this.commomFunction.getproject(cmpCode, plantCode)
    } else {
      this.ProjectMaterialLabour.controls['projCode'].setValue(null); this.ProjectMaterialLabour.controls['projCode'].setValue(null); this.ProjectList = [];
    }
  }
  async onChangeProject(projCode: any) {
    // if (projCode) {
    //   this.ProjectList = await this.commomFunction.getproject(this.ProjectMaterialLabour.value.cmpCode, projCode)
    // } else {
    //   this.ProjectMaterialLabour.controls['projCode'].setValue(null); this.ProjectMaterialLabour.controls['projCode'].setValue(null); this.ProjectList = [];
    // }
  }
  // Material on change events
  async onchangeMaterialGroup(MaterialGroup: any) {
    if (MaterialGroup) {
      this.SortedMaterialList = this.MaterialList.find((item: any) => item.materialID === MaterialGroup);
    } else {
      // this.ProjectMaterialMaterial.controls['unit'].setValue(null); 
      this.SortedMaterialList = [];
    }
  }
  async onchangeSubMaterialGroup(SubMaterialGroup: any) {
    if (SubMaterialGroup) {
      this.ActualMaterialList = this.SortedMaterialList.subMaterial.find((item: any) => item.materialSubID === SubMaterialGroup);
    } else {
      // this.ProjectMaterialMaterial.controls['unit'].setValue(null); 
      this.ActualMaterialList = [];
    }
  }
  async onchangeActualMaterialGroup(materialDetailGroup: any, i: any) {
    if (materialDetailGroup) {
      var ActualMaterialItem = this.ActualMaterialList.materialDetails.find((item: any) => item.materialDetailID === materialDetailGroup);

      this.getmaterialdetails().controls[i].patchValue({ unit: ActualMaterialItem.standardrUnitCode });
    } else {
      // this.ProjectMaterialMaterial.controls['unit'].setValue(null); 
      this.ActualMaterialList = [];
    }
  }
  // Labour on change events
  async onchangeLabourGroup(LabourGroup: any) {
    if (LabourGroup) {
      this.SortedLabourList = this.LabourList.find((item: any) => item.labourID === LabourGroup);
    } else {
      // this.ProjectMaterialLabour.controls['unit'].setValue(null); 
      this.SortedLabourList = [];
    }
  }

  async onchangeLabourSubGroup(SubLabourGroup: any, i: any) {
    if (SubLabourGroup) {
      var dt = this.SortedLabourList.labourDetails.find((item: any) => item.labourDetailsID === SubLabourGroup);

      this.getlabourdetails().controls[i].patchValue({ unit: dt.standardrUnitCode });
    } else {
      // this.ProjectMaterialLabour.controls['unit'].setValue(null); 
      this.SortedLabourList = [];
    }
  }
  // ApplySypply on change events
  async onchangeApplySypplyGroup(ApplySypplyGroup: any) {
    if (ApplySypplyGroup) {

      this.SortedApplySypplyList = this.ApplySupplyList.find((item: any) => item.applySupplyID === ApplySypplyGroup);

    } else {
      // this.ProjectMaterialLabour.controls['unit'].setValue(null); 
      this.SortedLabourList = [];
    }
  }
  async onchangeSubApplySypplyGroup(SubApplySypplyGroup: any, i: any) {
    if (SubApplySypplyGroup) {

      var dt = this.SortedApplySypplyList.applySupplyDetails.find((item: any) => item.applySupplyDetailsID === SubApplySypplyGroup);

      this.getapplysupplydetails().controls[i].patchValue({ unit: dt.standardrUnitCode });
    } else {
      // this.ProjectMaterialLabour.controls['unit'].setValue(null); 
      this.SortedLabourList = [];
    }
  }

  ViewProjectMaterialLabourApplySupply() {
    if (this.ViewProjectMaterialLabour.invalid) {
      this.commonService.showError("Fill Required Fields!");
      this.dataSource = new MatTableDataSource();
      this.rateAllocationList = []
      return;
    }
    this.rateAllocationList = []
    this.spinnerService.show();
    let Api = this.apiCallService.getRateMasters;
    let URI
    var head = { cmpCode: this.ViewProjectMaterialLabour.value.cmpCode, plantCode: this.ViewProjectMaterialLabour.value.plantCode, projCode: this.ViewProjectMaterialLabour.value.projCode }
    if (this.ViewProjectMaterialLabour.value.isMaster == 'Material') {
      URI = 'MaterialRates';
      this.displayedColumns = ['cmpCode', 'plantCode', 'projCode', 'materialDetailID', 'standardrUnitCode', 'rate'];
    } else if (this.ViewProjectMaterialLabour.value.isMaster == 'Labour') {
      URI = 'LabourRates';
      this.displayedColumns = ['cmpCode', 'plantCode', 'projCode', 'labourDetailsID', 'standardrUnitCode', 'rate'];
    } else if (this.ViewProjectMaterialLabour.value.isMaster == 'Apply Supply') {
      URI = 'ApplySupplyRates';
      this.displayedColumns = ['cmpCode', 'plantCode', 'projCode', 'applySupplyDetailsID', 'standardrUnitCode', 'rate'];
    }
    var data = { URI: URI, head: head }
    Api(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.rateAllocationList = result;
          if (this.rateAllocationList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.rateAllocationList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.spinnerService.hide();
          } else {
            this.dataSource = new MatTableDataSource();
            this.rateAllocationList = []
            this.spinnerService.hide();
          }
        } else {
          this.dataSource = new MatTableDataSource();
          this.rateAllocationList = []
          this.spinnerService.hide();
        }
      }
    })
  }

  async ProjectRateAllocation() {

    if (this.ProjectMaterialLabour.valid || this.MaterialJSON.length > 0 || this.LabourJSON.length > 0 || this.ApplySupplyJSON.length > 0) {
      if (this.ProjectMaterialLabour.value.material.length > 0 || this.ProjectMaterialLabour.value.labour.length > 0 || this.ProjectMaterialLabour.value.applySupply.length > 0 || this.MaterialJSON.length > 0 || this.LabourJSON.length > 0 || this.ApplySupplyJSON.length > 0) {

        this.spinnerService.show();
        var json = null;
        var api: any = null;
        if (this.ProjectMaterialLabour.value.isMaster == 'Material') {
          api = this.apiCallService.addMaterialRates
          if (this.MaterialJSON.length > 0) { json = await this.MaterialJSON; }
          else { json = await this.jsonMaterialSort(this.ProjectMaterialLabour.value.material); }

        } else if (this.ProjectMaterialLabour.value.isMaster == 'Labour') {
          api = this.apiCallService.addLabourRates
          if (this.LabourJSON.length > 0) { json = await this.LabourJSON; }
          else { json = await this.jsonLabourSort(this.ProjectMaterialLabour.value.labour); }

        } else if (this.ProjectMaterialLabour.value.isMaster == 'Apply Supply') {
          api = this.apiCallService.addApplySupplyRates
          if (this.ApplySupplyJSON.length > 0) { json = await this.ApplySupplyJSON; }
          else { json = await this.jsonApplySupplySort(this.ProjectMaterialLabour.value.applySupply); }

        }

        var data = { data: json }
        console.log("///////////////////",data)

        api(data).subscribe({
          next: (result: any) => {
            if (result) {
              this.commonService.showSuccess("Rate Allocation Successfully!")
              this.ProjectMaterialLabour.reset();
              this.spinnerService.hide();
              this.myForm.resetForm();
              this.MaterialJSON = []; this.LabourJSON = []; this.ApplySupplyJSON = [];
              this.UploadMaterial.reset();
              if (this.getmaterialdetails()) {
                while (this.getmaterialdetails().length !== 0) {
                  this.getmaterialdetails().removeAt(0)
                }
              }
              if (this.getlabourdetails()) {
                while (this.getlabourdetails().length !== 0) {
                  this.getlabourdetails().removeAt(0)
                }
              }
              if (this.getapplysupplydetails()) {
                while (this.getapplysupplydetails().length !== 0) {
                  this.getapplysupplydetails().removeAt(0)
                }
              }
            }
          }, error: (error: any) => {

            this.spinnerService.hide();
            this.commonService.showError(error);
          }
        })
      } else { this.commonService.showError("At list one Material, Labour, Apply Supply Rate required!") }
    } else {
      this.commonService.showError("Invalid!");
      return;
    }
  }





  async jsonMaterialSort(data: any) {
    this.UnitList = await this.commomFunction.getunitMaster(this.ProjectMaterialLabour.value.cmpCode);
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach(async (element: any) => {
        var found = await this.UnitList.find((item: any) => item.standardrUnitCode == element.unit || item.unitCode1 == element.unit || item.unitCode2 == element.unit || item.unitCode3 == element.unit || item.unitDesc == element.unit);
        if (found) {
          arr.push(
            {
              "cmpCode": this.ProjectMaterialLabour.value.cmpCode,
              "plantCode": this.ProjectMaterialLabour.value.plantCode,
              "projCode": this.ProjectMaterialLabour.value.projCode,
              "MaterialDetailID": element.actualMaterialId,
              "LabourDetailsID": null,
              "ApplySupplyDetailsID": null,
              "StandardrUnitCode": found.standardrUnitCode,
              "Rate": Number(Number(element.rate).toFixed(2))
            })
        } else {
          this.commonService.showError("Unit not found in system " + element.unit)
          resolve([]);
        }
      });
      resolve(arr)
    })
  }

  async jsonLabourSort(data: any) {
    var arr: any = []
    this.UnitList = await this.commomFunction.getunitMaster(this.ProjectMaterialLabour.value.cmpCode);
    return new Promise(async (resolve, reject) => {
      await data.forEach(async (element: any) => {
        var found = await this.UnitList.find((item: any) => item.standardrUnitCode == element.unit || item.unitCode1 == element.unit || item.unitCode2 == element.unit || item.unitCode3 == element.unit || item.unitDesc == element.unit);
        if (found) {
          arr.push({
            "cmpCode": this.ProjectMaterialLabour.value.cmpCode,
            "plantCode": this.ProjectMaterialLabour.value.plantCode,
            "projCode": this.ProjectMaterialLabour.value.projCode,
            "LabourDetailsID": element.subLabourId,
            "MaterialDetailID": null,
            "ApplySupplyDetailsID": null,
            "StandardrUnitCode": found.standardrUnitCode,
            "Rate": Number(Number(element.rate).toFixed(2))
          })
        } else {
          this.commonService.showError("Unit not found in system " + element.unit)
          resolve([]);
        }
      });
      resolve(arr)
    })
  }

  async jsonApplySupplySort(data: any) {
    var arr: any = []
    this.UnitList = await this.commomFunction.getunitMaster(this.ProjectMaterialLabour.value.cmpCode);
    return new Promise(async (resolve, reject) => {
      await data.forEach(async (element: any) => {
        var found = await this.UnitList.find((item: any) => item.standardrUnitCode == element.unit || item.unitCode1 == element.unit || item.unitCode2 == element.unit || item.unitCode3 == element.unit || item.unitDesc == element.unit);

        if (found) {
          arr.push({
            "cmpCode": this.ProjectMaterialLabour.value.cmpCode,
            "plantCode": this.ProjectMaterialLabour.value.plantCode,
            "projCode": this.ProjectMaterialLabour.value.projCode,
            "ApplySupplyDetailsID": element.applySupplyDetailsID,
            "MaterialDetailID": null,
            "LabourDetailsID": null,
            "StandardrUnitCode": found.standardrUnitCode,
            "Rate": Number(Number(element.rate).toFixed(2))
          })
        } else {
          this.commonService.showError("Unit not found in system" + element.unit)
          resolve([]);
        }

      });
      resolve(arr)
    })
  }

  // Download / Export Material Labour and Apply Supply
  async exportMaterial() {
    var arr: any = []

    this.spinnerService.show();
    if (this.ProjectMaterialLabour.value.cmpCode) {
      var AllMaterialList: any = await this.commomFunction.getMaterial(this.ProjectMaterialLabour.value.cmpCode);
      if (AllMaterialList.length > 0) {
        await AllMaterialList.forEach(async (element: any) => {
          if (element.subMaterial.length > 0) {
            await element.subMaterial.forEach( (element1: any) => {

                if (element1.materialDetails.length > 0) {
                  console.log(element1.materialDetails)

                  for (let i = 0; i < element1.materialDetails.length; i++) {
                    arr.push({
                      "materialID": element1.materialDetails[i].materialDetailID,
                      "descripption": element1.materialDetails[i].descripption,
                      "UnitCode": element1.materialDetails[i].standardrUnitCode,
                      "Rate": ""
                    })
                  }
                }

            })
          }
        });
        var headersArray = ['Actual Material ID', 'Description', 'Unit Code', 'Rate'];
        this.JsonToXlsxService.exportExcelWithoutHead(arr, headersArray, "Material Rate Allocation");
        this.spinnerService.hide();
      } else { this.spinnerService.hide(); this.commonService.showError("Material master not found") }
    } else { this.spinnerService.hide(); this.commonService.showError("Please Select Company") }
  }
  async exportLabour() {
    var arr: any = []
    this.spinnerService.show();
    if (this.ProjectMaterialLabour.value.cmpCode) {
      var AllLabourList: any = await this.commomFunction.getLabour(this.ProjectMaterialLabour.value.cmpCode);
      console.log(AllLabourList)
      if (AllLabourList.length > 0) {
        await AllLabourList.forEach((element: any) => {
          if (element.labourDetails) {
            if (element.labourDetails.length > 0) {
              for (let i = 0; i < element.labourDetails.length; i++) {
                arr.push({
                  "labourID": element.labourDetails[i].labourDetailsID,
                  "descripption": element.labourDetails[i].descripption,
                  "UnitCode": element.labourDetails[i].standardrUnitCode,
                  "Rate": ""
                })
              }
            }
          }
        });
        var headersArray = ['Actual Labour ID', 'Description', 'Unit Code', 'Rate'];
        this.JsonToXlsxService.exportExcelWithoutHead(arr, headersArray, "Labour Rate Allocation");
        this.spinnerService.hide();
      } else { this.spinnerService.hide(); this.commonService.showError("Labour master not found") }
    } else { this.spinnerService.hide(); this.commonService.showError("Please Select Company") }
  }
  async exportApplySupply() {
    var arr: any = []
    this.spinnerService.show();
    if (this.ProjectMaterialLabour.value.cmpCode) {
      var AllApplySupplyList: any = await this.commomFunction.getApplySupply(this.ProjectMaterialLabour.value.cmpCode);
      console.log(AllApplySupplyList)
      if (AllApplySupplyList.length > 0) {
        await AllApplySupplyList.forEach((element: any) => {

          if (element.applySupplyDetails.length > 0) {
            for (let i = 0; i < element.applySupplyDetails.length; i++) {

               arr.push({
                "applySupplyID": element.applySupplyDetails[i].applySupplyDetailsID,
                "descripption": element.applySupplyDetails[i].descripption,
                "UnitCode": element.applySupplyDetails[i].standardrUnitCode,
                "Rate": ""
              })
            }
          }
        });
        var headersArray = ['Apply Supply ID', 'Description', 'Unit Code', 'Rate'];
        this.JsonToXlsxService.exportExcelWithoutHead(arr, headersArray, "ApplySupply Rate Allocation");
        this.spinnerService.hide();
      } else { this.spinnerService.hide(); this.commonService.showError("Apply Supply master not found") }
    } else { this.spinnerService.hide(); this.commonService.showError("Please Select Company") }
  }

  async selectFileMaterial(event: any) {
    this.spinnerService.show();
    var files = event.target.files;
    if (files.length == 0) {
      this.commonService.showError("Please select excel file Ex.(.XLS,.XLSX).");
      this.spinnerService.hide();
      return;
    }

    this.spinnerService.show();
    this.ProjectMaterialLabour.patchValue({ FileUpload: files[0].name })
    var filename = files[0].name;
    var extension = filename.substring(filename.lastIndexOf(".")).toUpperCase();
    if (extension == '.XLS' || extension == '.XLSX') {

      const fileReader = new FileReader();
      fileReader.readAsBinaryString(files[0])
      fileReader.onload = (event: any) => {

        const BinaryData = event.target['result'];
        let workbook = XLSX.read(BinaryData, { type: 'binary' })
        // Read multiple sheets here ...
        workbook.SheetNames.forEach(async (sheetsName: any) => {
          const worksheet = workbook.Sheets[sheetsName];
          let xlsData: any = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

          var arr: any = []
          if (this.ProjectMaterialLabour.value.isMaster == 'Material') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ actualMaterialId: xlsData[i][0], unit: xlsData[i][2], rate: xlsData[i][3] })
            }
            this.MaterialJSON = await this.jsonMaterialSort(arr);

            this.spinnerService.hide();
          }
          if (this.ProjectMaterialLabour.value.isMaster == 'Labour') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ subLabourId: xlsData[i][0], unit: xlsData[i][2], rate: xlsData[i][3] })
            }
            this.LabourJSON = await this.jsonLabourSort(arr);

            this.spinnerService.hide();
          }
          if (this.ProjectMaterialLabour.value.isMaster == 'Apply Supply') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ applySupplyDetailsID: xlsData[i][0], unit: xlsData[i][2], rate: xlsData[i][3] })
            }
            this.ApplySupplyJSON = await this.jsonApplySupplySort(arr);
            this.spinnerService.hide();

          }

        });

      }
    } else {

      this.spinnerService.hide();
      this.commonService.showError("Please select a valid excel file Ex.(.XLS,.XLSX).");
    }
  }



}
interface rateAllocation {
  'cmpCode': string,
  'plantCode': string,
  'projCode': string,
  'labourDetailsID': string,
  'standardrUnitCode': string,
  'rate': string
};