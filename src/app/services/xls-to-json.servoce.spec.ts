import { TestBed } from '@angular/core/testing';
import { XlsxToJsonService } from './xls-to-json.service';


describe('JsonToXlsxService', () => {
  let service: XlsxToJsonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(XlsxToJsonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
