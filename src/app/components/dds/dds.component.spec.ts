import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DDSComponent } from './dds.component';

describe('DDSComponent', () => {
  let component: DDSComponent;
  let fixture: ComponentFixture<DDSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DDSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DDSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
