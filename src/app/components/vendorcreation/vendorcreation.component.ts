
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray,FormGroupDirective} from "@angular/forms";

import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ViewChild, AfterViewInit, TemplateRef } from '@angular/core';
import { ComponentsComponent } from '../components.component';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from 'src/environments/environment';
import { mobileValidation, EmailValidation, panValidation, ZipValidation, gstinValidation, IFSCValidation, Accountnumber } from '../../services/validations';
@Component({
  selector: 'app-vendorcreation',
  templateUrl: './vendorcreation.component.html',
  styleUrls: ['./vendorcreation.component.scss']
})

export class VendorcreationComponent implements OnInit {
  createVendor: FormGroup;
  val: any;
  CountryList: any;
  updateRow: any;
  deleteRow: any;
  isActiveTab: number = 0;
  isCreate: number = 0;
  submitted: any;
  StateList: any;
  CompanyList: any;
  ViewVendor: FormGroup;
  displayedColumns = ['action','bpCode', 'bpFName', 'bpLName', 'pan', 'phone', 'email'];
  AccountTypeList = [{ "type": "Savings Account", "value": "SA" }, { "type": "Salary Account", "value": "SC" }]
  VendorList: any;
  @ViewChild(FormGroupDirective) myForm: any;
  dataSource!: MatTableDataSource<VENDORDATA>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  // Mat Dialogs.
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  constructor(private spinnerService: NgxSpinnerService, private commomFunction: ComponentsComponent, private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createVendor = this.fb.group({

      cmpCode: [null, [Validators.required]],
      bpCode: [null],
      plantCode: null,
      actGroupCode: ["VEN"],
      bpFName: [null, [Validators.required, Validators.maxLength(50)]],
      bpMName: [null, [Validators.required, Validators.maxLength(35)]],
      bpLName: [null, [Validators.required, Validators.maxLength(35)]],
      shortKey: [''],
      countryCode: [null, [Validators.required]],
      stateCode: [null, [Validators.required]],
      PAN: [null, [Validators.required, panValidation]],
      gstNumber: [null, [Validators.required, gstinValidation]],
      City: [null, [Validators.required]],
      Street: [null, [Validators.required]],
      AddressLine1: '',
      AddressLine2: '',
      PostalCode: [null, [Validators.required, ZipValidation]],
      Phone: [null, [Validators.required, mobileValidation]],
      Email: [null, [Validators.required, EmailValidation]],
      Status: true,
      Remark: [null],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      isDeleted: [null],
      banks: new FormArray([this.addBanksGroup()]),
      contactPersons: new FormArray([this.addContactPerGroup()]),
    })

    this.ViewVendor = this.fb.group({ cmpCode: [null, [Validators.required]], })
  }


  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany();
    this.CountryList = await this.commomFunction.getCountry();
  }


  
  tabIsActive(value: any, change: any) {
    console.log(value, change);
    if (value != change) {
    
      this.updateRow = null, value = 0
      this.createVendor.reset();
      while (this.BanksGroupArr.length !== 1) {
        this.BanksGroupArr.removeAt(0)
      }
      this.createVendor.reset();
    }
    this.isActiveTab = value;
    this.isCreate = value;
    this.createVendor.enable();    
    
  }
  // Table view filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  async onChangeCountry(data: any) {
    this.StateList = await this.commomFunction.getState(data);
  }
  public myError = (controlName: string, errorName: string) => {
    return this.createVendor.controls[controlName].hasError(errorName);
  }

  // Bank Array
  get BanksGroupArr(): any {
    return <FormArray>this.createVendor.get('banks') as FormArray;
  }
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.BanksGroupArr['controls'][i]['controls'][controlName].hasError(errorName)
  }

  private addBanksGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      BpCode: null,
      MICR: null,
      BankName: [null, [Validators.required]],
      IFSE: [null, [Validators.required, IFSCValidation]],
      AccountType: [null, [Validators.required]],
      AccountNumber: [null, [Validators.required, Accountnumber]],
      BranchAddress: [null, [Validators.required]],
      status: true
    })
  }
  // Add Banks Array
  addBanks() {
    this.BanksGroupArr.push(this.addBanksGroup());
  }
  //Delete Form Array
  deletearr(i: any) {
    this.BanksGroupArr.removeAt(i);
  }


  //Contact Person Array

  // Bank Array
  get contactArray(): any {
    return <FormArray>this.createVendor.get('contactPersons') as FormArray;
  }
  public myErrorContact = (controlName: string, errorName: string, i: any) => {
    return this.contactArray['controls'][i]['controls'][controlName].hasError(errorName)
  }

  private addContactPerGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      BpCode: null,
      actGroupCode: "CP",
      bpFName: [null, [Validators.required, Validators.maxLength(50)]],
      bpMName: [null, [Validators.required, Validators.maxLength(35)]],
      bpLName: [null, [Validators.required, Validators.maxLength(35)]],
      shortKey: [''],
      countryCode: [null, [Validators.required]],
      stateCode: [null, [Validators.required]],
      City: [null, [Validators.required]],
      Street: [null, [Validators.required]],
      AddressLine1: '',
      AddressLine2: '',
      PostalCode: [null, [Validators.required, ZipValidation]],
      Phone: [null, [Validators.required, mobileValidation]],
      Email: [null, [Validators.required, EmailValidation]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      isDeleted: [null],

      })
  }
  // Add Banks Array
  addContact() {
    this.contactArray.push(this.addContactPerGroup());
  }
  //Delete Form Array
  deleteContact(i: any) {
    this.contactArray.removeAt(i);
  }

//Add vendor
  addVendor() {
    this.isActiveTab = 1;

    console.log('this.createVendor', this.createVendor);
    if (this.createVendor.invalid) {
      this.submitted = true;
      this.createVendor.markAllAsTouched()
      return;
    }
    this.createVendor.patchValue({
      actGroupCode: 'VEN',
      Status: true,  

    })
    this.BanksGroupArr.value.forEach((element: any, index: any) => {
      (<FormGroup>this.BanksGroupArr.at(index)).patchValue({
        cmpCode: this.createVendor.value.cmpCode,
        BpCode: this.createVendor.value.bpCode,
        status: true,
      })
    })
    this.contactArray.value.forEach((element: any, index: any) => {
      (<FormGroup>this.contactArray.at(index)).patchValue({
        cmpCode: this.createVendor.value.cmpCode,
        actGroupCode: 'CP',
      })
    })

    var JSON = this.createVendor.value
    let Api: any = this.apiCallService.addVendor;
    JSON.createBy = environment.createBy; // Loged IN User ID
    JSON.updateBy = environment.updateBy; // Loged IN User ID

    var head = {}
    if (this.isCreate == 1) {
      
      Api = this.apiCallService.updateVendor, head = { cmpCode: this.updateRow.cmpCode, bpCode: this.updateRow.bpCode }
    }
    var data = { data: JSON, head: head }
    console.log(data);
    Api(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");
        
          console.log(this.createVendor.value.cmpCode)
          this.ViewVendor.patchValue({cmpCode:this.createVendor.value.cmpCode});
          console.log( this.ViewVendor.value.cmpCode)
          this.VendorList = await this.commomFunction.getVendor(JSON.cmpCode);
          this.tabIsActive(0, 0);

          if (this.VendorList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.VendorList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
          else {
            this.VendorList = [];
          }
          // this.createVendor.reset();
        }
        this.createVendor.reset();
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }

  // update Vendor
  async editVendor(row: any, isEdit: any) {
    
    console.log(row,isEdit)
    this.updateRow = row;
    if (this.BanksGroupArr.length > 0) {
      while (this.BanksGroupArr.length !== 0) {
        this.BanksGroupArr.removeAt(0)
      }
      if (this.contactArray.length > 0) {
        while (this.contactArray.length !== 0) {
          this.contactArray.removeAt(0)
        }}
    }
    console.log('array',this.updateRow)

    this.createVendor.patchValue({
      bpCode: this.updateRow.bpCode,
      cmpCode: this.updateRow.cmpCode,
      plantCode: this.updateRow.plantCode,
      actGroupCode: this.updateRow.actGroupCode,
      bpFName: this.updateRow.bpFName,
      bpMName: this.updateRow.bpMName,
      bpLName: this.updateRow.bpLName,
      shortKey: this.updateRow.shortKey,
      PAN: this.updateRow.pan,
      gstNumber: this.updateRow.gstNumber,
      AddressLine1: this.updateRow.addressLine1,
      AddressLine2: this.updateRow.addressLine2,
      Street: this.updateRow.street,
      City: this.updateRow.city,
      PostalCode: this.updateRow.postalCode,
      Email: this.updateRow.email,
      Phone: this.updateRow.phone,
      stateCode: this.updateRow.stateCode,
      countryCode: this.updateRow.countryCode,
      Remark: this.updateRow.remark,
    });


    if (this.updateRow.banks.length > 0) {
      this.updateRow.banks.map((item: any, index: any) => {
        this.addBanks();
        (<FormGroup>this.BanksGroupArr.at(index)).patchValue({
          cmpCode: this.createVendor.value.cmpCode,
          plantCode: this.createVendor.value.plantCode,
          BpCode: this.createVendor.value.bpCode,
          Status: true,
          BankName: item.bankName,
          IFSE: item.ifse,
          AccountType: item.accountType,
          MICR: item.micr,
          AccountNumber: item.accountNumber,
          BranchAddress: item.branchAddress,
        })
      })
    }
   
    if (this.updateRow.contactPersons.length > 0) {
      this.updateRow.contactPersons.map((item: any, index: any) => {
        this.addContact();
        (<FormGroup>this.contactArray.at(index)).patchValue({
          cmpCode: this.createVendor.value.cmpCode,
          plantCode: this.createVendor.value.plantCode,
          BpCode: item.bpCode,
          actGroupCode: item.actGroupCode,
          bpFName:item.bpFName,
          bpMName:item.bpMName,
          bpLName: item.bpLName,
          shortKey: item.shortKey,
          countryCode:item.countryCode,
          stateCode: item.stateCode,
          City: item.city,
          Street: item.street,
          AddressLine1:item.addressLine1,
          AddressLine2:item.addressLine2,
          PostalCode: item.postalCode,
          Phone: item.phone,
          Email:item.email,
        })
      })
    }
    this.tabIsActive(1, 1);
    if (isEdit == 'edit') { 
      this.createVendor.enable(); 
    } 
    else { 
      this.createVendor.disable();
       this.isCreate = 2; 
    }
  
  }

  async onChangeCompany(data: any) {
    this.spinnerService.show();
    this.VendorList = await this.commomFunction.getVendor(data); //get vendor

    if (this.VendorList.length > 0) {
      console.log(this.VendorList)
      // Assign the data to the data source for the table to render
      this.dataSource = new MatTableDataSource(this.VendorList);
      console.log(this.dataSource)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinnerService.hide();
    }
    else {
      this.VendorList = [];
      this.spinnerService.hide();
    }
  }
  // open dialog box
  deleteMe(row: any): void {
    this.deleteRow = row;
    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  // confirm delete
  deleteVendor() {
    var head = { cmpCode: this.deleteRow.cmpCode, bpCode: this.deleteRow.bpCode }
    var data = { head: head, param: null }
    this.apiCallService.deleteVendor(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Deleted Succefully!");
          // const index = this.CompanyList.indexOf(result);
          // if (index > -1) { // only splice array when item is found
          //   this.CompanyList.splice(index, 1); // 2nd parameter means remove one item only
          // }
          this.VendorList = await this.commomFunction.getVendor(this.deleteRow.cmpCode);
          this.tabIsActive(0, 0);

          if (this.VendorList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.VendorList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
}

export interface VENDORDATA {
  
  bpCode: string,
  bpFName: string,
  bpLName: string,
  pan: string,
  phone: string,
  email: string,
}

