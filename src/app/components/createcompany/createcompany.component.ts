import { Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective } from "@angular/forms";
import { MatDialog } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { mobileValidation, EmailValidation, panValidation, gstinValidation, ZipValidation, CINValidation } from '../../services/validations';
import { ComponentsComponent } from '../components.component';

@Component({
  selector: 'app-createcompany',
  templateUrl: './createcompany.component.html',
  styleUrls: ['./createcompany.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreatecompanyComponent implements OnInit {
  panelOpenState: boolean = false;
  createCompany: FormGroup;
  CountryList: any;
  StateList: any;
  CompanyList: any = [];
  selectedFiles: any;
  selectedFileNames: any;
  // View dataTabale
  displayedColumns = ['action', 'cmpCode', 'cmpName', 'cmpPAN', 'cmpCIN', 'email', 'street', 'city', 'postalCode'];
  displayedColumn = ['cmpCode', 'plantCode', 'plantName', 'shortKey', 'street', 'addressLine1', 'addressLine2', 'city', 'postalCode', 'countryCode', 'stateCode', 'email', 'phone'];

  dataSource!: MatTableDataSource<CmpData>;
  viewDataSource!: MatTableDataSource<PlantData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  updateRow: any;
  deleteRow: any;
  isActiveTab: number = 0;
  isCreate: number = 0;
  previews: any;
  length: any;
  @ViewChild(FormGroupDirective) myForm: any;
  @ViewChild('accordion', { static: true }) Accordion?: MatAccordion;
  temp: unknown;
  jsonlist: any = [];
  fileData: any = [];
  constructor(private spinnerService: NgxSpinnerService, private commomFunction: ComponentsComponent, private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createCompany = this.fb.group({
      cmpCode: [null, [Validators.required, Validators.maxLength(10)]],
      cmpName: [null, [Validators.required, Validators.maxLength(45)]],
      shortKey: [null, [Validators.required, Validators.maxLength(15)]],
      cmpPAN: [null, [Validators.required, panValidation]],
      cin: [null, [Validators.required, CINValidation]],
      // countryCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      stateCode: [null, [Validators.required]],
      city: [null, [Validators.required, Validators.maxLength(45)]],
      street: [null, [Validators.required, Validators.maxLength(45)]],
      addressLine1: [null, Validators.maxLength(50)],
      addressLine2: [null, Validators.maxLength(50)],
      postalCode: [null, [Validators.required, ZipValidation]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      cmpLogo: [null],
      cmpWebSiteURL: [null, [Validators.required, Validators.maxLength(50)]],
      plants: new FormArray([this.addGSTNArrGroup()]),
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
    })
    // Validators.maxLength(10)]
  }
  async ngOnInit() {
    document.body.style.backgroundColor = "#E5E7E9";
    this.getCompany();
    this.CountryList = await this.commomFunction.getCountry();

  }

  public myError = (controlName: string, errorName: string) => {
    return this.createCompany.controls[controlName].hasError(errorName);
  }
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.plantsArray['controls'][i]['controls'][controlName].hasError(errorName)
  }

  // Mat Dialogs.
  @ViewChild('viewDialog') viewDialog!: TemplateRef<any>;
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef, {
      disableClose: true,
    });
  }


  // Get plantsArray Form Array
  get plantsArray(): any {
    return <FormArray>this.createCompany.get('plants') as FormArray;
  }
  plantsArrayFun() {
    return <FormArray>this.createCompany.get('plants') as FormArray;
  }
  //Form Array
  private addGSTNArrGroup(): FormGroup {
    return this.fb.group({
      cmpCode: [null],
      plantCode: [null, [Validators.required, Validators.maxLength(10)]],
      plantName: [null, [Validators.required, Validators.maxLength(50)]],
      shortKey: [null, [Validators.required, Validators.maxLength(10)]],
      gstNumber: [null, [Validators.required, gstinValidation]],
      addressLine1: [null, Validators.maxLength(50)],
      addressLine2: [null, Validators.maxLength(50)],
      street: [null, [Validators.required, Validators.maxLength(45)]],
      city: [null, [Validators.required, Validators.maxLength(45)]],
      postalCode: [null, [Validators.required, ZipValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null]
    })
  }
  // ADD Click event of plantsArray Form Array
  addGSTNArr() {
    let control = <FormArray>this.createCompany.controls.plants;
    control.push(
      this.fb.group({
        cmpCode: [null],
        plantCode: [null, [Validators.required]],
        plantName: [null, [Validators.required]],
        shortKey: [null, [Validators.required]],
        gstNumber: [null, [Validators.required, gstinValidation]],
        addressLine1: [null],
        addressLine2: [null],
        street: [null, [Validators.required]],
        city: [null, [Validators.required]],
        postalCode: [null, [Validators.required]],
        stateCode: [null, [Validators.required]],
        countryCode: [null, [Validators.required]],
        email: [null, [Validators.required, EmailValidation]],
        phone: [null, [Validators.required, mobileValidation]],
        createBy: [null],
        createOn: [null],
        updateBy: [null],
        updateOn: [null]
      })
    );
  }
  //Delete plantsArray Form array
  deletearr(i: any) {
    let control = <FormArray>this.createCompany.controls.plants;
    control.removeAt(i)
    this.jsonlist = this.jsonlist.splice(i, 1);

  }

  // mat table datasource filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  async onChangeCountry(countryCode: any) {
    if (countryCode) { this.StateList = await this.commomFunction.getState(countryCode); } else { this.createCompany.controls['stateCode'].setValue(null); this.StateList = [] }
  }

  async onChangeCountrySub(countryCode: any, index: any) {
    if (countryCode) {
      var StateLists = await this.commomFunction.getState(countryCode);
      if (this.jsonlist[index]) {
        if (this.jsonlist[index]) {
          this.jsonlist[index] = StateLists
        }
        else {
          this.jsonlist[index].push(StateLists)
        }
      }
      else {
        this.jsonlist.push(StateLists)
      }

    } else {

      this.plantsArrayFun().at(index).patchValue({ stateCode: null })
      // this.createCompany.controls['stateCode'].setValue(null);
      this.StateList = []
    }
  }
  async getCompany() {
    this.spinnerService.show();
    this.CompanyList = await this.commomFunction.getCompany();
    if (this.CompanyList.length > 0) {
      // Assign the data to the data source for the table to render
      this.dataSource = new MatTableDataSource(this.CompanyList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.spinnerService.hide();
    } else {
      this.dataSource = new MatTableDataSource();
      this.CompanyList = [];
      this.spinnerService.hide();
    }
  }
  async addCompany() {
    this.isActiveTab = 1;
    console.log(this.createCompany)
    if (this.createCompany.invalid) {
      this.createCompany.markAllAsTouched()
      return;
    }
    let Api = this.apiCallService.addCompany;
    var JSON = this.createCompany.value;
    JSON.createBy = environment.createBy; // Loged IN User ID
    JSON.updateBy = environment.updateBy; // Loged IN User ID    
    // let dt = await this.Uint8ArrayFromBase64Url(this.previews?.replace("data:image/png;base64,", ""))
    // console.log(dt)
    JSON.cmpLogo = this.previews?.replace("data:image/png;base64,", "");
    console.log(btoa(this.previews), JSON.cmpLogo, this.previews)
    JSON.cmpLogo = btoa(this.previews);
    JSON.plants.map((item: any) => { item.cmpCode = JSON.cmpCode, item.createBy = JSON.createBy, item.updateBy = JSON.updateBy })
    if (this.isCreate == 1) { Api = this.apiCallService.updateCompany, JSON.oldCmpCode = this.updateRow.cmpCode }
    if (JSON.plants.length > 0) {
      var data = { data: JSON }
      console.log(data)
      Api(data).subscribe({
        next: async (result: any) => {
          if (result) {
            this.getCompany();
            this.commonService.showSuccess("Record Inserted Succefully!");
            this.Accordion?.closeAll();
            this.tabIsActive(0, 0);
            this.removefile();
          }
          this.createCompany.reset();
        }, error: (error: any) => {
          this.commonService.showError(error);
        }
      })
    } else { this.commonService.showError("Please Add at list 1 plant") }
  }
  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {

    if (value == 0 && change == 0) { this.Accordion?.closeAll(); }
    if (value != change) {
      this.removefile(); this.updateRow = null, value = 0;
    }
    this.isActiveTab = value;
    this.isCreate = value;
    this.createCompany.reset();
    this.myForm.resetForm();
    this.createCompany.enable();

  }
  async editCompany(row: any) {

    this.updateRow = row;
    if (this.plantsArray.length > 0) {
      while (this.plantsArray.length !== 0) {
        <FormGroup>this.plantsArray.removeAt(0)
      }
    }
    if (row.plants.length > 0) {
      row.plants.map((item: any, i: any) => { if (item.isDeleted === false) { this.addGSTNArr(); this.onChangeCountrySub(item.countryCode, i); } });
      row.plants = await row.plants.filter(function (obj: any) { return !obj.isDeleted });
    }
    this.tabIsActive(1, 1);
    this.isCreate = 1;
    if (row.cmpLogo) { this.previews = atob(row.cmpLogo); this.selectedFileNames = "Logo.png"; }
    this.StateList = await this.commomFunction.getState(row.countryCode)
    // Use add form for update patch values
    this.createCompany.patchValue(row)
  }
  async viewCompany(row: any) {
    this.updateRow = row;
    if (this.plantsArray.length > 0) {
      while (this.plantsArray.length !== 0) {
        <FormGroup>this.plantsArray.removeAt(0)
      }
    }
    if (row.plants.length > 0) {
      row.plants.map((item: any, i: any) => { if (item.isDeleted === false) { this.addGSTNArr(); this.onChangeCountrySub(item.countryCode, i); } });
      row.plants = await row.plants.filter(function (obj: any) { return !obj.isDeleted });
    }
    this.tabIsActive(1, 1);
    this.isCreate = 2;
    if (row.cmpLogo) { this.previews = atob(row.cmpLogo); this.selectedFileNames = "Logo.png"; }
    this.StateList = await this.commomFunction.getState(row.countryCode);
    // Use add form for update patch values

    this.createCompany.patchValue(row);
    this.createCompany.disable();
  }
  deleteMe(row: any) {
    this.deleteRow = row;
    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  deleteCompany() {
    var data = { cmpCode: this.deleteRow.cmpCode }
    this.apiCallService.deleteCompany(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Deleted Succefully!");
          const index = this.CompanyList.indexOf(this.deleteRow);
          if (index > -1) { // only splice array when item is found
            this.CompanyList.splice(index, 1); // 2nd parameter means remove one item only
          }
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.CompanyList);
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
  async selectFiles(event: any) {
    this.selectedFileNames = null;
    this.previews = null;
    this.selectedFiles = event.target.files;
    this.selectedFileNames = this.selectedFiles[0].name
    if (this.selectedFiles[0].size > 150000) {
      this.commonService.showError("Please select file less than 150 kb.")
      return;
    } else if (this.selectedFiles[0].name.length > 80) {
      this.commonService.showError("Please select file less than 80 characters.")
      return;
    } else {
      const file = event.target.files[0];
      if (file) {
        // Convert file to base64
        const fileContents = await this.commonService.readAsDataURL(file)
        this.previews = fileContents;
        console.log(atob(this.previews), btoa(this.previews))
      } else {
        this.previews = null
      }
    }

    console.log(this.previews, this.selectedFiles)
  }
  removefile() {
    this.selectedFileNames = null;
    this.previews = null;
  }

  Uint8ArrayFromBase64Url(base64Url: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      var unitArr = Uint8Array.from(window.atob(base64Url.replace(/-/g, "+").replace(/_/g, "/")), (v) => v.charCodeAt(0));
      for (let value of unitArr.values()) {
        await arr.push(value)
      }
      resolve(arr)
    })
  }

}

export interface CmpData {
  cmpCode: string;
  stateCode: string;
  cmpName: string;
  shortKey: string;
  street: string;
  cmpPAN: string;
  cmpCIN: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  postalCode: string;
  cmpLogo: string;
  cmpWebSiteURL: string;
  email: string;
  phone: string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;
}

export interface PlantData {
  cmpCode: string;
  plantCode: string;
  plantName: string;
  shortKey: string;
  street: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  postalCode: string;
  email: string;
  phone: string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;
}




