import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateconsultantComponent } from './createconsultant.component';
const routes: Routes = [
  { path: '', component: CreateconsultantComponent, children: [] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateconsultantRoutingModule { }
