// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl:'http://115.160.216.115:4004/API/',
  // baseUrl:'http://115.160.216.115:1080/API/',
  // baseUrl:'https://sconapi.primustechsys.com/api/',
  Cid:"1000",
  Pid:"1010",
  updateBy:"101770",
  createBy:"101770"
  
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
