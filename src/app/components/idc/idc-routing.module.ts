import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IDCComponent } from './idc.component';

const routes: Routes = [{path:'',component:IDCComponent,children:[]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IDCRoutingModule { }
