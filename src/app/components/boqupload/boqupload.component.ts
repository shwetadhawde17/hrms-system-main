import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDragEnter, CdkDragExit, CdkDragStart } from '@angular/cdk/drag-drop';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { JsonToXlsxService } from 'src/app/services/json-to-xlsx.service';
import { XlsxToJsonService } from 'src/app/services/xls-to-json.service';
import * as XLSX from 'xlsx';
import { ComponentsComponent } from '../components.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-boqupload',
  templateUrl: './boqupload.component.html',
  styleUrls: ['./boqupload.component.scss']
})
export class BoquploadComponent implements OnInit {
  uploadBOQ: FormGroup;
  bulkUploadBOQ: FormGroup;
  ProjectList: any;
  convertedJSON: any = [];
  ActivityHead: any = [];
  BtnDisabled = 0;
  SelectedActivityHead: any = [];
  arr: any;
  UnitMasterList: any;
  updateRow: any;
  isActiveTab: any;
  isCreate: any;
  CompanyList: any;
  PlantList: any;
  BOQList: any = [];
  displayedColumns = ['action', 'cmpCode', 'plantCode', 'projCode']
  displayedColumnsview = ['actGroupId', 'actSeqNo', 'displaySequence', 'actDesc', 'quntity', 'displayUnit', 'rate', 'amount']

  dataSource!: MatTableDataSource<BOQData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  viewDataSource: any = [];
  // Mat Dialogs.
  @ViewChild('viewDialog') viewDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef, {
      disableClose: true,
    });
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      // let item: any = event.previousContainer.data[event.previousIndex];
      // let copy: any = JSON.parse(JSON.stringify(item));
      // this.SelectedActivityHead.splice(event.currentIndex, 0, copy);
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
  }

  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  constructor(private changeDetectorRefs: ChangeDetectorRef, private dialog: MatDialog, private commomFunction: ComponentsComponent, private JsonToXlsxService: JsonToXlsxService, private cdRef: ChangeDetectorRef, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService, private spinnerService: NgxSpinnerService) {
    this.uploadBOQ = fb.group({
      cmpCode: [null, Validators.required],
      plantCode: [null, Validators.required],
      projCode: [null, Validators.required]
    })
    this.bulkUploadBOQ = fb.group({
      cmpCode: [null, Validators.required],
      plantCode: [null, Validators.required],
      projCode: [null, Validators.required],
      DocumentFileUpload: [null, Validators.required]
    })
  }
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }
  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany();
  }

  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {
    if (value != change) {
      this.updateRow = null, value = 0; this.ActivityHead = []
    }
    this.isActiveTab = value;
    this.isCreate = value;
    this.bulkUploadBOQ.reset();
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  async getBOQByID(cmpCode: any, plantCode: any, projCode: any) {
    this.BOQList = []
    this.spinnerService.show();

    if (cmpCode && plantCode) {
      this.BOQList = await this.commomFunction.getBOQ(cmpCode, plantCode, projCode, false);
      console.log(this.BOQList)
      var NewBOQList: any = await this.commonService.getFilterBOQ(this.BOQList)
      if (NewBOQList.length > 0) {
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(NewBOQList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide();
      } else {
        this.BOQList = [];
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide();
      }
    } else {
      this.spinnerService.hide();
    }
  }

  downloadUploadBOQ() {
    var arr = []
    if (this.SelectedActivityHead.length > 0) {
      for (var i = 0; i < this.SelectedActivityHead.length; i++) {
        var dt: any = this.SelectedActivityHead[i]

        arr.push({ SR_NO: dt.actGroupId, SeqNO: "", subSeqNO: "", Description: dt.actGroupDesc, Qty: "", Unit: "", Rate: "" });
        for (var k = 1, kLen = 4; k < kLen; k++) {
          arr.push({
            SR_NO: "", SeqNO: k, subSeqNO: "", Description: "", Qty: "", Unit: "", Rate: ""
          })
          for (var L = 1, LLen = 3; L < LLen; L++) {
            var dd
            if (L == 1) { dd = 'a' } else if (L == 2) { dd = 'b' } else if (L == 3) { dd = 'c' }
            arr.push({
              SR_NO: "", SeqNO: k, subSeqNO: dd, Description: "", Qty: "", Unit: "", Rate: ""
            })
          }
        }

      }


      var headersArray = ['ActGroupId', 'ActSeqNo', 'DisplaySequence', 'ActDesc', 'Quntity', 'UnitCode', 'Rate'];

      this.JsonToXlsxService.exportExcelWithoutHead(arr, headersArray, "BOQ")
    } else {
      this.commonService.showError("Please select activity head!")
    }
  }
  async downloadUplodedView(row: any) {
    var find: any = await this.BOQList.map((item: any) => { if (item[0].cmpCode == row.cmpCode && item[0].plantCode == row.plantCode && item[0].projCode == row.projCode) { return item } });


    if (find.length > 0) {
      var finalList: any = await this.commonService.getExcelJSONList(find[0]);

      if (finalList.length > 0) {
        var headersArray = ['ActGroupId', 'ActSeqNo', 'DisplaySequence', 'ActDesc', 'UnitCode', 'Quntity', 'Rate','Amount'];

        this.JsonToXlsxService.exportExcelWithoutHead(finalList, headersArray, "BOQ");
      }
    }
  }
  async onChangeCompany(cmpCode: any) {
    if (cmpCode) {
      this.PlantList = await this.commomFunction.getPlant(cmpCode);
      this.UnitMasterList = await this.commomFunction.getunitMaster(cmpCode);
    } else {
      this.dataSource = new MatTableDataSource()
      this.BOQList = [];
      this.uploadBOQ.controls['plantCode'].setValue(null); this.PlantList = [];
      this.uploadBOQ.controls['projCode'].setValue(null); this.ProjectList = [];
      this.bulkUploadBOQ.controls['plantCode'].setValue(null); this.ActivityHead = [];
      this.bulkUploadBOQ.controls['projCode'].setValue(null); this.UnitMasterList = [];
    }
  }
  async onChangePlant(cmpCode: any, plantCode: any) {
    if (cmpCode && plantCode) {
      //   this.getBOQByID(cmpCode, plantCode, null);
      this.ProjectList = await this.commomFunction.getproject(cmpCode, plantCode)
    } else {
      this.uploadBOQ.controls['projCode'].setValue(null);
      this.bulkUploadBOQ.controls['projCode'].setValue(null);
      this.ProjectList = []; this.ActivityHead = [];
      this.dataSource = new MatTableDataSource()
      this.BOQList = [];
    }
  }
  async onChangeProject(cmpCode: any, projCode: any) {
    if (cmpCode && projCode) {
      var found = await this.ProjectList.find((item: any) => { if (item.projCode == projCode) return item });
      this.getBOQByID(cmpCode, this.bulkUploadBOQ.value.plantCode, projCode);
      if (found) {
        this.ActivityHead = await this.commomFunction.getActivityHead(cmpCode, found.activityCode)
      } else { this.ActivityHead = []; this.SelectedActivityHead = [] };
    } else {
      this.getBOQByID(cmpCode, this.bulkUploadBOQ.value.plantCode, null);
    }

  }
  async bulkUploadBOQs() {
    if (this.bulkUploadBOQ.invalid) {
      return;
    }
    if (this.convertedJSON.length > 0) {
      let param = null;
      let IsBOQPresent: any = await this.commomFunction.getIsBOQPresent(this.bulkUploadBOQ.value.cmpCode, this.bulkUploadBOQ.value.plantCode, this.bulkUploadBOQ.value.projCode)

      if (IsBOQPresent > 0) {
        param = { IsUpdate: true }
      } else {
        param = { IsUpdate: false }
      }
      var data = { data: this.convertedJSON, param: param }
      console.log("boq upload data", data)

      this.apiCallService.addBOQUpload(data).subscribe({
        next: (result: any) => {
          if (result) {
            this.commonService.showSuccess("BOQ Uploded Succefully!");
            this.bulkUploadBOQ.reset();
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
        }
      })
    }
    // }
  }
  async getJSON(data: any) {
    return new Promise(async (resolve, reject) => {
      this.arr = []
      var details = []
      let j = 0
      let k = 0;
      var ActGroupId
      for (let i = 0; i < data.length; i++) {

        if (data[i]) {
          if (data[i].ActGroupId) {
            if (details.length > 0) {
              data[j].details = details;
              details = []
            }

            ActGroupId = null;
            ActGroupId = data[i].ActGroupId;
            if (data[i].UnitCode) {
              data[i].DisplayUnit = data[i].UnitCode;
              var find = await this.UnitMasterList.find((item: any) => {
                if (item.standardrUnitCode.toLowerCase().split(" ").join("") == data[i].UnitCode.toLowerCase().split(" ").join("") ||
                  item.unitCode1.toLowerCase().split(" ").join("") == data[i].UnitCode.toLowerCase().split(" ").join("") ||
                  item.unitCode2 == data[i].UnitCode.toLowerCase().split(" ").join("") ||
                  item.unitCode3.toLowerCase().split(" ").join("") == data[i].UnitCode.toLowerCase().split(" ").join("")
                )
                  return item.standardrUnitCode
              });
              if (find) {
                data[i].UnitCode = find.standardrUnitCode
              }
              else {
                this.commonService.showError("Unit Code not in system at row no " + (i + 2) + data[i].UnitCode); break;
              }
            } else { data[i].DisplayUnit = "" }
            data[i].cmpCode = this.bulkUploadBOQ.value.cmpCode;
            data[i].plantCode = this.bulkUploadBOQ.value.plantCode;
            data[i].ProjCode = this.bulkUploadBOQ.value.projCode;
            if (data[i].ActSeqNo) { data[i].ActSeqNo = (data[i].ActSeqNo) }
            data[i].Quntity = Number(String(data[i].Quntity).split(" ").join("").replace(/^\s+|\s+$/gm, ''));
            data[i].Rate = Number(String(data[i].Rate).split(" ").join("").replace(/^\s+|\s+$/gm, ''));

            if (data[i].Quntity && data[i].Rate) { data[i].Quntity = data[i].Quntity; data[i].Rate = data[i].Rate }
            else if (!data[i].Quntity && !data[i].Rate) { data[i].Rate = null; data[i].Quntity = null }
            else if (!data[i].Quntity && data[i].Rate) { data[i].Quntity = 0; }
            else if (data[i].Quntity && !data[i].Rate) { data[i].Rate = 0; }
            data[i].details = []
            await this.arr.push(data[i]);
            j = i;
            k++;
          } else {
            if (data[i].UnitCode) {
              data[i].DisplayUnit = data[i].UnitCode;
              var find = await this.UnitMasterList.find((item: any) => { if (item.standardrUnitCode.toLowerCase() == data[i].UnitCode.toLowerCase() || item.unitCode1.toLowerCase() == data[i].UnitCode.toLowerCase() || item.unitCode2.toLowerCase() == data[i].UnitCode.toLowerCase() || item.unitCode3.toLowerCase() == data[i].UnitCode.toLowerCase()) return item.standardrUnitCode });
            
              if (find) { data[i].UnitCode = find.standardrUnitCode }
              else {
                this.commonService.showError("Unit Code not in system at row no " + (i + 2) + data[i].UnitCode); break;
              }
            } else { data[i].DisplayUnit = ""; }

            data[i].cmpCode = this.bulkUploadBOQ.value.cmpCode;
            data[i].plantCode = this.bulkUploadBOQ.value.plantCode;
            data[i].ProjCode = this.bulkUploadBOQ.value.projCode;
            if (data[i].ActSeqNo) { data[i].ActSeqNo = (data[i].ActSeqNo) } else { this.commonService.showError("ActSeqNo not in excel at row no " + (i + 2) + data[i].ActSeqNo); return }


            data[i].Quntity = Number(String(data[i].Quntity).split(" ").join("").replace(/^\s+|\s+$/gm, ''));
            data[i].Rate = Number(String(data[i].Rate).split(" ").join("").replace(/^\s+|\s+$/gm, ''));
            if (data[i].Quntity && data[i].Rate) { data[i].Quntity = data[i].Quntity; data[i].Rate = data[i].Rate }
            else if (!data[i].Quntity && !data[i].Rate) { data[i].Rate = null; data[i].Quntity = null }
            else if (!data[i].Quntity && data[i].Rate) { data[i].Quntity = 0; }
            else if (data[i].Quntity && !data[i].Rate) { data[i].Rate = 0; }

            data[i].ActGroupId = ActGroupId;
            data[i].CreatedBy = environment.createBy
            data[i].UpdateBy = environment.updateBy
            details.push(data[i]);
          }
        }
      }
      if (details.length > 0) { data[j].details = details; this.arr.pop(k); this.arr[k] = data[j]; details = [] }
      this.arr = await this.commonService.filterIsEmpltyElement(this.arr)
      resolve(this.arr)
    })

  }
  // view BOQ Details
  async view(row: any) {
    this.spinnerService.show();
    this.BtnDisabled = 1;
    var find: any = await this.BOQList.map((item: any) => { if (item[0].cmpCode == row.cmpCode && item[0].plantCode == row.plantCode && item[0].projCode == row.projCode) { return item } });


    if (find.length > 0) {
      var finalList: any = await this.commonService.getExcelJSONList(find[0]);

      if (finalList.length > 0) {
        // Assign the data to the data source for the table to render
        this.viewDataSource = finalList;
        this.dataSource.sort = this.sort;
        this.openDialogWithTemplateRef(this.viewDialog);
        this.BtnDisabled = 0;
        this.spinnerService.hide();
      }
    } else {
      this.BtnDisabled = 0;
    }
  }
  // Method to upload a valid excel file

  async selectFile(event: any) {
    var files = event.target.files;
    if (files.length == 0) {
      this.commonService.showError("Please select excel file Ex.(.XLS,.XLSX).");
      return;
    }
    this.bulkUploadBOQ.patchValue({ DocumentFileUpload: files[0].name })
    var filename = files[0].name;
    var extension = filename.substring(filename.lastIndexOf(".")).toUpperCase();
    if (extension == '.XLS' || extension == '.XLSX') {

      const fileReader = new FileReader();
      fileReader.readAsBinaryString(files[0])
      fileReader.onload = (event: any) => {
        // 
        const BinaryData = event.target['result'];
        let workbook = XLSX.read(BinaryData, { type: 'binary' })
        // Read multiple sheets here ...
        workbook.SheetNames.forEach(async (sheetsName: any) => {
          const worksheet = workbook.Sheets[sheetsName];
          // const data: any = XLSX.utils.sheet_to_json(worksheet);
          let xlsData: any = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

          var arr = []
          for (let i = 1; i < xlsData.length; i++) {
            arr.push({ ActGroupId: xlsData[i][0], ActSeqNo: xlsData[i][1], DisplaySequence: xlsData[i][2], ActDesc: xlsData[i][3], Quntity: xlsData[i][4], UnitCode: xlsData[i][5], Rate: xlsData[i][6] })
          }
          this.convertedJSON = await this.getJSON(arr);

        });

      }
    } else {
      this.commonService.showError("Please select a valid excel file Ex.(.XLS,.XLSX).");
    }
    // }
  }

}



export interface BOQData {
  cmpCode: string,
  plantCode: string,
  projCode: string
}
export interface BOQDetailsData {
  actDesc: string,
  actSeqNo: string,
  boqId: string,
  cmpCode: string,
  displaySequence: string,
  displayUnit: string,
  plantCode: string,
  projCode: string,
  quntity: string,
  rate: string,
  subSequence: string,
  amount: string
}



