import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BoquploadRoutingModule } from './boqupload-routing.module';
import { MatTableModule } from '@angular/material/table';
import { BoquploadComponent } from './boqupload.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  declarations: [BoquploadComponent],
  imports: [
    CommonModule,
     FormsModule,
      ReactiveFormsModule,
    BoquploadRoutingModule,
    MatTableModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    NgxSpinnerModule,
  ]
})
export class BoquploadModule { }
