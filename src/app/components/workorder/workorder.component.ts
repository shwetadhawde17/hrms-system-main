import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from "@angular/forms";

import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ViewChild, AfterViewInit, TemplateRef } from '@angular/core';
import { ComponentsComponent } from '../components.component';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { mobileValidation, EmailValidation, panValidation,  gstinValidation} from '../../services/validations';


@Component({
  selector: 'app-workorder',
  templateUrl: './workorder.component.html',
  styleUrls: ['./workorder.component.scss']
})
export class WorkorderComponent implements OnInit {
  CreateWorkOrder: FormGroup;
  PlantList: any;
  CompanyList: any;
  ProjectList: any;submitted: any;
  contactPersonList= ['Minal', 'Riya', 'kiya']
  VendorList= ['Minal', 'Riya', 'kiya']
  MasterList = ['Material', 'Labour', 'Apply Supply']
  venderList:any;
  subCpList:any;
  CPNO:any;
  BOQList:any;
  displayedColumns: string[] = ['action','Srno','laborCode', 'descriptionofServices', 'Quantity','Unit','Rate','Amount'];
  dataSource = new MatTableDataSource<serviceRequestNote>(ELEMENT_DATA);
  @ViewChild(MatPaginator) paginator!:MatPaginator; 
  @ViewChild(MatSort) sort!:MatSort;


  constructor(private spinnerService: NgxSpinnerService, private commomFunction: ComponentsComponent, private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.CreateWorkOrder = this.fb.group({
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      projCode: [null, [Validators.required]],
      Masters: [null, [Validators.required]],
      workOrderNo: [null],
      date: [null, [Validators.required]],
      vendorselect: [null, [Validators.required]],
      pan: [null, [Validators.required,panValidation]],
      gstin: [null, [Validators.required,gstinValidation]],
      phone: [null, [Validators.required,mobileValidation]],
      contactPerson: [null, [Validators.required]],
      billingAddress: [null, [Validators.required]],
      detailAddress:[null, [Validators.required]],
      actGroupId:[null],
      actSeqNo:[null]
    })
  }

  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany();
  }
  public myError = (controlName: string, errorName: string) => {
    return this.CreateWorkOrder.controls[controlName].hasError(errorName);
  }
  async OnChangeCompany(cmpCode: any) {
    console.log('OnChangeCompany', cmpCode);
    if (cmpCode) {
      this.PlantList = await this.commomFunction.getPlant(cmpCode);
      this.venderList = await this.commomFunction.getVendor(cmpCode);

    } else {
      this.CreateWorkOrder.controls['plantCode'].setValue(null); this.PlantList = [];
      this.CreateWorkOrder.controls['vendorselect'].setValue(null); this.venderList = [];
    }
  }
  async OnChangeProject(cmpCode: any, plantCode: any, projCode: any) {
    console.log(cmpCode, plantCode, projCode)
   
      this.BOQList = await this.commomFunction.getBOQ(cmpCode, plantCode, projCode, false);
      console.log("BOQList",this.BOQList);
      this.BOQList= this.BOQList[0];

  }
  async OnChangeBOQHead(data:any){
    var found = await this.BOQList.find((item: any) => { if (item.actGroupId == data) { return item } })
    console.log('subMaterialList',found);
    if (found) { this.subCpList = found.details; this.CPNO = found.nActSeqNo; } else { this.subCpList = []; this.CPNO = null }
    console.log('subMaterialListqq',found);

  }

  //Vendor On Change Event Get Conntact persons
  async onChangeVendorCP(data: any) {
    var found = await this.venderList.find((item: any) => { if (item.bpCode == data) { return item } })
    if (found) { this.subCpList = found.contactPersons; this.CPNO = found.bpCode; } else { this.subCpList = []; this.CPNO = null }
    console.log('subMaterialList',found);

  }

  date = new FormControl(new Date()); serializedDate = new FormControl(new Date().toISOString());
  async onChangePlant(cmpCode: any, plantCode: any) {
    if (cmpCode && plantCode) {
      console.log(cmpCode, plantCode);
      this.ProjectList = await this.commomFunction.getproject(cmpCode, plantCode)
    }
    else {
      this.CreateWorkOrder.controls['projCode'].setValue(null); this.ProjectList = [];
      
    }
  }


  addWorkOrder() {
    console.log('this.CreateWorkOrder',this.CreateWorkOrder);
    console.log(this.CreateWorkOrder)
    if (this.CreateWorkOrder.invalid) {
      this.CreateWorkOrder.markAllAsTouched()
      this.submitted = true;
      return;
    }
    
   }

}
export interface serviceRequestNote{
  Srno:String;
  laborCode:string;
  descriptionofServices:string;
  Quantity:string;
  Unit:string;
  Rate:String;
  Amount:String;
}
const ELEMENT_DATA: serviceRequestNote[] = [
  {Srno: '1',laborCode: 'Labour 1', descriptionofServices: 'Unskilled Labour', Quantity: '10',Unit:'45',Rate: '100',Amount: '1000'},
  { Srno: '2',laborCode: 'Labour 2', descriptionofServices: 'Male Labour', Quantity: '20',Unit:'45',Rate: '200',Amount: '2000'},
  {Srno: '3', laborCode: 'Labour 3', descriptionofServices: 'Skilled Labour', Quantity: '30',Unit:'45',Rate: '350',Amount: '10500'},
  { Srno: '4',laborCode: 'Labour 4', descriptionofServices: 'Female Labour', Quantity: '40',Unit:'45',Rate: '300',Amount: '12000'},
  {Srno: '5', laborCode: 'Labour 5', descriptionofServices: 'Meson', Quantity: '50',Unit:'45',Rate: '500',Amount: '25000'},
 
];
