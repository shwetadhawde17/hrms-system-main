import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApproverRoutingModule } from './approver-routing.module';
import { ApproverComponent } from './approver.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [ApproverComponent],
  imports: [
    CommonModule,
    ApproverRoutingModule,
    NgxSpinnerModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ApproverModule { }
