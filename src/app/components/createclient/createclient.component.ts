import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormGroupDirective } from "@angular/forms";
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { mobileValidation, EmailValidation, panValidation, gstinValidation, ZipValidation } from '../../services/validations';
import { ComponentsComponent } from '../components.component';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { Inject } from '@angular/core';
@Component({
  selector: 'app-createclient',
  templateUrl: './createclient.component.html',
  styleUrls: ['./createclient.component.scss'],
  providers: [ComponentsComponent],
})
export class CreateclientComponent implements OnInit {
  createClient: FormGroup;
  viewtableClient:FormGroup;
  submitted: any;
  CompanyList: any;
  PlantList: any;
  CountryList: any;
  StateList: any;
  isActiveTab: number = 0;
  updateRow: any;
  BusinessPartnerList: any;
  displayedColumns = ['action', 'bpCode', 'bpFName', 'shortKey', 'pan', 'gstNumber', 'phone', 'email', 'street', 'city', 'postalCode'];
  CPRoleList = ["Commercial", "Technical"]
  dataSource!: MatTableDataSource<BPClientData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  isCreate: number = 0;
  deleteRow: any;
  AccountGroupList: any;

  @ViewChild(FormGroupDirective) myForm: any;

  constructor(
    private spinnerService: NgxSpinnerService, private commomFunction: ComponentsComponent, private dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createClient = this.fb.group({
      bpCode: [null],
      cmpCode: [null, [Validators.required]],
      actGroupCode: ["CL", [Validators.required]],
      bpFName: [null, [Validators.required,Validators.maxLength(50)]],
      shortKey: [null, [Validators.required,,Validators.maxLength(10)]],
      pan: [null, [Validators.required,panValidation]],
      gstNumber: [null, [Validators.required,gstinValidation]],
      addressLine1: [null, [Validators.maxLength(50)]],
      addressLine2: [null, [Validators.maxLength(50)]],
      street: [null, [Validators.required,Validators.maxLength(45)]],
      city: [null, [Validators.required,Validators.maxLength(45)]],
      postalCode: [null, [Validators.required, ZipValidation]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      status: true,
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      relationPersons: new FormArray([this.addContactPerson()]),
    });
    this.viewtableClient = this.fb.group({
      cmpCode: [null, [Validators.required]],
    })
  }
 
  // onNoClick(): void {
  //   this.dialogRef.close();
  // }
  async ngOnInit() {
    document.body.style.backgroundColor = "#E5E7E9";
    this.CompanyList = await this.commomFunction.getCompany();
    this.CountryList = await this.commomFunction.getCountry();
    // this.spinnerService.show();
    // this.BusinessPartnerList = await this.commomFunction.getBusinessPartners(environment.Cid, "CL");
    // 
    // if (this.BusinessPartnerList.length > 0) {
    //   // Assign the data to the data source for the table to render
    //   this.dataSource = new MatTableDataSource(this.BusinessPartnerList);
    //   this.dataSource.paginator = this.paginator;
    //   this.dataSource.sort = this.sort;
    // } else {
    //   this.BusinessPartnerList = [];
    //   this.spinnerService.hide();
    // }
    // this.spinnerService.hide();
  }

  //Form Group Error
  public myError = (controlName: string, errorName: string) => {
    return this.createClient.controls[controlName].hasError(errorName);
  }
  //Form Array error 
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.relationPersonsArr['controls'][i]['controls'][controlName].hasError(errorName)
  }

  // Mat Dialogs.
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef, {
      disableClose: true,
    });
  }

  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {
    
    if (value != change) {
      this.updateRow = null, value = 0
      this.createClient.reset();
      if(this.relationPersonsArr){
      while (this.relationPersonsArr.length !== 1) {
        <FormGroup>this.relationPersonsArr.removeAt(0)
      }}
      this.createClient.reset();
    }
    this.isActiveTab = value;
    this.isCreate = value;
    this.createClient.reset();
    this.myForm.resetForm();
    this.createClient.enable();

  }


  // Get relationPersonsArr Form Array
  get relationPersonsArr(): any {
    return <FormArray>this.createClient.get('relationPersons') as FormArray;
  }

  // Get relationPersonsArr Form Array
  relationPersonsArrFun(): any {
    return <FormArray>this.createClient.get('relationPersons') as FormArray;
  }
  //Form Array
  private addContactPerson(): FormGroup {
    return this.fb.group({
      cmpCode: [null],
      bpCode: [null],
      remark: [null, [Validators.required]],
      bpFName: [null, [Validators.required,Validators.maxLength(50)]],
      actGroupCode: ["CP", [Validators.required]],
      phone: [null, [Validators.required, mobileValidation]],
      email: [null, [Validators.required, EmailValidation]],
      status: ["1", [Validators.required]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null]
    })
  }

  // ADD Click event of addressArray Form Array
  addPerson() {
    let control = <FormArray>this.createClient.controls.relationPersons;
    control.push(
      this.fb.group({
        cmpCode: [null],
        bpCode: [null],
        remark: [null, [Validators.required]],
        bpFName: [null, [Validators.required]],
        actGroupCode: ["CP", [Validators.required]],
        phone: [null, [Validators.required, mobileValidation]],
        email: [null, [Validators.required, EmailValidation]],
        status: ["1", [Validators.required]],
        createBy: [null],
        createOn: [null],
        updateBy: [null],
        updateOn: [null],

      })
    );
  }

  //Delete Form array
  deletearr(i: any) {
    let control = <FormArray>this.createClient.controls.relationPersons;
    control.removeAt(i)
  }

  // company view table filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  async onChangeCountry(cmpCode: any) {
    if (cmpCode) { this.StateList = await this.commomFunction.getState(cmpCode); } else { this.createClient.controls['stateCode'].setValue(null); this.StateList = [] }
  }

  async onChangeCompany(data: any) {
    this.spinnerService.show();
    this.BusinessPartnerList = await this.commomFunction.getBusinessPartners(data, "CL");
    
    if (this.BusinessPartnerList.length > 0) {
      // Assign the data to the data source for the table to render
      this.dataSource = new MatTableDataSource(this.BusinessPartnerList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } else {
      this.BusinessPartnerList = [];
      this.spinnerService.hide();
    }
    this.spinnerService.hide();
  }

  addClient() {

    this.isActiveTab = 1;
    
    this.createClient.patchValue({ actGroupCode: "CL", status: true })
    // Patch CompanyCode PlantCode ProjectCode to Floor Array
    this.relationPersonsArr.value.forEach((element: any, index: any) => {
      (<FormGroup>this.relationPersonsArr.at(index)).patchValue({
        cmpCode: this.createClient.value.cmpCode,
        plantCode: this.createClient.value.plantCode,
        bpCode: this.createClient.value.bpCode,
        actGroupCode: "CP",
        createBy: environment.createBy,
        updateBy: environment.updateBy,
        status: true
      })
    })
    if (this.createClient.invalid) {
      this.submitted = true;
      this.createClient.markAllAsTouched()
      return;
    }
    

    var JSON = this.createClient.value;
    let Api = null;
    if (this.isCreate == 1) {
      JSON.relationPersons.forEach((element: any) => {
        if (!element.bpCode) {
          element.bpCode=this.createClient.value.bpCode
        }
      });
      Api = this.apiCallService.updateBusinessPartner, head = { cmpCode: this.updateRow.cmpCode, bpCode: this.updateRow.bpCode }
    } else {
      Api = this.apiCallService.addBusinessPartner;
    }
    var head = {};
    JSON.createBy = environment.createBy; // Loged IN User ID
    JSON.updateBy = environment.updateBy; // Loged IN User ID


    var data = { data: JSON, head: head }
    console.log('data',data);
    console.log(data)
    Api(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");

          this.BusinessPartnerList = await this.commomFunction.getBusinessPartners(JSON.cmpCode, "CL");
          this.tabIsActive(0, 0);
          if (this.BusinessPartnerList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.BusinessPartnerList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
          this.createClient.reset();

        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
  async editClient(row: any) {
    this.updateRow = row;
    
    if (this.relationPersonsArrFun().length > 0) {
      while (this.relationPersonsArrFun().length !== 0) {
        this.relationPersonsArrFun().removeAt(0)
      }
    }

    this.tabIsActive(1, 1);
    this.isCreate = 1;
    this.StateList = await this.commomFunction.getState(row.countryCode);
    // Use add form for update patch values
    this.createClient.patchValue({
      bpCode: this.updateRow.bpCode,
      cmpCode: this.updateRow.cmpCode,
      actGroupCode: this.updateRow.actGroupCode,
      bpFName: this.updateRow.bpFName,
      shortKey: this.updateRow.shortKey,
      pan: this.updateRow.pan,
      gstNumber: this.updateRow.gstNumber,
      addressLine1: this.updateRow.addressLine1,
      addressLine2: this.updateRow.addressLine2,
      street: this.updateRow.street,
      city: this.updateRow.city,
      postalCode: this.updateRow.postalCode,
      email: this.updateRow.email,
      phone: this.updateRow.phone,
      stateCode: this.updateRow.stateCode,
      countryCode: this.updateRow.countryCode,
      status: this.updateRow.status,
      createBy: this.updateRow.createBy,
      createOn: this.updateRow.createOn,
      updateBy: this.updateRow.updateBy,
      updateOn: this.updateRow.updateOn,
    });

    if (this.updateRow.relationPersons.length > 0) {

      this.updateRow.relationPersons = this.updateRow.relationPersons.filter(function (obj: any) { return !obj.isDeleted });
      

      this.updateRow.relationPersons.map((item: any, index: any) => {
        

        this.addPerson();
        this.relationPersonsArrFun().at(index).patchValue({
          cmpCode: item.cmpCode,
          bpCode: item.bpCode,
          remark: item.remark,
          bpFName: item.bpFName,
          actGroupCode: item.actGroupCode,
          phone: item.phone,
          email: item.email,
          status: item.status,
          createBy: item.createBy,
          createOn: item.createOn,
          updateBy: item.updateBy,
          updateOn: item.updateOn,
        })
      });
    }

  }
  async viewClient(row: any) {
    this.updateRow = row;
    if (this.relationPersonsArrFun().length > 0) {
      while (this.relationPersonsArrFun().length !== 0) {
        this.relationPersonsArrFun().removeAt(0)
      }
    }
    if (row.relationPersons.length > 0) {
      row.relationPersons.map((item: any) => { if (item.isDeleted === false) { this.addPerson() } });
      row.relationPersons = row.relationPersons.filter(function (obj: any) { return !obj.isDeleted });
    }
    this.tabIsActive(1, 1);
    this.isCreate = 2;
    this.StateList = await this.commomFunction.getState(row.countryCode);
    // Use add form for update patch values
    this.createClient.patchValue(row);
    this.createClient.disable();
  }
  deleteMe(row: any) {
    this.deleteRow = row;
    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  deleteClient() {
    var head = { cmpCode: this.deleteRow.cmpCode, bpCode: this.deleteRow.bpCode }
    var data = { head: head }
    this.apiCallService.deleteBusinessPartner(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Deleted Succefully!");
          const index = this.BusinessPartnerList.indexOf(this.deleteRow);
          if (index > -1) { // only splice array when item is found
            this.BusinessPartnerList.splice(index, 1); // 2nd parameter means remove one item only
          }
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.BusinessPartnerList);
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }


}




export interface BPClientData {
  bpCode: string,
  cmpCode: string,
  plantCode: string,
  actGroupCode: string,
  bpFName: string,
  bpMName: string,
  bpLName: string,
  shortKey: string,
  pan: string,
  gstNumber: string,
  addressLine1: string,
  addressLine2: string,
  street: string,
  city: string,
  postalCode: string,
  stateCode: string,
  countryCode: string,
  phone: string,
  email: string,
  status: string,
  createBy: string,
  createOn: string,
  updateBy: string,
  updateOn: string,
  isDeleted: string,
  relationPersons: string,
}
