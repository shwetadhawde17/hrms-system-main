import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServicerequestnoteComponent } from './servicerequestnote.component';

const routes: Routes = [{path:'',component:ServicerequestnoteComponent,children:[]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicerequestnoteRoutingModule { }
