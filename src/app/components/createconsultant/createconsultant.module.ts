import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateconsultantRoutingModule } from './createconsultant-routing.module';
import { CreateclientComponent } from '../createclient/createclient.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CreateconsultantComponent } from './createconsultant.component';


@NgModule({
  declarations: [CreateconsultantComponent],
  imports: [
    CommonModule,
    CreateconsultantRoutingModule,
    MatDialogModule,
    HttpClientModule,
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ]
})
export class CreateconsultantModule { }
