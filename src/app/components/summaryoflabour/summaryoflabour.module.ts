import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SummaryoflabourRoutingModule } from './summaryoflabour-routing.module';
import { SummaryoflabourComponent } from './summaryoflabour.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [SummaryoflabourComponent],
  imports: [
    CommonModule,
    SummaryoflabourRoutingModule,
    FormsModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ]
})
export class SummaryoflabourModule { }
