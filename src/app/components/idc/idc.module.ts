import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IDCRoutingModule } from './idc-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    IDCRoutingModule
  ]
})
export class IDCModule { }
