import { Component, OnInit } from '@angular/core';
import { ApiCallService } from '../services/api-call.service';
import { CommonService } from '../services/common.service';
import { promise } from 'protractor';
import { reject, result } from 'lodash';
import { resolve } from 'path';
import { error } from 'console';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.scss']
})
export class ComponentsComponent implements OnInit {

  constructor(private apiCallService: ApiCallService, private commonService: CommonService) { }

  ngOnInit(): void {
  }
  // ****************************** GET COMMON API's ******************************************* //

  // Get Company
  getCompany() {
    return new Promise((resolve, reject) => {
      var data = {}
      this.apiCallService.getCompany(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // Get Plant
  getPlant(cmpCode: any) {
    return new Promise((resolve, reject) => {
      if (cmpCode) {
        var data = { cmpCode: cmpCode }
        this.apiCallService.getPlantId(data).subscribe({
          next: async (result: any) => {
            if (result) {
              let res = await this.commonService.filterIsDeleted(result);
              resolve(res);
            }
          }, error: (error: any) => {
            this.commonService.showError(error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let res: any = [];
        resolve(res);
      }
    })
  }
  // get Country
  getCountry() {
    return new Promise((resolve, reject) => {
      var data = {}
      this.apiCallService.getCountry(data).subscribe({
        next: (result: any) => {
          if (result) {
            resolve(result)
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // Get State
  getState(CountryCode: any) {
    return new Promise((resolve, reject) => {
      if (CountryCode) {
        var param = { CountryCode: CountryCode }
        var data = { param: param }
        this.apiCallService.getState(data).subscribe({
          next: async (result: any) => {
            if (result) {
              resolve(result)
            }
          }, error: (error: any) => {
            this.commonService.showError(error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let res: any = [];
        resolve(res);
      }
    })
  }
  // Get Employee
  getEmployee(cmpCode: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode }
      var data = { param: param }
      this.apiCallService.getEmployeeId(data).subscribe({
        next: async (result: any) => {
          if (result) {
            // let res = await this.filterIsDeleted(result)
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // get consaltant || get client  
  getBusinessPartners(Cid: any, GP: any) {
    return new Promise((resolve, reject) => {
      if (Cid) {
        var param = { Cid: Cid, GP: GP }
        var data = { param: param }
        this.apiCallService.getBusinessPartnerByCompPlant(data).subscribe({
          next: async (result: any) => {
            if (result) {
              let res = await this.commonService.filterIsDeleted(result);

              resolve(res);
            }
          }, error: (error: any) => {
            this.commonService.showError(error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let res: any = [];
        resolve(res);
      }
    })
  }
  // get Project
  getproject(cmpCode: any, plantCode: any) {
    return new Promise((resolve, reject) => {
      var param = {}
      if (cmpCode && plantCode) { param = { cmpCode: cmpCode, plantCode: plantCode } }
      else if (cmpCode) { param = { cmpCode: cmpCode } } else { param = {} }
      var data = { param: param }
      this.apiCallService.getProject(data).subscribe({
        next: async (result: any) => {
          if (result) {
            // let res = await this.commonService.filterIsDeleted(result);
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // get Activity Head
  getActivityHead(cmpCode: any, activityCode: any) {
    return new Promise((resolve, reject) => {
      var head = { cmpCode: cmpCode }
      var param = { activityCode: activityCode }
      var data = { head: head, param: param }
      this.apiCallService.getActivityHead(data).subscribe({
        next: (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // get BOQ 
  getBOQ(cmpCode: any, plantCode: any, projCode: any, approver: any) {
    return new Promise((resolve, reject) => {
      var head = { cmpCode: cmpCode }
      var param = {}
      if (projCode) { param = { plantCode: plantCode, projCode: projCode, approver: approver } }
      var data = { head: head, param: param }
      this.apiCallService.getBOQUploadId(data).subscribe({
        next: (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // get IS BOQ Present
  getIsBOQPresent(cmpCode: any, plantCode: any, projCode: any) {
    return new Promise((resolve, reject) => {
      var head = { cmpCode: cmpCode, plantCode: plantCode, projCode: projCode }
      var data = { head: head }
      this.apiCallService.getISBOQPresent(data).subscribe({
        next: (result: any) => {
          resolve(result);
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // get Rate Analysis
  getRateAnalysis(cmpCode: any, plantCode: any, projCode: any, authority: any) {
    return new Promise((resolve, reject) => {
      var head = { cmpCode: cmpCode, plantCode: plantCode }
      var data = { head: head, param: { projCode: projCode, authority: authority } }
      console.log(data)
      this.apiCallService.getRateAnalysis(data).subscribe({
        next: (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // *************************************************************
  //                           Project Masters
  // *************************************************************
  // Project Execution Master11111111111111111111111111111111111111111111111111111111111111111
  getExecutaionList(cmpCode?: any) {
    return new Promise((resolve, reject) => {
      if (cmpCode) {
        var head = { cmpCode: cmpCode }
        var data = { head: head }

        this.apiCallService.getExecutionTypeById(data).subscribe({
          next: async (result: any) => {
            if (result) {
              // let res = await this.filterIsDeleted(result)
              resolve(result);
            }
          }, error: (error: any) => {
            this.commonService.showError(error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let res: any = [];
        resolve(res)
      }
    })
  }
  // Project Type Master
  getprojectTypeList(cmpCode?: any) {
    return new Promise((resolve, reject) => {
      if (cmpCode) {
        // var data = { cmpCode: cmpCode }
        var head = { cmpCode: cmpCode }
        var data = { head: head }
        this.apiCallService.getProjectTypeId(data).subscribe({
          next: async (result: any) => {
            if (result) {
              // let res = await this.filterIsDeleted(result)
              resolve(result);
            }
          }, error: (error: any) => {
            this.commonService.showError(error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let res: any = []
        resolve(res)
      }
    })
  }
  // Project Designation Master
  getdesignationList(cmpCode: any) {
    return new Promise((resolve, reject) => {
      if (cmpCode) {
        // var data = { cmpCode: cmpCode }
        var head = { cmpCode: cmpCode }
        var data = { head: head }
        this.apiCallService.getProjectDesignationById(data).subscribe({
          next: async (result: any) => {
            if (result) {
              // let res = await this.filterIsDeleted(result)
              resolve(result);
            }
          }, error: (error: any) => {
            this.commonService.showError(error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let res: any = []
        resolve(res)
      }
    })
  }

  // *************************************************************
  //                            Data Masters
  // *************************************************************
  // Unit Master
  getunitMaster(cmpCode?: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode }
      var data = { param: param }
      this.apiCallService.getunitMaster(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })

    })
  }
  // Account Group Master
  getAccountGroup(cmpCode: any) {
    return new Promise((resolve, reject) => {
      if (cmpCode) {
        var data = { cmpCode: cmpCode }
        this.apiCallService.getAccountGroup(data).subscribe({
          next: async (result: any) => {
            if (result) {
              // let res = await this.filterIsDeleted(result)
              resolve(result);
            }
          }, error: (error: any) => {
            this.commonService.showError(error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let res: any = [];
        resolve(res);
      }
    })
  }
  // Department Master
  // getDepartment(cmpCode: any) {
  //   return new Promise((resolve, reject) => {
  //     if (cmpCode) {
  //       var data = { cmpCode: cmpCode }
  //       this.apiCallService.getDepartment(data).subscribe({
  //         next: async (result: any) => {
  //           if (result) {
  //             // let res = await this.filterIsDeleted(result)
  //             resolve(result);
  //           }
  //         }, error: (error: any) => {
  //           this.commonService.showError(error);
  //           let res: any = [];
  //           resolve(res);
  //         }
  //       })
  //     } else {
  //       let res: any = [];
  //       resolve(res);
  //     }
  //   })
  // }
  // Employee Master
  // getDesignation(cmpCode: any) {
  //   return new Promise((resolve, reject) => {
  //     if (cmpCode) {
  //       var data = { cmpCode: cmpCode }
  //       this.apiCallService.getDesignation(data).subscribe({
  //         next: async (result: any) => {
  //           if (result) {
  //             // let res = await this.filterIsDeleted(result)
  //             resolve(result);
  //           }
  //         }, error: (error: any) => {
  //           this.commonService.showError(error);
  //           let res: any = [];
  //           resolve(res);
  //         }
  //       })
  //     } else {
  //       let res: any = [];
  //       resolve(res);
  //     }
  //   })
  // }
  // Labour By Company Master || All Labour Master
  getLabour(cmpCode: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode }
      var data = { param: param }

      this.apiCallService.getLabourMaster(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // Material Master By Company 
  getMaterial(cmpCode: any) {
    return new Promise((resolve, reject) => {

      var head = { cmpCode: cmpCode };
      var data = { head: head }

      this.apiCallService.getMaterialMasterById(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  //  Delete SUB Labour Array
  deleteSubLabour(cmpCode: any, labourID: any, labourDetailsID: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode, labourID: labourID, labourDetailsID: labourDetailsID }

      var data = { param: param }
      this.apiCallService.deleteSubLabour(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // Details  Material Delete
  deleteDetailsMaterial(cmpCode: any, materialID: any, materialSubID: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode, materialID: materialID, materialSubID: materialSubID }

      var data = { param: param }
      this.apiCallService.deleteDetailsMaterialSub(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // Actual Material Delete
  deleteActualMaterial(cmpCode: any, materialID: any, materialSubID: any, materialDetailID: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode, materialID: materialID, materialSubID: materialSubID, materialDetailID: materialDetailID }

      var data = { param: param }
      this.apiCallService.deleteActualMaterial(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // Apply Supply sub Array Delete
  deleteApplySupplySub(cmpCode: any, applySupplyID: any, applySupplyDetailsID: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode, applySupplyID: applySupplyID, applySupplyDetailsID: applySupplyDetailsID }

      var data = { param: param }
      this.apiCallService.deleteSubApplySupply(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // IDC Master By Company 
  getIDCActivity(cmpCode: any) {
    return new Promise((resolve, reject) => {

      var head = { cmpCode: cmpCode };
      var data = { head: head }

      this.apiCallService.getIDCActivityById(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // Details  IDC Delete
  deleteDetailsIDC(cmpCode: any, idcActivityID: any, idcSubHeadID: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode, idcActivityID: idcActivityID, idcSubHeadID: idcSubHeadID }

      var data = { param: param }
      this.apiCallService.deleteDetailsIDCSub(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // Actual Material Delete
  deleteActualIDC(cmpCode: any, idcActivityID: any, idcSubHeadID: any, idcDetailsID: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode, idcActivityID: idcActivityID, idcSubHeadID: idcSubHeadID, idcDetailsID: idcDetailsID }

      var data = { param: param }
      this.apiCallService.deleteActualIDC(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // vmm By Company Master || All vmm Master
  getVmm(cmpCode: any) {
    return new Promise((resolve, reject) => {
      // var head = { cmpCode: cmpCode }
      // var data = { head: head }
      var param = { cmpCode: cmpCode }
      var data = { param: param }
      this.apiCallService.getVmmMaster(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // vmm By Company Master || All vmm Master
  getMmm(cmpCode: any) {
    return new Promise((resolve, reject) => {
      // var head = { cmpCode: cmpCode }
      // var data = { head: head }
      var param = { cmpCode: cmpCode }
      var data = { param: param }
      this.apiCallService.getmmmMaster(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  // Apply Supply By Company Master
  getApplySupply(cmpCode: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode }
      var data = { param: param }
      this.apiCallService.getApplySupply(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  //Get By ID department API
  getDepartmentById(cmpCode: any) {
    return new Promise((resolve, reject) => {
      var head = { cmpCode: cmpCode }
      var data = { head: head }
      this.apiCallService.getDepartmentById(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
  getDesignationById(cmpCode: any) {
    return new Promise((resolve, reject) => {
      var head = { cmpCode: cmpCode }
      var data = { head: head }
      this.apiCallService.getDesignationById(data).subscribe({
        next: async (result: any) => {
          if (result) {
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // Get IDC Analysis
  getIDCAnalysis(cmpCode: any, plantCode: any, projCode: any, authority: any) {
    return new Promise((resolve, reject) => {
      var head = { cmpCode: cmpCode, plantCode: plantCode }
      var param = { projCode: projCode, authority: authority }
      var data = { head: head, param: param }
      this.apiCallService.getIDCAnalysisById(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // Labour By Company Master || All Labour Master
  getSOMLA(cmpCode: any, PlantCode: any, ProjCode: any, Type: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode, PlantCode: PlantCode, ProjCode: ProjCode, Type: Type }
      var data = { param: param }
      this.apiCallService.getSOMLA(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // Get Service Request Note 
  getServiceRequestNote(cmpCode: any, plantCode: any, projCode: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode, plantCode: plantCode, projCode: projCode }
      var data = { param: param }
      this.apiCallService.getServiceRequestNote(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [0];
          resolve(res)
        }
      })
    })
  }

  // Get Vendor
  getVendor(cmpCode: any) {
    return new Promise((resolve, reject) => {
      var param = { cmpCode: cmpCode }
      var data = { param: param }
      this.apiCallService.getVendor(data).subscribe({
        next: async (result: any) => {
          if (result) {
            // let res = await this.filterIsDeleted(result)
            let res = await this.commonService.filterIsDeleted(result);
            resolve(res);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  // MaterialRates || LabourRates || ApplySupplyRates
  getRateMasters(cmpCode: any, plantCode: any, projCode: any, URL: any) {
    return new Promise((resolve, reject) => {
      var head = { cmpCode: cmpCode, plantCode: plantCode, projCode: projCode }
      var param = { cmpCode: cmpCode }
      var data = { URI: URL, head: head, param: param }
      this.apiCallService.getRateMasters(data).subscribe({
        next: async (result: any) => {
          if (result) {
            resolve(result);
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }
}
