import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectMaterialLabourRoutingModule } from './project-material-labour-routing.module';
import { ProjectMaterialLabourComponent } from './project-material-labour.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [ProjectMaterialLabourComponent],
  imports: [
    CommonModule,
    ProjectMaterialLabourRoutingModule,
    FormsModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ]
})
export class ProjectMaterialLabourModule { }
