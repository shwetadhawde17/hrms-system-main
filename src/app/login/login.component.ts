
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../services/common.service';
import { ApiCallService } from '../services/api-call.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  loginForm: FormGroup;
  constructor(public fb: FormBuilder, private router: Router, private commanService: CommonService, private apiService: ApiCallService) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    })
  }
  ngOnInit() {
    document.body.style.backgroundColor = "#E5E7E9";
  }

  ngOnDestroy() {
    document.body.style.background = '#fff';
  }
  submitForm() {
  
    if (this.loginForm.valid) {
      console.log("form submitted details", this.loginForm.value);
     if(this.loginForm.value.email == 'sourabh@abc.com' && this.loginForm.value.password=='Sourabh@123')
     {
      this.commanService.showSuccess('LogedIn Successfully !!');
      this.router.navigate(['components']);
     }else
     {
      this.commanService.showError('Incorrect Credentials !!')
     }
    }
    else {
      this.commanService.showError('Fill out all the details')
    }

  }
}



