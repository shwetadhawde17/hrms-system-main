import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DDSRoutingModule } from './dds-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DDSRoutingModule
  ]
})
export class DDSModule { }
