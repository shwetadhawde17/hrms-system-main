import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateclientComponent } from './createclient.component';

const routes: Routes = [
  { path: '', component: CreateclientComponent, children: [] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateclientRoutingModule { }
