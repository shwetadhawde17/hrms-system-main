import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectmasterRoutingModule } from './projectmaster-routing.module';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { ProjectmasterComponent } from './projectmaster.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsRoutingModule } from '../components-routing.module';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [ProjectmasterComponent],
  imports: [
    CommonModule,
    ProjectmasterRoutingModule,
    MatPaginatorModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ]
})
export class ProjectmasterModule { }
