import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsComponent } from './components.component';

const routes: Routes = [
  {
    path: '', component: ComponentsComponent, children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(
          module => module.DashboardModule)
      },
      {
        path: 'createcompany',
        loadChildren: () => import('./createcompany/createcompany.module').then(
          module => module.CreatecompanyModule)
      },
      {
        path: 'createclient',
        loadChildren: () => import('./createclient/createclient.module').then(
          module => module.CreateclientModule)
      },
      {
        path: 'createconsultant',
        loadChildren: () => import('./createconsultant/createconsultant.module').then(
          module => module.CreateconsultantModule)
      },
      {
        path: 'createemployee',
        loadChildren: () => import('./createemployee/createemployee.module').then(
          module => module.CreateemployeeModule)
      },
      {
        path: 'projectcreation',
        loadChildren: () => import('./projectcreation/projectcreation.module').then(
          module => module.ProjectcreationModule)
      },

      {
        path: 'projectmaster',
        loadChildren: () => import('./projectmaster/projectmaster.module').then(
          module => module.ProjectmasterModule)
      },
      {
        path: 'boqupload',
        loadChildren: () => import('./boqupload/boqupload.module').then(
          module => module.BoquploadModule)
      },
      {
        path: 'datamaster',
        loadChildren: () => import('./datamaster/datamaster.module').then(
          module => module.DatamasterModule)
      },
      {
        path: 'vendorcreation',
        loadChildren: () => import('./vendorcreation/vendorcreation.module').then(
          module => module.VendorcreationModule)
      },
      {
        path: 'projectrateallocation',
        loadChildren: () => import('./project-material-labour/project-material-labour.module').then(
          module => module.ProjectMaterialLabourModule)
        },
        {
          path: 'rateanalysis',
          loadChildren: () => import('./prestartestimation/prestartestimation.module').then(
            module => module.PrestartestimationModule)
        },{
          path: 'approver',
          loadChildren: () => import('./approver/approver.module').then(
            module => module.ApproverModule)
        },
        {
          path: 'idc',
          loadChildren: () => import('./idc/idc.module').then(
            module => module.IDCModule)
        },
        {
          path: 'dds',
          loadChildren: () => import('./dds/dds.module').then(
            module => module.DDSModule)
        },
        {
          path:'servicerequestnote',
          loadChildren: () => import('./servicerequestnote/servicerequestnote.module').then(
            module => module.ServicerequestnoteModule)
        },
        {
          path:'summaryoflabour',
          loadChildren: () => import('./summaryoflabour/summaryoflabour.module').then(
            module => module.SummaryoflabourModule)
        },
        {
          path:'workorder',
          loadChildren: () => import('./workorder/workorder.module').then(
            module => module.WorkorderModule)
        }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
