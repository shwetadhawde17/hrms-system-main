import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective } from "@angular/forms";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, MatSortModule, MatSortHeader } from '@angular/material/sort'
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ComponentsComponent } from '../components.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { JsonToXlsxService } from 'src/app/services/json-to-xlsx.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-datamaster',
  templateUrl: './datamaster.component.html',
  styleUrls: ['./datamaster.component.scss']
})
export class DatamasterComponent implements OnInit {
  panelOpenState = false
  step = 0;


  datamaster: FormGroup;
  viewMasterData: FormGroup;
  modalDescription: FormGroup;
  isActiveTab: number = 0;
  updateRow: any;
  isCreate: number = 0;
  submitted: any;
  length: any;
  unitList: any;
  projectmasterList: any = [];
  uploadDataMaster: FormGroup;
  displayedColumns: string[] = [];
  materialDetailsget: any;
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  @ViewChild('viewDialog') viewDialog!: TemplateRef<any>;
  deleteRow: any;
  CompanyList: any;
  labourList: any;
  vmmList: any;
  mmmList: any;
  MaterialList: any;
  convertedJSON: any = [];
  applySupplyList: any;
  subMaterialList: any;
  modaldata: any;
  grouopindex: any;
  MaterialGroupDesc: any;
  UploadJSON: any = [];
  IDCList: any = [];
  subIDCList: any = [];

  DatamasterList = [{ code: 'departmentDetails', name: 'Employee Department' }, { code: 'empDesignation', name: 'Employee Designation' },
  { code: 'unitMaster', name: 'Unit Master' }, { code: 'labour', name: 'Labour' }, { code: 'material', name: 'Material' },
  { code: 'applysupply', name: 'Apply supply' }, { code: 'idc', name: 'IDC' },
    //  { code: 'miscellaneous', name: 'Miscellaneous' },
    // { code: 'vmm', name: 'VMM' }, { code: 'mmm', name: 'MMM' },
  ]

  uploadDatamasterList = [{ code: 'departmentDetails', name: 'Employee Department' }, { code: 'empDesignation', name: 'Employee Designation' },
  { code: 'unitMaster', name: 'Unit Master' }, { code: 'labour', name: 'Labour Group' }, { code: 'labourDetails', name: 'Labour Details' },
  { code: 'material', name: 'Material Group' }, { code: 'materialDetails', name: 'Material Details' }, { code: 'materialActualGroup', name: 'Material Actual Group' },
  { code: 'applySupply', name: 'Apply supply Group' }, { code: 'applySupplyDetails', name: 'Apply supply Details' },
  { code: 'idc', name: 'IDC Group' }, { code: 'idcDetails', name: 'IDC Details' }, { code: 'idcActualGroup', name: 'IDC Actual Group' },
    // { code: 'miscellaneous', name: 'Miscellaneous' },
    // { code: 'vmm', name: 'VMM' }, { code: 'mmm', name: 'MMM' },
  ]



  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  //filter data  Mat table datasource paginator sorting 
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(FormGroupDirective) myForm: any;
  dataSource!: MatTableDataSource<masterData>;
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  constructor(private JsonToXlsxService: JsonToXlsxService, private commomFunction: ComponentsComponent, private spinnerService: NgxSpinnerService, private commonFunction: ComponentsComponent, private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.datamaster = this.fb.group({
      masterType: [null, [Validators.required]],
      cmpCode: [null, [Validators.required]],
    })

    this.viewMasterData = this.fb.group({
      Masters: [null, [Validators.required]],
      cmpCode: [null, [Validators.required]],
    })
    this.modalDescription = this.fb.group({
      cmpCode: null,
      descripption: [null, [Validators.required]],

    })
    this.uploadDataMaster = this.fb.group({
      cmpCode: [null, [Validators.required]],
      uploadMastersType: [null, [Validators.required]],
      FileUpload: null
    })
  }

  async ngOnInit() {

    this.CompanyList = await this.commomFunction.getCompany();

    this.datamaster.controls['cmpCode'].valueChanges.subscribe((value) => {
      this.onChangeCompany(value)
    });

    // this.datamaster.controls['materialID'].valueChanges.subscribe((value) => {
    //   
    //   this.onChangeMaterial(value)
    // });
    // this.materials().valueChanges.subscribe((value:any) => {
    //   if (this.materials().length > 0) {
    //     this.materials().forEach((element: any) => {
    //       
    //     });
    //   }
    // });
    // this.datamaster.controls['subMaterial'].valueChanges.subscribe((value: any) => {
    //   // 
    // })

  }

  // **********************************************************************************************************
  //                                       Labour Section
  // **********************************************************************************************************

  //get labour Form Array
  labour(): any {
    return <FormArray>this.datamaster.get('labourDetails') as FormArray;
  }
  get labourArr(): any {
    return <FormArray>this.datamaster.get('labourDetails') as FormArray;
  }
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.labour()['controls'][i]['controls'][controlName].hasError(errorName)
  }
  //Add labour contact person
  addLabour() {
    this.labour().push(this.labourDataGroup());
  }
  //Remove labour contact person
  async removeLabour(i: number) {
    
    if (this.labour().controls[i].value.cmpCode && this.labour().controls[i].value.labourID) {
      var dt = await this.commomFunction.deleteSubLabour(this.labour().controls[i].value.cmpCode, this.labour().controls[i].value.labourID, this.labour().controls[i].value.labourDetailsID);
      if (dt) { this.commonService.showSuccess("Record deleted successfully!") } else {

      }
    }
    this.labour().removeAt(i);


  }

  //client contact person Form Array function
  labourDataGroup(): FormGroup {
    return this.fb.group({
      labourDetailsID: null,
      labourID: null,
      cmpCode: null,
      descripption: [null, Validators.maxLength(1000)],
      StandardrUnitCode: null
    });
  }
  // **********************************************************************************************************
  //                                        Materials Section
  // **********************************************************************************************************

  //get material Form Array
  materials(): any {
    return <FormArray>this.datamaster.get('subMaterial') as FormArray;
  }
  // add material Form Array
  addMaterial() {
    this.materials().push(this.materialDataGroup());
  }
  // remove material Form Array
  async removeSubMaterial(i: number) {
    
    if (this.materials().controls[i].value.cmpCode && this.materials().controls[i].value.materialID) {
      var dt = await this.commomFunction.deleteDetailsMaterial(this.materials().controls[i].value.cmpCode, this.materials().controls[i].value.materialID, this.materials().controls[i].value.materialSubID);
      if (dt) { this.commonService.showSuccess("Record deleted successfully!") } else {
        // this.commonService.showError("Record not deleted!")
      }
    }
    this.materials().removeAt(i);
  }

  //material Form group Array function
  materialDataGroup(): FormGroup {
    return this.fb.group({
      materialSubID: null,
      materialID: [null],
      cmpCode: null,
      descripption: [null, Validators.maxLength(10)],
      materialDetails: new FormArray([
        // this.SubStoreGroup()
      ])
    });
  }
  //get Sub store Form Array
  MaterialDet(index: number): FormArray {
    return <FormArray>this.materials().at(index).get('materialDetails') as FormArray;
  }
  //  public myErrorMaterial = (controlName: string, errorName: string, i: any) => {
  //   return this.MaterialDet().controls[j][controlName].hasError(errorName)
  // }
  //add Sub store Form Array
  addSubMaterial(index: number) {
    this.MaterialDet(index).push(this.MaterialDetailsGroup());
  }
  //remove Sub store Form Array
  async removeMaterialDet(i: number, j: number) {
    if (this.MaterialDet(i).controls[j].value.cmpCode && this.MaterialDet(i).controls[j].value.materialID && this.MaterialDet(i).controls[j].value.materialSubID && this.MaterialDet(i).controls[j].value.materialDetailID) {
      var dt = await this.commomFunction.deleteActualMaterial(this.MaterialDet(i).controls[j].value.cmpCode, this.MaterialDet(i).controls[j].value.materialID, this.MaterialDet(i).controls[j].value.materialSubID, this.MaterialDet(i).controls[j].value.materialDetailID)
      if (dt) { this.commonService.showSuccess("Record deleted successfully!") } else {
        // this.commonService.showError("Record not deleted!")
      }
    }
    this.MaterialDet(i).removeAt(j);

  }
  //Sub store Form Array function
  private MaterialDetailsGroup(): FormGroup {
    return this.fb.group({
      materialDetailID: null,
      materialSubID: null,
      StandardrUnitCode: null,
      materialID: null,
      cmpCode: null,
      descripption: null,
    })
  }
  // **********************************************************************************************************
  //                                        Apply Supply Section
  // **********************************************************************************************************

  //get apply supply Form Array
  applysupply(): any {
    return <FormArray>this.datamaster.get('applySupplyDetails') as FormArray;
  }
  //Add apply supply contact person
  addapplysupply() {
    this.applysupply().push(this.applysupplyDataGroup());
  }
  public myErrorApplySupply = (controlName: string, errorName: string, i: any) => {
    return this.applysupply()['controls'][i]['controls'][controlName].hasError(errorName)
  }
  //Remove applysupply contact person
  async removeapplysupply(i: number) {
    
    if (this.applysupply().controls[i].value.cmpCode && this.applysupply().controls[i].value.applySupplyID) {
      var dt = await this.commomFunction.deleteApplySupplySub(this.applysupply().controls[i].value.cmpCode, this.applysupply().controls[i].value.applySupplyID, this.applysupply().controls[i].value.applySupplyDetailsID);
      if (dt) { this.commonService.showSuccess("Record deleted successfully!") } else {
        // this.commonService.showError("Record not deleted!")
      }
    }
    this.applysupply().removeAt(i);
  }

  //apply supply Form Array function
  applysupplyDataGroup(): FormGroup {
    return this.fb.group({
      applySupplyDetailsID: null,
      applySupplyID: null,
      cmpCode: null,
      descripption: [null, Validators.maxLength(1000)],
      StandardrUnitCode: null,
    });
  }

  // **********************************************************************************************************
  //                                       In-direct Cost Section
  // **********************************************************************************************************

  //get Store Form Array
  idc(): any {
    return <FormArray>this.datamaster.get('subHead') as FormArray;
  }
  //Add idc 
  addIDC() {
    this.idc().push(this.IDCDataGroup());
  }
  //Remove IDC 
  async removeIDC(i: any) {
    
    if (this.idc().controls[i].value.cmpCode && this.idc().controls[i].value.idcActivityID) {
      var dt = await this.commomFunction.deleteDetailsIDC(this.idc().controls[i].value.cmpCode, this.idc().controls[i].value.idcActivityID, this.idc().controls[i].value.idcSubHeadID);
      if (dt) { this.commonService.showSuccess("Record deleted successfully!") } else {
        // this.commonService.showError("Record not deleted!")
      }
    }

    this.idc().removeAt(i);
  }
  //IDC subhead Form Array function
  IDCDataGroup(): FormGroup {
    return this.fb.group({
      idcSubHeadID: [null, [Validators.required]],
      idcActivityID: null,
      cmpCode: null,
      description: null,
      idcDetails: new FormArray([
        // this.SubStoreGroup()
      ])
    });
  }

  //get Sub store Form Array
  idcActualArray(index: number): FormArray {
    return <FormArray>this.idc().at(index).get('idcDetails') as FormArray;
  }
  //add Sub store Form Array
  addSubActualIDC(index: number) {
    this.idcActualArray(index).push(this.IDCActualGroup());
  }
  //remove Sub store Form Array
  async removeIDCActual(i: number, j: number) {
    if (this.idcActualArray(i).controls[j].value.cmpCode && this.idcActualArray(i).controls[j].value.idcActivityID && this.idcActualArray(i).controls[j].value.idcSubHeadID && this.idcActualArray(i).controls[j].value.idcDetailsID) {
      var dt = await this.commomFunction.deleteActualIDC(this.idcActualArray(i).controls[j].value.cmpCode, this.idcActualArray(i).controls[j].value.idcActivityID, this.idcActualArray(i).controls[j].value.idcSubHeadID, this.idcActualArray(i).controls[j].value.idcDetailsID)
      if (dt) { this.commonService.showSuccess("Record deleted successfully!") } else {
        // this.commonService.showError("Record not deleted!")
      }
    }
    this.idcActualArray(i).removeAt(j);
    // this.idcActualArray(0).removeAt(j);
  }
  //Sub store Form Array function
  private IDCActualGroup(): FormGroup {
    return this.fb.group({
      idcActivityID: null,
      idcSubHeadID: null,
      idcDetailsID: null,
      cmpCode: null,
      description: null
    })
  }

  // **********************************************************************************************************
  //                                       Miscellaneous Section
  // **********************************************************************************************************

  //get apply supply Form Array
  miscellaneous(): any {
    return <FormArray>this.datamaster.get('miscellaneousArray') as FormArray;
  }
  //Add apply supply contact person
  addmiscellaneous() {
    this.miscellaneous().push(this.miscellaneousDataGroup());
  }
  //Remove applysupply contact person
  removemiscellaneous(index: number) {
    this.miscellaneous().removeAt(index);
  }
  //apply supply Form Array function
  miscellaneousDataGroup(): FormGroup {
    return this.fb.group({
      miscellaneousSubID: null,
      miscellaneousId: null,
      cmpCode: null,
      descripption: null,
    });
  }
  //Vehicle machinary maintenance

  //get vmm Form Array
  vmm(): any {
    return <FormArray>this.datamaster.get('details') as FormArray;
  }

  public vmErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.vmm()['controls'][i]['controls'][controlName].hasError(errorName)
  }
  //Add labour contact person
  addVmm() {
    this.vmm().push(this.vmmDataGroup());
  }
  //Remove vmm contact person
  removeVmm(i: any) {
    this.vmm().removeAt(i);
  }
  //client contact person Form Array function
  vmmDataGroup(): FormGroup {
    return this.fb.group({
      VmmDetailsID: null,
      vmmID: null,
      cmpCode: null,
      descripption: null,
      StandardrUnitCode: null
    });
  }



  //get vmm Form Array
  materialmm(): any {
    return <FormArray>this.datamaster.get('details') as FormArray;
  }

  public mmErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.materialmm()['controls'][i]['controls'][controlName].hasError(errorName)
  }
  //Add labour contact person
  addMmm() {
    this.materialmm().push(this.mmmDataGroup());
  }
  //Remove mmm contact person
  removeMmm(i: any) {
    this.materialmm().removeAt(i);
  }
  //client contact person Form Array function
  mmmDataGroup(): FormGroup {
    return this.fb.group({
      mmmDetailsID: null,
      mmmID: null,
      cmpCode: null,
      description: null,
      StandardrUnitCode: null
    });
  }

  async downloadFile() {
    var arr = [];
    var headersArray: any = [];
    var fileName = ""
    if (this.uploadDataMaster.value.uploadMastersType && this.uploadDataMaster.value.cmpCode) {
      if (this.uploadDataMaster.value.uploadMastersType == 'unitMaster' && this.uploadDataMaster.value.cmpCode) {
        for (var L = 1, LLen = 21; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadDataMaster.value.cmpCode, standardrUnitCode: "", unitDesc: "", unitCode1: "", unitCode2: "",
            unitCode3: ""
          })
        }
        headersArray = ['Company Code', 'Standard Unit Code', 'Unit Description', 'Unit Code1', 'Unit Code2', 'Unit Code3'];
        fileName = "Unit Master"

      }
      //  else if (this.uploadDataMaster.value.uploadMastersType == 'accountGroup' && this.uploadDataMaster.value.cmpCode) {
      //   for (var L = 1, LLen = 21; L < LLen; L++) {
      //     arr.push({
      //       cmpCode: this.uploadDataMaster.value.cmpCode, actGroupCode: "", actGroupDesc: ""
      //     })
      //   }
      //   headersArray = ['Company Code', 'Account Group Code', 'Account Group Description'];
      //   fileName = "Account Group Master"

      // }

      else if (this.uploadDataMaster.value.uploadMastersType == 'departmentDetails' && this.uploadDataMaster.value.cmpCode) {
        for (var L = 1, LLen = 21; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadDataMaster.value.cmpCode, deptDesc: ""
          })
        }
        headersArray = ['Company Code', 'Department Description'];
        fileName = "Employee Department Master"

      }
      else if (this.uploadDataMaster.value.uploadMastersType == 'empDesignation' && this.uploadDataMaster.value.cmpCode) {
        for (var L = 1, LLen = 21; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadDataMaster.value.cmpCode, designationDesc: ""
          })
        }
        headersArray = ['Company Code', 'Employee Designation'];
        fileName = "Employee Designation "

      }
      else if (this.uploadDataMaster.value.uploadMastersType == 'labour' && this.uploadDataMaster.value.cmpCode) {
        for (var L = 1, LLen = 21; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadDataMaster.value.cmpCode, descripption: ""
          })
        }
        headersArray = ['Company Code', 'Labour Description'];
        fileName = "Labour Group Master"

      }

      else if (this.uploadDataMaster.value.uploadMastersType == 'labourDetails' && this.uploadDataMaster.value.cmpCode) {
        this.labourList = await this.commomFunction.getLabour(this.uploadDataMaster.value.cmpCode);
        
        if (this.labourList.length > 0) {
          for (let L = 0, LLen = this.labourList.length; L < LLen; L++) {
            arr.push({ cmpCode: this.uploadDataMaster.value.cmpCode, labourID: this.labourList[L].labourID, desc: this.labourList[L].descripption, descripption: "", standardrUnitCode: "" })
          }
        }
        // for (var L = 1, LLen = 21; L < LLen; L++) {
        //   arr.push({
        //     cmpCode: this.uploadDataMaster.value.cmpCode, labourID: "", descripption: "", standardrUnitCode: ""
        //   })
        // }
        headersArray = ['Company Code', 'Labour ID', 'Labour Group Descripption', 'Labour Details Descripption', 'Standard Unit Code'];
        fileName = "Labour Detail Master"

      }

      else if (this.uploadDataMaster.value.uploadMastersType == 'material' && this.uploadDataMaster.value.cmpCode) {
        for (var L = 1, LLen = 21; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadDataMaster.value.cmpCode, descripption: "",
          })
        }
        headersArray = ['Company Code', 'Material Group Description'];
        fileName = "Material Group Master"

      }
      else if (this.uploadDataMaster.value.uploadMastersType == 'materialDetails' && this.uploadDataMaster.value.cmpCode) {
        this.MaterialList = await this.commomFunction.getMaterial(this.uploadDataMaster.value.cmpCode);
        
        if (this.MaterialList.length > 0) {
          for (let L = 0, LLen = this.MaterialList.length; L < LLen; L++) {
            arr.push({ cmpCode: this.uploadDataMaster.value.cmpCode, materialID: this.MaterialList[L].materialID, desc: this.MaterialList[L].descripption, descripption: "" })
          }
        }
        // for (var L = 1, LLen = 21; L < LLen; L++) {
        //   arr.push({
        //     cmpCode: this.uploadDataMaster.value.cmpCode, materialID: "", descripption: "",
        //   })
        // }
        headersArray = ['Company Code', 'Material Group Id', 'Material Group Description', 'Material Description'];
        fileName = "Material Details Master"

      }
      else if (this.uploadDataMaster.value.uploadMastersType == 'materialActualGroup' && this.uploadDataMaster.value.cmpCode) {
        this.MaterialList = await this.commomFunction.getMaterial(this.uploadDataMaster.value.cmpCode);
        if (this.MaterialList.length > 0) {
          for (let L = 0, LLen = this.MaterialList.length; L < LLen; L++) {
            if (this.MaterialList[L].subMaterial.length > 0) {
              for (let M = 0, MLen = this.MaterialList[L].subMaterial.length; M < MLen; M++) {
                arr.push({ cmpCode: this.uploadDataMaster.value.cmpCode, materialID: this.MaterialList[L].materialID, desc: this.MaterialList[L].descripption, materialSubID: this.MaterialList[L].subMaterial[M].materialSubID, materialSubDescription: this.MaterialList[L].subMaterial[M].descripption, descripption: "", standardrUnitCode: "" })
              }
            }
          }
        }
        // for (var L = 1, LLen = 21; L < LLen; L++) {
        //   arr.push({
        //     cmpCode: this.uploadDataMaster.value.cmpCode, materialID: "", materialSubID: "", materialDetailID: "", standardrUnitCode: ""
        //   })
        // }
        headersArray = ['Company Code', 'Material Group Id', 'Material Group Description', 'Material Sub ID', 'Material Sub Description', 'Material Actual Description', 'Standard Unit Code'];
        fileName = "Material Actual Group Master"

      }

      else if (this.uploadDataMaster.value.uploadMastersType == 'applySupply' && this.uploadDataMaster.value.cmpCode) {
        for (var L = 1, LLen = 21; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadDataMaster.value.cmpCode, descripption: "",
          })
        }
        headersArray = ['Company Code', 'Apply Supply Descripption',];
        fileName = "Apply Supply Group Master"

      }
      else if (this.uploadDataMaster.value.uploadMastersType == 'applySupplyDetails' && this.uploadDataMaster.value.cmpCode) {
        this.applySupplyList = await this.commomFunction.getApplySupply(this.uploadDataMaster.value.cmpCode);
        
        if (this.applySupplyList.length > 0) {
          for (let L = 0, LLen = this.applySupplyList.length; L < LLen; L++) {
            arr.push({ cmpCode: this.uploadDataMaster.value.cmpCode, applySupplyID: this.applySupplyList[L].applySupplyID, desc: this.applySupplyList[L].descripption, descripption: "", standardrUnitCode: "" })
          }
        }
        // for (var L = 1, LLen = 21; L < LLen; L++) {
        //   arr.push({
        //     cmpCode: this.uploadDataMaster.value.cmpCode, applySupplyID: "", descripption: "", standardrUnitCode: ""
        //   })
        // }
        headersArray = ['Company Code', 'Apply Supply ID', 'Apply Supply Description', 'Apply Supply Details Descripption', 'Standard Unit Code'];
        fileName = "Apply Supply Detail Master"

      }
      else if (this.uploadDataMaster.value.uploadMastersType == 'idc' && this.uploadDataMaster.value.cmpCode) {
        for (var L = 1, LLen = 21; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadDataMaster.value.cmpCode, descripption: "",
          })
        }
        headersArray = ['Company Code', 'IDC Group Description'];
        fileName = "IDC Group Master"

      }
      else if (this.uploadDataMaster.value.uploadMastersType == 'idcDetails' && this.uploadDataMaster.value.cmpCode) {
        this.IDCList = await this.commomFunction.getIDCActivity(this.uploadDataMaster.value.cmpCode);
        
        if (this.IDCList.length > 0) {
          for (let L = 0, LLen = this.IDCList.length; L < LLen; L++) {
            arr.push({ cmpCode: this.uploadDataMaster.value.cmpCode, idcActivityID: this.IDCList[L].idcActivityID, desc: this.IDCList[L].description, description: "" })
          }
        }
        // for (var L = 1, LLen = 21; L < LLen; L++) {
        //   arr.push({
        //     cmpCode: this.uploadDataMaster.value.cmpCode, idcActivityID: "", descripption: "",
        //   })
        // }
        headersArray = ['Company Code', 'IDC Group Id', 'IDC Group Description', 'IDC Detail Description'];
        fileName = "IDC Details Master"

      }
      else if (this.uploadDataMaster.value.uploadMastersType == 'idcActualGroup' && this.uploadDataMaster.value.cmpCode) {
        this.IDCList = await this.commomFunction.getIDCActivity(this.uploadDataMaster.value.cmpCode);
        if (this.IDCList.length > 0) {
          for (let L = 0, LLen = this.IDCList.length; L < LLen; L++) {
            if (this.IDCList[L].subHead.length > 0) {
              for (let M = 0, MLen = this.IDCList[L].subHead.length; M < MLen; M++) {
                arr.push({ cmpCode: this.uploadDataMaster.value.cmpCode, idcActivityID: this.IDCList[L].idcActivityID, desc: this.IDCList[L].description, idcSubHeadID: this.IDCList[L].subHead[M].idcSubHeadID, idcSubDescription: this.IDCList[L].subHead[M].description, description: "", })
              }
            }
          }
        }
        // for (var L = 1, LLen = 21; L < LLen; L++) {
        //   arr.push({
        //     cmpCode: this.uploadDataMaster.value.cmpCode, idcActivityID: "", IDCSubID: "", IDCDetailID: "", standardrUnitCode: ""
        //   })
        // }
        headersArray = ['Company Code', 'IDC Group Id', 'IDC Group Description', 'IDC Sub ID', 'IDC Sub Group Description', 'IDC Actual Description'];
        fileName = "IDC Actual Group Master"

      }

      if (arr.length > 0) {
        this.JsonToXlsxService.exportExcelWithoutHead(arr, headersArray, fileName);
      } else {
        this.commonService.showError("Master Data not found!")
      }
    }
    else {
      this.commonService.showError("Plese select Masters and Company!")
    }
  }


  async selectFile(event: any) {
    var files = event.target.files;
    
    this.spinnerService.show();
    if (files.length == 0) {
      this.commonService.showError("Please select excel file Ex.(.XLS,.XLSX).");
      this.spinnerService.hide();
      return;

    }

    this.uploadDataMaster.patchValue({ FileUpload: files[0].name })
    var filename = files[0].name;

    var extension = filename.substring(filename.lastIndexOf(".")).toUpperCase();
    if (extension == '.XLS' || extension == '.XLSX') {

      const fileReader = new FileReader();
      fileReader.readAsBinaryString(files[0])
      fileReader.onload = (event: any) => {
        const BinaryData = event.target['result'];
        let workbook = XLSX.read(BinaryData, { type: 'binary' })
        // Read multiple sheets here ...
        workbook.SheetNames.forEach(async (sheetsName: any) => {
          const worksheet = workbook.Sheets[sheetsName];
          let xlsData: any = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

          var arr: any = [];
          if (this.uploadDataMaster.value.uploadMastersType == 'unitMaster') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], standardrUnitCode: xlsData[i][1], unitDesc: xlsData[i][2], unitCode1: xlsData[i][3], unitCode2: xlsData[i][4], unitCode3: xlsData[i][5] })
            }
            this.UploadJSON = await this.unitMasterSort(arr);
            this.spinnerService.hide();
          }
          if (this.uploadDataMaster.value.uploadMastersType == 'departmentDetails') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], deptDesc: xlsData[i][1] })
            }
            this.UploadJSON = await this.departmentSort(arr);
            this.spinnerService.hide();
          }
          if (this.uploadDataMaster.value.uploadMastersType == 'empDesignation') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], designationDesc: xlsData[i][1] })
            }
            this.UploadJSON = await this.empDesignationSort(arr);
            this.spinnerService.hide();

          }

          if (this.uploadDataMaster.value.uploadMastersType == 'labour') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], descripption: xlsData[i][1] })
            }
            this.UploadJSON = await this.labourGroupSort(arr);
            this.spinnerService.hide();

          }
          if (this.uploadDataMaster.value.uploadMastersType == 'labourDetails') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], LabourID: xlsData[i][1], descripption: xlsData[i][3], StandardrUnitCode: xlsData[i][4] })
            }

            this.UploadJSON = await this.labourDetailsSort(arr);
            this.spinnerService.hide();

          }

          if (this.uploadDataMaster.value.uploadMastersType == 'material') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], descripption: xlsData[i][1] })
            }
            this.UploadJSON = await this.materialGroupSort(arr);
            this.spinnerService.hide();

          }
          if (this.uploadDataMaster.value.uploadMastersType == 'materialDetails') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], materialID: xlsData[i][1], descripption: xlsData[i][3] })
            }
            this.UploadJSON = await this.materialDetailsSort(arr);
            this.spinnerService.hide();

          }
          if (this.uploadDataMaster.value.uploadMastersType == 'materialActualGroup') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], materialID: xlsData[i][1], materialSubID: xlsData[i][3], descripption: xlsData[i][5], StandardrUnitCode: xlsData[i][6] })
            }
            
            this.UploadJSON = await this.materialActualGroupSort(arr);
            

            this.spinnerService.hide();

          }

          if (this.uploadDataMaster.value.uploadMastersType == 'applySupply') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], descripption: xlsData[i][1] })
            }
            this.UploadJSON = await this.applySupplyGroupSort(arr);
            this.spinnerService.hide();

          }
          if (this.uploadDataMaster.value.uploadMastersType == 'applySupplyDetails') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], applySupplyID: xlsData[i][1], descripption: xlsData[i][3], StandardrUnitCode: xlsData[i][4] })
            }
            this.UploadJSON = await this.applySupplyDetailsSort(arr);
            this.spinnerService.hide();

          }

          if (this.uploadDataMaster.value.uploadMastersType == 'idc') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], descripption: xlsData[i][1] })
            }
            this.UploadJSON = await this.idcGroupSort(arr);
            this.spinnerService.hide();

          }
          if (this.uploadDataMaster.value.uploadMastersType == 'idcDetails') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], idcActivityID: xlsData[i][1], description: xlsData[i][3] })
            }
            this.UploadJSON = await this.idcDetailsSort(arr);
            this.spinnerService.hide();

          }
          if (this.uploadDataMaster.value.uploadMastersType == 'idcActualGroup') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], idcActivityID: xlsData[i][1], idcSubHeadID: xlsData[i][3], description: xlsData[i][5] })
            }
            this.UploadJSON = await this.idcActualGroupSort(arr);
            this.spinnerService.hide();

          }

        });

      }
    } else {
      this.spinnerService.hide();
      this.commonService.showError("Please select a valid excel file Ex.(.XLS,.XLSX).");

    }
    // }
  }

  // JSON Sort For Upload Data Master
  unitMasterSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": element.cmpCode,
          'standardrUnitCode': element.standardrUnitCode,
          'unitDesc': element.unitDesc,
          'unitCode1': element.unitCode1,
          'unitCode2': element.unitCode2,
          'unitCode3': element.unitCode3,
        })
      });
      resolve(arr)
    })
  }
  departmentSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": element.cmpCode,
          'deptDesc': element.deptDesc

        })
      });
      resolve(arr)
    })
  }
  empDesignationSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": element.cmpCode,
          'designationDesc': element.designationDesc
        })
      });
      resolve(arr)
    })
  }
  labourGroupSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": element.cmpCode,
          'descripption': element.descripption,
          'labourDetails': []
        })
      });
      resolve(arr)
    })
  }
  async labourDetailsSort(data: any) {
    var arr: any = []
    this.labourList = []
    this.labourList = await this.commonFunction.getLabour(data[0].cmpCode);
    const unique: any = [...new Set(data.map((item: any) => item.LabourID))];
    return new Promise(async (resolve, reject) => {
      await unique.forEach(async (element: any) => {
        var cmpCode = null;
        var labourDetails: any = []
        var found = this.labourList.find((field: any) => field.labourID == element);
        if (found) {
          await data.forEach((element1: any) => {
            if (element.toLowerCase() == element1.LabourID.toLowerCase()) {

              cmpCode = element1.cmpCode;
              labourDetails.push({
                "cmpCode": element1.cmpCode,
                "labourID": element1.LabourID,
                "descripption": element1.descripption,
                "standardrUnitCode": element1.StandardrUnitCode  // Need to check with the Unit Masterand then send standard code
              })
            }
          })
          arr.push({
            "labourID": found.labourID,
            "descripption": found.descripption,
            "cmpCode": cmpCode,
            "labourDetails": labourDetails
          });
        } else {
          arr = [];
          this.commonService.showError(element + " is not found in Labour List");
          // break here no need to add records when data is wrong
          resolve(arr)
        }
        resolve(arr)
      })
    })
  }
  materialGroupSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": element.cmpCode,
          'descripption': element.descripption,
          'subMaterial': []
        })
      });
      resolve(arr)
    })
  }
  async materialDetailsSort(data: any) {

    var arr: any = []
    this.MaterialList = []
    this.MaterialList = await this.commonFunction.getMaterial(data[0].cmpCode);
    const unique: any = [...new Set(data.map((item: any) => item.materialID))];

    return new Promise(async (resolve, reject) => {
      await unique.forEach(async (element: any) => {
        var cmpCode = null;
        var subMaterial: any = []
        var found = this.MaterialList.find((field: any) => field.materialID == element);
        if (found) {
          await data.forEach((element1: any) => {
            
            if (element.toLowerCase() == element1.materialID.toLowerCase()) {
              cmpCode = element1.cmpCode;
              subMaterial.push({
                "cmpCode": element1.cmpCode,
                "materialID": element1.materialID,
                "materialSubID": null,
                "descripption": element1.descripption,
                "materialDetails": []
              })
            }
          })
          arr.push({
            "materialID": found.materialID,
            "descripption": found.descripption,
            "cmpCode": cmpCode,
            "subMaterial": subMaterial
          });
        } else {
          arr = [];
          this.commonService.showError(element + "is not found in Material List");
          // break here no need to add
        }
        resolve(arr)
      })
    })
  }
  async materialActualGroupSort(data: any) {

    var arr: any = []
    this.MaterialList = []
    this.MaterialList = await this.commonFunction.getMaterial(data[0].cmpCode);
    const uniqueMaterialID: any = [...new Set(data.map((item: any) => item.materialID))];
    this.unitList = await this.commonFunction.getunitMaster(data[0].cmpCode);
    return new Promise(async (resolve, reject) => {
      await uniqueMaterialID.forEach(async (element: any) => {
        var subMaterial: any = []
        var foundMain = this.MaterialList.find((field: any) => field.materialID == element);
        if (foundMain) {
          var materialDetails: any = []
          const uniqueSubMaterialID: any = [...new Set(data.map((item: any) => item.materialSubID))];
          await uniqueSubMaterialID.forEach( (element1: any) => {
            var foundSub = foundMain.subMaterial.find((field: any) => field.materialSubID == element1);

            if (foundSub) {
               data.forEach(async (element2: any) => {
                if (element == element2.materialID && element1 == element2.materialSubID) {
                  var foundUnit =null;
                  foundUnit= this.unitList.find((field: any) => {if(field.standardrUnitCode == element2.StandardrUnitCode || field.UnitCode1 == element2.StandardrUnitCode || field.UnitCode2 == element2.StandardrUnitCode || field.UnitCode3 == element2.StandardrUnitCode){return field}});
                  if (foundUnit) {
                    materialDetails.push({
                      "cmpCode": foundSub.cmpCode,
                      "materialDetailID": null,
                      "materialID": foundSub.materialID,
                      "descripption": element2.descripption,
                      "materialSubID": foundSub.materialSubID,
                      "standardrUnitCode": foundUnit.standardrUnitCode
                    })
                  } else {
                    this.commonService.showError("Unit Master not found" + element2.StandardrUnitCode);
                    // reject("fail with StandardrUnitCode")
                  }
                }
              })
               subMaterial.push({
                "cmpCode": foundSub.cmpCode,
                "materialID": foundSub.materialID,
                "materialSubID": foundSub.materialSubID,
                "descripption": foundSub.descripption,
                "materialDetails": materialDetails
              })
            } else {
              // Sub Id Is Not Found
              // Break
              // arr = [];
              // this.commonService.showError(element2 + " is not found in Sub Material List");
              // resolve(arr)
            }
          })
          await arr.push({
            "materialID": foundMain.materialID,
            "descripption": foundMain.descripption,
            "cmpCode": foundMain.cmpCode,
            "subMaterial": subMaterial
          });
        } else {
          // arr = [];
          // this.commonService.showError(element + "is not found in Material List");
          // // break here no need to add
          // resolve(arr)
        }
        
        resolve(arr)
      })
    })
  }
  applySupplyGroupSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": element.cmpCode,
          'descripption': element.descripption,
          'applySupplyDetails': []
        })
      });
      resolve(arr)
    })
  }
  async applySupplyDetailsSort(data: any) {
    
    var arr: any = []
    this.applySupplyList = await this.commonFunction.getApplySupply(data[0].cmpCode);
    const unique: any = [...new Set(data.map((item: any) => item.applySupplyID))];
    return new Promise(async (resolve, reject) => {
      await unique.forEach(async (element: any) => {
        var applySupplyDetails: any = []
        var found = this.applySupplyList.find((field: any) => field.applySupplyID == element);
        if (found) {
          await data.forEach((element1: any) => {
            if (element.trim() == element1.applySupplyID) {
              applySupplyDetails.push({
                'cmpCode': element1.cmpCode,
                'applySupplyDetailsID': null,
                'applySupplyID': found.applySupplyID,
                'descripption': element1.descripption,
                'StandardrUnitCode': element1.StandardrUnitCode
              })
            }
          });
          arr.push({
            'cmpCode': found.cmpCode,
            'applySupplyID': found.applySupplyID,
            'descripption': found.descripption,
            'applySupplyDetails': applySupplyDetails
          })
        }      

        resolve(arr)
      })
    })
  }
  // IDC JSON Sorting
  idcGroupSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "idcActivityID": null,
          "cmpCode": element.cmpCode,
          'description': element.descripption,
          'SubHead': []
        })
      });
      resolve(arr)
    })
  }
  async idcDetailsSort(data: any) {

    var arr: any = []
    this.IDCList = []
    this.IDCList = await this.commonFunction.getIDCActivity(data[0].cmpCode);

    const unique: any = [...new Set(data.map((item: any) => item.idcActivityID))];

    return new Promise(async (resolve, reject) => {
      await unique.forEach(async (element: any) => {
        var cmpCode = null;
        var subHead: any = []
        var found = this.IDCList.find((field: any) => field.idcActivityID == element);

        if (found) {
          await data.forEach((element1: any) => {

            if (element == element1.idcActivityID) {
              cmpCode = element1.cmpCode;
              subHead.push({
                "cmpCode": element1.cmpCode,
                "idcSubHeadID": null,
                "idcActivityID": element1.idcActivityID,
                "description": element1.description,
                "materialDetails": []
              })
            }
          })
          arr.push({
            "idcActivityID": found.idcActivityID,
            "description": found.description,
            "cmpCode": cmpCode,
            "subHead": subHead
          });
        } else {
          arr = [];
          this.commonService.showError(element + " is not found in IDC List");
          // break here no need to add
        }
        resolve(arr)
      })
    })
  }
  async idcActualGroupSort(data: any) {

    var arr: any = []
    this.IDCList = []
    this.IDCList = await this.commonFunction.getIDCActivity(data[0].cmpCode);
    const uniqueidcActivityID: any = [...new Set(data.map((item: any) => item.idcActivityID))];

    return new Promise(async (resolve, reject) => {
      await uniqueidcActivityID.forEach(async (element: any) => {
        var subHead: any = []
        var foundMain = this.IDCList.find((field: any) => field.idcActivityID == element);
        if (foundMain) {
          var idcDetails: any = []
          const uniqueSubidcActivityID: any = [...new Set(data.map((item: any) => item.idcSubHeadID))];
          await uniqueSubidcActivityID.forEach(async (element1: any) => {
            var foundSub = foundMain.subHead.find((field: any) => field.idcSubHeadID == element1);

            if (foundSub) {
              await data.forEach((element2: any) => {
                if (element.toLowerCase() == element2.idcActivityID.toLowerCase() && element1.toLowerCase() == element2.idcSubHeadID.toLowerCase()) {

                  idcDetails.push({
                    "cmpCode": foundSub.cmpCode,
                    "materialDetailID": null,
                    "idcActivityID": element2.idcActivityID,
                    "description": element2.description,
                    "idcSubHeadID": foundSub.idcSubHeadID,
                    // "standardrUnitCode": element2.StandardrUnitCode // Need to check with the Unit Masterand then send standard code
                  })
                }
              })
              subHead.push({
                "cmpCode": foundSub.cmpCode,
                "idcActivityID": foundSub.idcActivityID,
                "idcSubHeadID": foundSub.idcSubHeadID,
                "description": foundSub.description,
                "idcDetails": idcDetails
              })
            } else {
              // Sub Id Is Not Found
              // Break
              // arr = [];
              // this.commonService.showError(element1 + " is not found in Sub Material List");
              // resolve(arr)
            }
          })
          arr.push({
            "idcActivityID": foundMain.idcActivityID,
            "description": foundMain.description,
            "cmpCode": foundMain.cmpCode,
            "subHead": subHead
          });
        } else {
          // arr = [];
          // this.commonService.showError(element + "is not found in Material List");
          // // break here no need to add
          // resolve(arr)
        }
        resolve(arr)
      })
    })
  }

  // **********************************************************************************************************
  //                                       In Form Group addControls And Remove Controls  
  // **********************************************************************************************************
  async datamastertype() {
    if (this.datamaster.value.masterType == 'unitMaster') {
      this.datamaster.addControl('standardrUnitCode', this.fb.control('', [Validators.required, Validators.maxLength(20)]));
      this.datamaster.addControl('unitDesc', this.fb.control('', [Validators.required, Validators.maxLength(45)]));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('unitCode1', this.fb.control(null, [Validators.maxLength(20)]));
      this.datamaster.addControl('unitCode2', this.fb.control(null, [Validators.maxLength(20)]));
      this.datamaster.addControl('unitCode3', this.fb.control(null, [Validators.maxLength(20)]));
      if (this.datamaster.controls.deptCode) { this.datamaster.removeControl('deptCode'); }
      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }
      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }

      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }
      if (this.datamaster.controls.details) { this.datamaster.removeControl('details'); }
    }
    else if (this.datamaster.value.masterType == 'departmentDetails') {
      this.datamaster.addControl('deptDesc', this.fb.control('', [Validators.required, Validators.maxLength(50)]));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));

      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }

      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }

      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }
      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }
      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }
      if (this.datamaster.controls.details) { this.datamaster.removeControl('details'); }
    }
    else if (this.datamaster.value.masterType == 'empDesignation') {
      this.datamaster.addControl('designationDesc', this.fb.control('', [Validators.required, Validators.maxLength(50)]));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));

      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }

      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }
      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }
      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }
      if (this.datamaster.controls.details) { this.datamaster.removeControl('details'); }

    }
    else if (this.datamaster.value.masterType == 'labour') {
      this.onChangeCompany(this.datamaster.value.cmpCode);
      this.datamaster.addControl('labourID', this.fb.control(null));
      this.datamaster.addControl('descripption', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('labourDetails', this.fb.array([this.labourDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(this.datamaster.value.cmpCode);
      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }

      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }
      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }
      // if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.details) { this.datamaster.removeControl('details'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }
    }
    else if (this.datamaster.value.masterType == 'material') {

      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.details) { this.datamaster.removeControl('details'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }

      this.onChangeCompany(this.datamaster.value.cmpCode);
      this.datamaster.addControl('materialID', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control(null));
      this.datamaster.addControl('subMaterial', this.fb.array([this.materialDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(this.datamaster.value.cmpCode);
    }
    else if (this.datamaster.value.masterType == 'applysupply') {
      this.onChangeCompany(this.datamaster.value.cmpCode);
      this.datamaster.addControl('applySupplyID', this.fb.control('',));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('applySupplyDetails', this.fb.array([this.applysupplyDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(this.datamaster.value.cmpCode);
      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }
      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }

      // if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }

      if (this.datamaster.controls.details) { this.datamaster.removeControl('details'); }



    }
    else if (this.datamaster.value.masterType == 'idc') {
      this.onChangeCompany(this.datamaster.value.cmpCode);
      this.datamaster.addControl('idcActivityID', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('description', this.fb.control(null));
      this.datamaster.addControl('isFixed', this.fb.control(null));
      this.datamaster.addControl('subHead', this.fb.array([this.IDCDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(this.datamaster.value.cmpCode);
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }


      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.details) { this.datamaster.removeControl('details'); }



    }
    else if (this.datamaster.value.masterType == 'miscellaneous') {
      this.onChangeCompany(this.datamaster.value.cmpCode);
      this.datamaster.addControl('miscellaneousId', this.fb.control('',));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('miscellaneousArray', this.fb.array([this.miscellaneousDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(this.datamaster.value.cmpCode);

      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }
      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }
      if (this.datamaster.controls.details) { this.datamaster.removeControl('details'); }
    }
    else if (this.datamaster.value.masterType == 'vmm') {
      this.onChangeCompany(this.datamaster.value.cmpCode);
      this.datamaster.addControl('vmmId', this.fb.control(null));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('details', this.fb.array([this.vmmDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(this.datamaster.value.cmpCode);

      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }
      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }

      if (this.datamaster.controls.miscellaneousId) { this.datamaster.removeControl('miscellaneousId'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }
    }
    else if (this.datamaster.value.masterType == 'mmm') {
      this.onChangeCompany(this.datamaster.value.cmpCode);
      this.datamaster.addControl('mmmID', this.fb.control('',));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('details', this.fb.array([this.mmmDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(this.datamaster.value.cmpCode);
      if (this.datamaster.controls.description) { this.datamaster.removeControl('description'); }
      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.vmmId) { this.datamaster.removeControl('vmmId'); }
      if (this.datamaster.controls.miscellaneousId) { this.datamaster.removeControl('miscellaneousId'); }

      if (this.datamaster.controls.subMaterial) { this.datamaster.removeControl('subMaterial'); }
      if (this.datamaster.controls.labourDetails) { this.datamaster.removeControl('labourDetails'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.subHead) { this.datamaster.removeControl('subHead'); }

    }
    // else {
    //   this.datamaster.addControl('actGroupCode', this.fb.control('', [Validators.required]));
    //   this.datamaster.addControl('actGroupDesc', this.fb.control('', [Validators.required]));
    //   this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));

    //   if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
    //   if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
    //   if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
    //   if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
    //   if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
    //   if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
    //   if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
    //   if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
    //   if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
    //   if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
    //   if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
    //   if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
    //   if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }
    // }
  }


  // **********************************************************************************************************
  //                      Active Deactive matTab change event for Add method or update method
  // **********************************************************************************************************
  tabIsActive(value: any, change: any) {
    
    this.isCreate = value;
    if (value != change) {
      this.updateRow = null
      if (value == 1 && change == 0) {
        this.isCreate = 0;


      } else if (value == 2 && change == 0) {
        
      }
      this.datamaster.reset();

      if (this.idc()) {
        if (this.idc().length > 0) {
          while (this.idc().length !== 0) {
            this.idc().removeAt(0)
          }
        }
      }
      if (this.labour()) {
        if (this.labour().length > 0) {
          while (this.labour().length !== 0) {
            this.labour().removeAt(0)
          }
        }
      }

      if (this.applysupply()) {
        if (this.applysupply().length > 0) {
          while (this.applysupply().length !== 0) {
            this.applysupply().removeAt(0)
          }
        }
      }

      if (this.materials()) {
        if (this.materials().length > 0) {
          while (this.materials().length !== 0) {
            this.materials().removeAt(0)
          }
        }
      }

      this.isActiveTab = value;
    }
    this.isActiveTab = value;

    this.datamaster.enable();
    if (this.isCreate == 2) { this.uploadDataMaster.reset(); }

  }
  //VAlidation 
  public myError = (controlName: string, errorName: string) => {
    return this.datamaster.controls[controlName].hasError(errorName);
  }
  public validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  // **********************************************************************************************************
  //                      Add and Update masters API call
  // **********************************************************************************************************
  async addMasterData() {

    

    if (this.datamaster.valid || this.UploadJSON.length > 0) {
      var JSON = null;
      if (this.datamaster.valid) {
        JSON = [this.datamaster.value];
      } else {
        JSON = this.UploadJSON;
      }
      var param = {};
      var head = {}
      var data = {}
      let Api: any = null;
      //Account Group Add and Update API
      // if (this.datamaster.controls.masterType.value === 'accountGroup') {
      //   if (this.isCreate == 0) {
      //     data = { data: JSON, param: param };
      //     Api = this.apiCallService.addAccountGroup;
      //   }
      //   else {
      //     JSON.actGroupCode = this.updateRow.actGroupCode;
      //     head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.actGroupCode }
      //     data = { data: JSON, param: param, head: head }
      //     Api = this.apiCallService.updateAccountGroup;
      //   }
      // }
      //Unit Master  Add and Update API
      // else

      if (this.datamaster.controls.masterType.value === 'unitMaster' || this.uploadDataMaster.value.uploadMastersType == 'unitMaster') {
        if (this.isCreate == 0) {
          data = { data: JSON, param: param };

          Api = this.apiCallService.addunitMaster;
        }
        else if (this.isCreate == 1) {
          JSON = this.datamaster.value;
          JSON.standardrUnitCode = this.updateRow.standardrUnitCode;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.standardrUnitCode }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.updateunitMaster;
        }
        else if (this.isCreate == 2) {
          JSON = this.UploadJSON;
          data = { data: JSON, param: param };
          Api = this.apiCallService.addunitMaster;
        }
      }
      //Department Details Add and Update API
      else if (this.datamaster.controls.masterType.value === 'departmentDetails' || this.uploadDataMaster.value.uploadMastersType == 'departmentDetails') {
        if (this.isCreate == 0) {
          data = { data: JSON, param: param };
          Api = this.apiCallService.addDepartment;
        }
        else if (this.isCreate == 1) {
          JSON = this.datamaster.value;
          JSON.deptCode = this.updateRow.deptCode;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.deptCode }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.updateDepartment;
        }
        else if (this.isCreate == 2) {
          JSON = this.UploadJSON;
          // 
          param = { cmpCode: this.UploadJSON[0].cmpCode }

          data = { data: JSON };
          Api = this.apiCallService.addDepartment;
        }
      }
      //Employee Designation Details Add and Update API
      else if (this.datamaster.controls.masterType.value === 'empDesignation' || this.uploadDataMaster.value.uploadMastersType == 'empDesignation') {
        if (this.isCreate == 0) {
          data = { data: JSON, param: param };
          Api = this.apiCallService.addDesignation;
        }
        else if (this.isCreate == 1) {
          JSON = this.datamaster.value;
          JSON.descCode = this.updateRow.descCode;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.descCode }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.updateDesignation;
        }
        else if (this.isCreate == 2) {
          JSON = this.UploadJSON;
          data = { data: JSON, param: param };
          Api = this.apiCallService.addDesignation;
        }
      }
      //Material Details Add and Update API
      else if (this.datamaster.controls.masterType.value === 'material' || this.uploadDataMaster.value.uploadMastersType == 'material' || this.uploadDataMaster.value.uploadMastersType == 'materialDetails' || this.uploadDataMaster.value.uploadMastersType == 'materialActualGroup') {

        if (this.isCreate == 0) {
          // Patch CompanyCode materials Array
          
          let details = await this.MaterialList.find((field: any) => field.materialID == this.datamaster.value.materialID);

          this.datamaster.patchValue({
            descripption: details.descripption
          })

          this.materials().value.forEach((element: any, storeindex: any) => {
            let detailsS = details.subMaterial.find((item: any) => item.materialSubID == element.materialSubID);

            
            (<FormGroup>this.materials().at(storeindex)).patchValue({
              cmpCode: this.datamaster.value.cmpCode,
              materialID: this.datamaster.value.materialID,
              materialSubID: element.materialSubID,
              descripption: detailsS.descripption
            })
            if (this.MaterialDet(storeindex).value.length > 0) {
              this.MaterialDet(storeindex).value.forEach((element1: any, subIndex: any) => {

                (<FormGroup>this.MaterialDet(storeindex).at(subIndex)).patchValue({
                  cmpCode: this.datamaster.value.cmpCode,
                  materialID: this.datamaster.value.materialID,
                  materialSubID: element.materialSubID,
                })

              })
            }
          })

          JSON = [this.datamaster.value];
          data = { data: JSON, param: param };
          Api = this.apiCallService.addMaterialMaster;
        }
        else if (this.isCreate == 1) {
          
          let details = await this.MaterialList.find((field: any) => field.materialID == this.datamaster.value.materialID);

          this.datamaster.patchValue({
            descripption: details.descripption
          })

          this.materials().value.forEach((element: any, storeindex: any) => {
            let detailsS = details.subMaterial.find((item: any) => item.materialSubID == element.materialSubID);

            
            (<FormGroup>this.materials().at(storeindex)).patchValue({

              cmpCode: this.datamaster.value.cmpCode,
              materialID: this.datamaster.value.materialID,
              materiaSlSubID: element.materialSubID,
              descripption: detailsS.descripption,


            })

            if (this.MaterialDet(storeindex).value.length > 0) {
              this.MaterialDet(storeindex).value.forEach((element1: any, subIndex: any) => {

                (<FormGroup>this.MaterialDet(storeindex).at(subIndex)).patchValue({
                  cmpCode: this.datamaster.value.cmpCode,
                  materialID: this.datamaster.value.materialID,
                  materialSubID: element.materialSubID,
                })
              })
            }
          })
          JSON = [this.datamaster.value];
          JSON[0].materialID = this.updateRow.materialID;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.materialID }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.addMaterialMaster;
        } //Exel API Call

        else if (this.isCreate == 2) {
          
          JSON = this.UploadJSON;
          data = { data: JSON, param: param };
          Api = this.apiCallService.addMaterialMaster;
        }
      }
      //Labour Add and Update API
      else if (this.datamaster.controls.masterType.value === 'labour' || this.uploadDataMaster.value.uploadMastersType == 'labour' || this.uploadDataMaster.value.uploadMastersType == 'labourDetails') {

        if (this.isCreate == 0) {
          let details = this.labourList.find((field: any) => field.labourID == this.datamaster.value.descripption)

          this.datamaster.patchValue({
            descripption: details.descripption
          })
          this.labour().value.forEach((element: any, index: any) => {
            (<FormGroup>this.labour().at(index)).patchValue({
              cmpCode: this.datamaster.value.cmpCode,
            })
          })
          JSON = [this.datamaster.value];
          data = { data: JSON, param: param };
          Api = this.apiCallService.addLabourMaster;

        }
        else if (this.isCreate == 1) {
          this.labour().value.forEach((element: any, index: any) => {
            (<FormGroup>this.labour().at(index)).patchValue({
              cmpCode: this.datamaster.value.cmpCode,
              labourID: this.datamaster.value.labourID,
            })
          })
          JSON = [this.datamaster.value];
          JSON[0].labourID = this.updateRow.labourID;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.labourID }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.addLabourMaster;
        }
        //Exel API Call
        else if (this.isCreate == 2) {
          JSON = this.UploadJSON;
          data = { data: JSON, param: param };
          Api = this.apiCallService.addLabourMaster;
        }
      }

      //Apply Supply Add and Update API
      else if (this.datamaster.controls.masterType.value === 'applysupply' || this.uploadDataMaster.value.uploadMastersType == 'applySupply' || this.uploadDataMaster.value.uploadMastersType == 'applySupplyDetails') {
        if (this.isCreate == 0) {
          let details = this.applySupplyList.find((field: any) => field.applySupplyID == this.datamaster.value.descripption)

          this.datamaster.patchValue({
            descripption: details.descripption,
            applySupplyID: details.applySupplyID
          })
          this.applysupply().value.forEach((element: any, index: any) => {

            (<FormGroup>this.applysupply().at(index)).patchValue({
              cmpCode: this.datamaster.value.cmpCode,
              applySupplyID: this.datamaster.value.applySupplyID,
            })
          })

          JSON = [this.datamaster.value];
          data = { data: JSON, param: param };
          Api = this.apiCallService.addApplySupply;
        } else if (this.isCreate == 1) {

          this.applysupply().value.forEach((element: any, index: any) => {

            (<FormGroup>this.applysupply().at(index)).patchValue({
              cmpCode: this.datamaster.value.cmpCode,
              applySupplyID: this.datamaster.value.applySupplyID,

            })
          })

          JSON = [this.datamaster.value];
          JSON[0].applySupplyID = this.updateRow.applySupplyID;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.applySupplyID }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.addApplySupply;
        }
        else if (this.isCreate == 2) {
          JSON = this.UploadJSON;
          data = { data: JSON, param: param };
          Api = this.apiCallService.addApplySupply;
        }
      }

      //IDC Details Add and Update API
      else if (this.datamaster.controls.masterType.value === 'idc' || this.uploadDataMaster.value.uploadMastersType == 'idc' || this.uploadDataMaster.value.uploadMastersType == 'idcDetails' || this.uploadDataMaster.value.uploadMastersType == 'idcActualGroup') {
        if (this.isCreate == 0) {
          // Patch CompanyCode idcs Array
          let details = this.IDCList.find((field: any) => field.idcActivityID == this.datamaster.value.idcActivityID);

          this.datamaster.patchValue({
            description: details.description
          })

          this.idc().value.forEach((element: any, storeindex: any) => {
            let detailsS = details.subHead.find((item: any) => item.idcSubHeadID == element.idcSubHeadID);

            


              (<FormGroup>this.idc().at(storeindex)).patchValue({
                cmpCode: this.datamaster.value.cmpCode,
                idcActivityID: this.datamaster.value.idcActivityID,
                idcSubHeadID: element.idcSubHeadID,
                description: detailsS.description
              })
            if (this.idcActualArray(storeindex).value.length > 0) {
              this.idcActualArray(storeindex).value.forEach((element1: any, subIndex: any) => {

                (<FormGroup>this.idcActualArray(storeindex).at(subIndex)).patchValue({
                  cmpCode: this.datamaster.value.cmpCode,
                  idcActivityID: this.datamaster.value.idcActivityID,
                  idcSubHeadID: element.idcSubHeadID
                })

              })
            }

          })

          JSON = [this.datamaster.value];
          data = { data: JSON, param: param };
          Api = this.apiCallService.addIDCActivity;
        }
        else if (this.isCreate == 1) {
          let details = this.IDCList.find((field: any) => field.idcActivityID == this.datamaster.value.idcActivityID);

          this.datamaster.patchValue({
            description: details.description
          })

          this.idc().value.forEach((element: any, storeindex: any) => {
            let detailsS = details.subHead.find((item: any) => item.idcSubHeadID == element.idcSubHeadID);


            // this.idc().value.forEach((element: any, storeindex: any) => {
            //   let detailsS = details.idcDetails.find((item: any) => item.materialSubID == element.materialSubID);


            (<FormGroup>this.idc().at(storeindex)).patchValue({
              cmpCode: this.datamaster.value.cmpCode,
              idcActivityID: this.datamaster.value.idcActivityID,
              idcSubHeadID: element.idcSubHeadID,
              description: detailsS.description,
              // isFixed: detailsS.isFixed
            })
            if (this.idcActualArray(storeindex).value.length > 0) {
              this.idcActualArray(storeindex).value.forEach((element1: any, subIndex: any) => {

                (<FormGroup>this.idcActualArray(storeindex).at(subIndex)).patchValue({
                  cmpCode: this.datamaster.value.cmpCode,
                  idcActivityID: this.datamaster.value.idcActivityID,
                  idcSubHeadID: element.idcSubHeadID
                })

              })
            }

          })

          JSON = [this.datamaster.value];
          JSON[0].idcActivityID = this.updateRow.idcActivityID;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.idcActivityID }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.addIDCActivity;
        } //Exel API Call

        else if (this.isCreate == 2) {
          JSON = this.UploadJSON;
          data = { data: JSON, param: param };
          Api = this.apiCallService.addIDCActivity;
        }
      }
      //VMM Add and Update API
      else if (this.datamaster.controls.masterType.value === 'vmm' || this.uploadDataMaster.value.uploadMastersType == 'vmm') {

        if (this.isCreate == 0) {
          this.vmm().value.forEach((element: any, index: any) => {
            (<FormGroup>this.vmm().at(index)).patchValue({
              cmpCode: this.datamaster.value.cmpCode,

            })
          })
          JSON = [this.datamaster.value];
          data = { data: JSON, param: param };
          Api = this.apiCallService.addVmmMaster;
        }
        else {
          JSON.vmmID = this.updateRow.vmmID;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.vmmID }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.addVmmMaster;
        }
      }
      else if (this.datamaster.controls.masterType.value === 'mmm' || this.uploadDataMaster.value.uploadMastersType == 'mmm') {

        if (this.isCreate == 0) {
          this.materialmm().value.forEach((element: any, index: any) => {
            (<FormGroup>this.materialmm().at(index)).patchValue({
              cmpCode: this.datamaster.value.cmpCode,
              mmmID: this.datamaster.value.mmmID,

            })
          })
          JSON = [this.datamaster.value];
          data = { data: JSON, param: param };
          Api = this.apiCallService.addmmmMaster;
        }
        else {
          JSON.mmmID = this.updateRow.mmmID;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.mmmID }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.addmmmMaster;
        }
      }
      

      Api(data).subscribe({
        next: async (result: any) => {
          if (result) {
            this.commonService.showSuccess("Record Inserted Succefully!");
            this.UploadJSON = []
            this.isActiveTab = 1;
            //patch values datamaster to viewMasterdata
            if (this.isCreate == 0) {
              this.viewMasterData.patchValue({ Masters: this.datamaster.value.masterType });
              this.viewMasterData.patchValue({ cmpCode: this.datamaster.value.cmpCode });
            }
            else if (this.isCreate == 2) {
              if (this.uploadDataMaster.value.uploadMastersType == 'labourDetails') {
                this.viewMasterData.patchValue({ Masters: 'labour' });
              } else if (this.uploadDataMaster.value.uploadMastersType == 'materialDetails' || this.uploadDataMaster.value.uploadMastersType == 'materialActualGroup') {
                this.viewMasterData.patchValue({ Masters: 'material' });
              } else if (this.uploadDataMaster.value.uploadMastersType == 'applySupply' || this.uploadDataMaster.value.uploadMastersType == 'applySupplyDetails') {
                this.viewMasterData.patchValue({ Masters: 'applysupply' });
              } else if (this.uploadDataMaster.value.uploadMastersType == 'idcDetails' || this.uploadDataMaster.value.uploadMastersType == 'idcActualGroup') {
                this.viewMasterData.patchValue({ Masters: 'idc' });
              } else {
                this.viewMasterData.patchValue({ Masters: this.uploadDataMaster.value.uploadMastersType });
              }

              this.viewMasterData.patchValue({ cmpCode: this.uploadDataMaster.value.cmpCode });
            }

            this.spinnerService.show();
            if (this.datamaster.controls.masterType.value == 'unitMaster' || this.uploadDataMaster.value.uploadMastersType == 'unitMaster') {

              if (this.datamaster.value.cmpCode) { this.projectmasterList = await this.commomFunction.getunitMaster(this.datamaster.value.cmpCode); }
              else {
                this.projectmasterList = await this.commomFunction.getunitMaster(this.uploadDataMaster.value.cmpCode);
              }
              this.displayedColumns = ['action', 'cmpCode', 'standardrUnitCode', 'unitDesc', 'unitCode1', 'unitCode2', 'unitCode3'];

            } else if (this.datamaster.controls.masterType.value == 'accountGroup') {

              if (this.datamaster.value.cmpCode) { this.projectmasterList = await this.commomFunction.getAccountGroup(this.datamaster.value.cmpCode); }
              else {
                this.projectmasterList = await this.commomFunction.getAccountGroup(this.uploadDataMaster.value.cmpCode);
              }

              this.displayedColumns = ['action', 'cmpCode', 'actGroupCode', 'actGroupDesc'];
            }
            else if (this.datamaster.controls.masterType.value == 'departmentDetails' || this.uploadDataMaster.value.uploadMastersType == 'departmentDetails') {

              if (this.datamaster.value.cmpCode) { this.projectmasterList = await this.commomFunction.getDepartmentById(this.datamaster.value.cmpCode); }
              else {
                this.projectmasterList = await this.commomFunction.getDepartmentById(this.uploadDataMaster.value.cmpCode);
              }

              this.displayedColumns = ['action', 'cmpCode', 'deptDesc'];
            }
            else if (this.datamaster.controls.masterType.value == 'empDesignation' || this.uploadDataMaster.value.uploadMastersType == 'empDesignation') {

              if (this.datamaster.value.cmpCode) { this.projectmasterList = await this.commomFunction.getDesignationById(this.datamaster.value.cmpCode); }
              else {
                this.projectmasterList = await this.commomFunction.getDesignationById(this.uploadDataMaster.value.cmpCode);
              }

              this.displayedColumns = ['action', 'cmpCode', 'designationDesc'];
            }
            else if (this.datamaster.controls.masterType.value == 'material' || this.uploadDataMaster.value.uploadMastersType == 'material' || this.uploadDataMaster.value.uploadMastersType == 'materialDetails' || this.uploadDataMaster.value.uploadMastersType == 'materialActualGroup') {

              if (this.datamaster.value.cmpCode) { this.projectmasterList = await this.commomFunction.getMaterial(this.datamaster.value.cmpCode); }
              else {
                this.projectmasterList = await this.commomFunction.getMaterial(this.uploadDataMaster.value.cmpCode);
              }

              this.displayedColumns = ['action', 'cmpCode', 'materialID', 'descripption'];

            }
            else if (this.datamaster.controls.masterType.value == 'labour' || this.uploadDataMaster.value.uploadMastersType == 'labour' || this.uploadDataMaster.value.uploadMastersType == 'labourDetails') {

              if (this.datamaster.value.cmpCode) { this.projectmasterList = await this.commomFunction.getLabour(this.datamaster.value.cmpCode); }
              else {
                this.projectmasterList = await this.commomFunction.getLabour(this.uploadDataMaster.value.cmpCode);
              }

              this.displayedColumns = ['action', 'cmpCode', 'labourID', 'descripption'];
            }
            else if (this.datamaster.controls.masterType.value == 'applysupply' || this.uploadDataMaster.value.uploadMastersType == 'applySupply' || this.uploadDataMaster.value.uploadMastersType == 'applySupplyDetails') {

              if (this.datamaster.value.cmpCode) { this.projectmasterList = await this.commomFunction.getApplySupply(this.datamaster.value.cmpCode); }
              else {
                this.projectmasterList = await this.commomFunction.getApplySupply(this.uploadDataMaster.value.cmpCode);
              }

              this.displayedColumns = ['action', 'cmpCode', 'applySupplyID', 'descripption'];
            }
            else if (this.datamaster.controls.masterType.value == 'idc' || this.uploadDataMaster.value.uploadMastersType == 'idc' || this.uploadDataMaster.value.uploadMastersType == 'idcDetails' || this.uploadDataMaster.value.uploadMastersType == 'idcActualGroup') {

              if (this.datamaster.value.cmpCode) {
                this.projectmasterList = await this.commomFunction.getIDCActivity(this.datamaster.value.cmpCode);
              }
              else {
                this.projectmasterList = await this.commomFunction.getIDCActivity(this.uploadDataMaster.value.cmpCode);
              }

              this.displayedColumns = ['action', 'cmpCode', 'cmpName', 'idcActivityID', 'description'];

            }
            else if (this.datamaster.controls.masterType.value == 'vmm') {
              this.projectmasterList = await this.commomFunction.getVmm(this.datamaster.value.cmpCode);
              this.displayedColumns = ['action', 'cmpCode', 'vmmID', 'descripption'];
            }
            else {
              this.projectmasterList = []

            }

            this.tabIsActive(0, 0);

            if (this.projectmasterList.length > 0) {
              this.dataSource = new MatTableDataSource(this.projectmasterList);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              this.datamaster.reset();
              this.uploadDataMaster.reset();
            }
            this.spinnerService.hide();

          }
          this.uploadDataMaster.reset();
          this.datamaster.reset();
        }, error: (error: any) => {
          this.commonService.showError(error);
        }
      })
    } else {
      this.validateAllFormFields(this.datamaster);
    }
  }

  addmodalDescription() {
    this.modalDescription.patchValue({
      cmpCode: this.datamaster.value.cmpCode,
    });

    if (this.modalDescription.invalid) {
      this.submitted = true;
      return;
    }
    var JSON: any = [];
    var data = {}
    let Api = null;
    if (this.datamaster.controls.masterType.value === 'labour') {
      JSON = [this.modalDescription.value];
      this.modalDescription.value.labourDetails = []
      data = { data: JSON };
      Api = this.apiCallService.addLabourMaster;

    } else if (this.datamaster.controls.masterType.value === 'material') {
      if (this.modaldata == "group") {
        JSON = [{
          "cmpCode": this.datamaster.controls.cmpCode.value,
          "descripption": this.modalDescription.controls.descripption.value,
          "subMaterial": []
        }]
      } else if (this.modaldata == "subGroup") {
        let details = this.MaterialList.find((field: any) => field.materialID == this.datamaster.value.materialID)

        JSON = [{
          "materialID": this.datamaster.controls.materialID.value,
          "cmpCode": this.datamaster.controls.cmpCode.value,
          "descripption": details.descripption,
          "subMaterial": [
            {
              "materialID": this.datamaster.controls.materialID.value,
              "cmpCode": this.datamaster.controls.cmpCode.value,
              "descripption": this.modalDescription.controls.descripption.value,
              "materialDetails": []
            }
          ]
        }]
      }

      data = { data: JSON };
      Api = this.apiCallService.addMaterialMaster;
    }

    else if (this.datamaster.controls.masterType.value === 'applysupply') {
      this.modalDescription.value.applySupplyDetails = []
      JSON = [this.modalDescription.value];
      data = { data: JSON, };
      Api = this.apiCallService.addApplySupply;
    } else if (this.datamaster.controls.masterType.value === 'idc') {

      if (this.modaldata == "group") {
        JSON = [{
          "idcActivityID": null,
          "cmpCode": this.datamaster.controls.cmpCode.value,
          "description": this.modalDescription.controls.descripption.value,
          "subHead": []
        }]

      } else if (this.modaldata == "subGroup") {

        let details = this.IDCList.find((field: any) => field.idcActivityID == this.datamaster.value.idcActivityID)

        JSON = [{
          "idcActivityID": this.datamaster.controls.idcActivityID.value,
          "cmpCode": this.datamaster.controls.cmpCode.value,
          "description": details.description,
          "subHead": [
            {
              "idcActivityID": this.datamaster.controls.idcActivityID.value,
              "cmpCode": this.datamaster.controls.cmpCode.value,
              "description": this.modalDescription.controls.descripption.value,
              "idcDetails": []
            }
          ]
        }]
      }

      data = { data: JSON };
      Api = this.apiCallService.addIDCActivity;
    }
    else if (this.datamaster.controls.masterType.value === 'vmm') {
      this.modalDescription.value.details = []
      JSON = [this.modalDescription.value];
      data = { data: JSON, };
      Api = this.apiCallService.addVmmMaster;
    }
    else if (this.datamaster.controls.masterType.value === 'mmm') {
      this.modalDescription.value.Details = []
      JSON = [this.modalDescription.value];
      data = { data: JSON, };
      Api = this.apiCallService.addmmmMaster;
    }


    Api!(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");

          if (this.datamaster.controls.masterType.value == 'material') {
            this.MaterialList = await this.commonFunction.getMaterial(this.datamaster.value.cmpCode);

            // this.subMaterialList = await this.commonFunction.getMaterial(this.datamaster.value.cmpCode);
            if (JSON[0].materialID) {
              this.onChangeMaterial(JSON[0].materialID)
            }
          }
          else if (this.datamaster.controls.masterType.value == 'idc') {
            this.IDCList = await this.commonFunction.getIDCActivity(this.datamaster.value.cmpCode);
            // this.subIDCList = await this.commonFunction.getIDCActivity(this.datamaster.value.cmpCode);
            if (JSON[0].idcActivityID) {
              this.onChangeIDC(JSON[0].idcActivityID)
            }
          }
          else if (this.datamaster.controls.masterType.value == 'labour') {
            this.labourList = await this.commonFunction.getLabour(this.datamaster.value.cmpCode);
          }
          else if (this.datamaster.controls.masterType.value == 'applysupply') {
            this.applySupplyList = await this.commonFunction.getApplySupply(this.datamaster.value.cmpCode);
          }
          // if (this.datamaster.controls.masterType.value == 'idc') {
          //   this.IDCList = await this.commonFunction.getIDCActivity(this.datamaster.value.cmpCode);

          //   if (JSON[0].idcActivityID) {
          //     this.onChangeIDC(JSON[0].idcActivityID)
          //   }
          // }
          else if (this.datamaster.controls.masterType.value == 'vmm') {
            this.mmmList = await this.commonFunction.getVmm(this.datamaster.value.cmpCode);
          }
          else if (this.datamaster.controls.masterType.value == 'mmm') {
            this.mmmList = await this.commonFunction.getMmm(this.datamaster.value.cmpCode);
          }
          else {
            this.projectmasterList = []

          }
          this.modalDescription.reset();
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })

  }
  // **********************************************************************************************************
  //                      Edit and View Maters API
  // **********************************************************************************************************
  async editMasterData(row: any, isView: any) {

    this.updateRow = []
    this.updateRow = row;
    this.datamaster.patchValue({ masterType: this.viewMasterData.value.Masters })
    //Unit Master Edit
    if (this.datamaster.value.masterType == 'unitMaster') {

      this.datamaster.addControl('standardrUnitCode', this.fb.control('', [Validators.required]))

      this.datamaster.addControl('unitDesc', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('cmpCode', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('unitCode1', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('unitCode2', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('unitCode3', this.fb.control('', [Validators.required]));
      if (this.datamaster.controls.deptCode) { this.datamaster.removeControl('deptCode'); }
      if (this.datamaster.controls.deptDesc) { this.datamaster.removeControl('deptDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      this.datamaster.patchValue({ masterType: 'unitMaster' });
      this.datamaster.patchValue(row)
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }

    }
    //Department Details Master Edit
    else if (this.datamaster.value.masterType == 'departmentDetails') {
      // this.datamaster.addControl('deptCode', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('deptDesc', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('cmpCode', this.fb.control('', [Validators.required]));

      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }

      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.deptCode) { this.datamaster.removeControl('deptCode'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      this.datamaster.patchValue(row)
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }
    }
    //empDesignation Master Edit
    else if (this.datamaster.value.masterType == 'empDesignation') {
      this.datamaster.addControl('designationDesc', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('cmpCode', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('actGroupCode', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('actGroupDesc', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('cmpCode', this.fb.control('', [Validators.required]));

      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }

      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }

      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      this.datamaster.patchValue(row)
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }
    }
    //Labour Master Edit
    else if (this.datamaster.value.masterType == 'labour') {
      if (this.labour()) {
        while (this.labour().length !== 0) {
          this.labour().removeAt(0)
        }
      }

      this.datamaster.addControl('labourID', this.fb.control('',));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('labourDetails', this.fb.array([]));
      this.unitList = await this.commonFunction.getunitMaster(row.cmpCode);
      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }

      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      this.datamaster.patchValue({ masterType: 'labour' });
      this.datamaster.patchValue({
        labourID: row.labourID,
        cmpCode: row.cmpCode,
        descripption: row.descripption,
      });


      if (row.labourDetails.length > 0) {
        for (let i = 0; i < row.labourDetails.length; i++) {
          // this.removeLabour(i)

          this.addLabour();
          // await this.labour().value.forEach((element: any, index: any) => {
          (<FormGroup>this.labour().at(i)).patchValue({
            labourDetailsID: row.labourDetails[i].labourDetailsID,
            labourID: row.labourDetails[i].labourID,
            cmpCode: row.labourDetails[i].cmpCode,
            descripption: row.labourDetails[i].descripption,
            StandardrUnitCode: row.labourDetails[i].standardrUnitCode,
          })
          // })
        }
      }
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }
    }
    //Material Master Edit
    else if (this.datamaster.value.masterType == 'material') {

      if (this.materials()) {
        if (this.materials().length > 0) {
          while (this.materials().length !== 0) {
            this.materials().removeAt(0)
          }
        }
      }


      this.datamaster.addControl('materialID', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('cmpCode', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control('', [Validators.required]));

      this.datamaster.addControl('subMaterial', this.fb.array([]));

      this.unitList = await this.commonFunction.getunitMaster(row.cmpCode);
      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }


      this.datamaster.patchValue({
        materialID: row.materialID,
        cmpCode: row.cmpCode,
        descripption: row.descripption,
      });

      if (row.subMaterial) {
        if (row.subMaterial.length > 0) {

          for (let i = 0; i < row.subMaterial.length; i++) {


            this.addMaterial();
            (<FormGroup>this.materials().at(i)).patchValue({
              cmpCode: row.subMaterial[i].cmpCode,
              descripption: row.subMaterial[i].descripption,
              materialSubID: row.subMaterial[i].materialSubID,
              materialID: row.subMaterial[i].materialID
            })

            if (row.subMaterial.length > 0) {
              for (let j = 0; j < row.subMaterial[i].materialDetails.length; j++) {
                this.addSubMaterial(i);

                (<FormGroup>this.MaterialDet(i).at(j)).patchValue({

                  cmpCode: row.subMaterial[i].materialDetails[j].cmpCode,
                  materialID: row.subMaterial[i].materialDetails[j].materialID,
                  materialSubID: row.subMaterial[i].materialDetails[j].materialSubID,
                  materialDetailID: row.subMaterial[i].materialDetails[j].materialDetailID,
                  descripption: row.subMaterial[i].materialDetails[j].descripption,
                  StandardrUnitCode: row.subMaterial[i].materialDetails[j].standardrUnitCode,
                })
              }
            }
          }
        }
      }

      this.tabIsActive(1, 1);
      if (isView == 'view') {
        this.datamaster.disable(); this.isCreate = 2;

      } else { this.datamaster.enable(); }

      this.MaterialList = await this.commonFunction.getMaterial(row.cmpCode);
      var found = await this.MaterialList.find((item: any) => { if (item.materialID == row.materialID) { return item } })

      if (found) { this.subMaterialList = found.subMaterial; this.MaterialGroupDesc = found.descripption; } else { this.subMaterialList = []; this.MaterialGroupDesc = null }



      // 


    }
    //Apply supply Master Edit
    else if (this.datamaster.value.masterType == 'applysupply') {

      if (this.applysupply()) {
        while (this.applysupply().length !== 0) {
          this.applysupply().removeAt(0)
        }
      }
      this.datamaster.addControl('applySupplyID', this.fb.control(''));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control('', [Validators.required]));

      this.datamaster.addControl('applySupplyDetails', this.fb.array([]));
      this.unitList = await this.commonFunction.getunitMaster(row.cmpCode);
      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }

      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      this.datamaster.patchValue(row)
      this.datamaster.patchValue({
        applySupplyID: row.applySupplyID,
        cmpCode: row.cmpCode,
        descripption: row.descripption,
      });

      if (row.applySupplyDetails.length > 0) {
        for (let i = 0; i < row.applySupplyDetails.length; i++) {
          // this.removeapplysupply(i);

          this.addapplysupply();
          // await this.labour().value.forEach((element: any, index: any) => {
          (<FormGroup>this.applysupply().at(i)).patchValue({
            StandardrUnitCode: row.applySupplyDetails[i].standardrUnitCode,
            descripption: row.applySupplyDetails[i].descripption,
            applySupplyID: row.applySupplyDetails[i].applySupplyID,
            cmpCode: row.applySupplyDetails[i].cmpCode,
            applySupplyDetailsID: row.applySupplyDetails[i].applySupplyDetailsID,
          })
          // })
        }
      }

      // if (row.applySupplyDetails.length > 0) {
      // for (let i = 0; i < row.applySupplyDetails.length; i++) {
      //   // await this.applysupply().value.forEach((element: any, storeindex: any) => {
      //     // this.addapplysupply();
      //     (<FormGroup>this.applysupply().at(i)).patchValue({
      //       StandardrUnitCode: row.applySupplyDetails[i].standardrUnitCode,
      //       descripption: row.applySupplyDetails[i].descripption,
      //       applySupplyID: row.applySupplyDetails[i].applySupplyID,
      //       cmpCode: row.applySupplyDetails[i].cmpCode,

      //     })
      //   // })
      // }}
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }
    }
    //Vmm Master Edit
    else if (this.datamaster.value.masterType == 'vmm') {

      this.datamaster.addControl('vmmId', this.fb.control(null));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('details', this.fb.array([this.vmmDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(row.cmpCode);

      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }

      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }

      if (this.datamaster.controls.miscellaneousId) { this.datamaster.removeControl('miscellaneousId'); }
      this.datamaster.patchValue({ masterType: 'vmm' });
      this.datamaster.patchValue({
        vmmID: row.vmmID,
        cmpCode: row.cmpCode,
        description: row.description,
      });


      if (row.details.length > 0) {
        for (let i = 0; i < row.details.length; i++) {

          this.addVmm();
          // await this.labour().value.forEach((element: any, index: any) => {
          (<FormGroup>this.vmm().at(i)).patchValue({
            vmmDetailsID: row.details[i].vmmDetailsID,
            vmmID: row.details[i].vmmID,
            cmpCode: row.details[i].cmpCode,
            description: row.details[i].description,
            StandardrUnitCode: row.details[i].standardrUnitCode,
          })
        }
      }
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }
    }
    else if (this.datamaster.value.masterType == 'mmm') {

      this.datamaster.addControl('mmmID', this.fb.control('',));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('details', this.fb.array([this.mmmDataGroup()]));
      this.unitList = await this.commonFunction.getunitMaster(row.cmpCode);

      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.vmmId) { this.datamaster.removeControl('vmmId'); }
      if (this.datamaster.controls.miscellaneousId) { this.datamaster.removeControl('miscellaneousId'); }
      this.datamaster.patchValue({ masterType: 'mmm' });
      this.datamaster.patchValue({
        mmmID: row.mmmID,
        cmpCode: row.cmpCode,
        description: row.description,
      });

      if (row.details.length > 0) {
        for (let i = 0; i < row.details.length; i++) {

          this.addMmm();
          // await this.labour().value.forEach((element: any, index: any) => {
          (<FormGroup>this.materialmm().at(i)).patchValue({
            mmmDetailsID: row.details[i].mmmDetailsID,
            mmmID: row.details[i].mmmID,
            cmpCode: row.details[i].cmpCode,
            description: row.details[i].description,
            StandardrUnitCode: row.details[i].standardrUnitCode,
          })
        }
      }
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }

    }

    else if (this.datamaster.value.masterType == 'idc') {
      if (this.idc()) {
        while (this.idc().length !== 0) {
          this.idc().removeAt(0)
        }
      }
      this.datamaster.addControl('idcActivityID', this.fb.control(null,));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('description', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('isFixed', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('subHead', this.fb.array([]));
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      // this.datamaster.patchValue({ masterType: 'idc' });
      // this.datamaster.patchValue(row)
      
      this.datamaster.patchValue({
        idcActivityID: row.idcActivityID,
        cmpCode: row.cmpCode,
        description: row.description,
        isFixed: row.isFixed,
      });
      if (row.subHead) {
        if (row.subHead.length > 0) {
          for (let i = 0; i < row.subHead.length; i++) {
            this.addIDC();

            (<FormGroup>this.idc().at(i)).patchValue({
              cmpCode: row.subHead[i].cmpCode,
              description: row.subHead[i].description,
              idcSubHeadID: row.subHead[i].idcSubHeadID,
              idcActivityID: row.subHead[i].idcActivityID
            })
            if (row.subHead.length > 0) {
              for (let j = 0; j < row.subHead[i].idcDetails.length; j++) {
                this.addSubActualIDC(i);

                (<FormGroup>this.idcActualArray(i).at(j)).patchValue({
                  cmpCode: row.subHead[i].idcDetails[j].cmpCode,
                  idcDetailsID: row.subHead[i].idcDetails[j].idcDetailsID,
                  idcSubHeadID: row.subHead[i].idcDetails[j].idcSubHeadID,
                  idcActivityID: row.subHead[i].idcDetails[j].idcActivityID,
                  description: row.subHead[i].idcDetails[j].description,

                })
              }
            }
          }
        }
      }


      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }

      this.IDCList = await this.commonFunction.getIDCActivity(row.cmpCode);
      var found = this.IDCList.find((item: any) => { if (item.idcActivityID == row.idcActivityID) { return item } })

      if (found) { this.subIDCList = found.subHead; } else { this.subIDCList = []; }
    }

    else if (this.datamaster.value.masterType == 'miscellaneous') {
      this.datamaster.addControl('miscellaneousId', this.fb.control('',));
      this.datamaster.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.datamaster.addControl('descripption', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('miscellaneousArray', this.fb.array([this.miscellaneousDataGroup()]));

      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }
      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }
      if (this.datamaster.controls.actGroupCode) { this.datamaster.removeControl('actGroupCode'); }
      if (this.datamaster.controls.actGroupDesc) { this.datamaster.removeControl('actGroupDesc'); }
      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.applySupplyID) { this.datamaster.removeControl('applySupplyID'); }
      if (this.datamaster.controls.applySupplyDetails) { this.datamaster.removeControl('applySupplyDetails'); }
      if (this.datamaster.controls.idcActivityID) { this.datamaster.removeControl('idcActivityID'); }
      this.datamaster.patchValue({ masterType: 'miscellaneous' });
      this.datamaster.patchValue(row)
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }

    }
    else {
      this.datamaster.addControl('actGroupCode', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('actGroupDesc', this.fb.control('', [Validators.required]));
      this.datamaster.addControl('cmpCode', this.fb.control('', [Validators.required]));

      if (this.datamaster.controls.DeptDesc) { this.datamaster.removeControl('DeptDesc'); }

      if (this.datamaster.controls.designationDesc) { this.datamaster.removeControl('designationDesc'); }
      if (this.datamaster.controls.standardrUnitCode) { this.datamaster.removeControl('standardrUnitCode'); }
      if (this.datamaster.controls.unitDesc) { this.datamaster.removeControl('unitDesc'); }

      if (this.datamaster.controls.unitCode1) { this.datamaster.removeControl('unitCode1'); }
      if (this.datamaster.controls.unitCode2) { this.datamaster.removeControl('unitCode2'); }
      if (this.datamaster.controls.unitCode3) { this.datamaster.removeControl('unitCode3'); }

      if (this.datamaster.controls.labourID) { this.datamaster.removeControl('labourID'); }
      if (this.datamaster.controls.descripption) { this.datamaster.removeControl('descripption'); }
      if (this.datamaster.controls.materialID) { this.datamaster.removeControl('materialID'); }
      if (this.datamaster.controls.materialSubID) { this.datamaster.removeControl('materialSubID'); }
      this.datamaster.patchValue(row)
      this.tabIsActive(1, 1);
      if (isView == 'view') { this.datamaster.disable(); this.isCreate = 2; } else { this.datamaster.enable(); }
    }
  }
  async onChangeCompany(cmpCode: any) {

    if (cmpCode && this.datamaster.value.masterType == 'labour') {
      this.labourList = []
      this.labourList = await this.commonFunction.getLabour(cmpCode);
    } else if (cmpCode && this.datamaster.value.masterType == 'material') {
      this.MaterialList = []
      this.MaterialList = await this.commonFunction.getMaterial(cmpCode);
    }
    else if (cmpCode && this.datamaster.value.masterType == 'idc') {
      this.IDCList = []
      this.IDCList = await this.commonFunction.getIDCActivity(cmpCode);

    } else if (cmpCode && this.datamaster.value.masterType == 'applysupply') {
      this.applySupplyList = []
      this.applySupplyList = await this.commonFunction.getApplySupply(cmpCode);

    } else if (cmpCode && this.datamaster.value.masterType == 'vmm') {
      this.vmmList = []
      this.vmmList = await this.commonFunction.getVmm(cmpCode);

    } else if (cmpCode && this.datamaster.value.masterType == 'mmm') {
      this.mmmList = []
      this.mmmList = await this.commonFunction.getMmm(cmpCode);

    } else if (cmpCode && this.datamaster.value.masterType == 'unitMaster') {
      this.unitList = []
      this.unitList = await this.commonFunction.getunitMaster(this.datamaster.value.cmpCode);
    }


  }

  async onChangeMaterial(data: any) {

    var found = await this.MaterialList.find((item: any) => { if (item.materialID == data) { return item } })

    if (found) { this.subMaterialList = found.subMaterial; this.MaterialGroupDesc = found.descripption; } else { this.subMaterialList = []; this.MaterialGroupDesc = null }

    


  }




  onChangeIDC(data: any) {

    var found = this.IDCList.find((item: any) => { if (item.idcActivityID == data) { return item } })

    if (found) { this.subIDCList = found.subHead; } else { this.subIDCList = []; }

  }
  // onChangeMaterial(data: any) {

  //  

  //  var found = this.MaterialList.find((item: any) => { if (item.materialID == data) { return item } })

  //  if (found) { this.subMaterialList = found.subMaterial; } else { this.subMaterialList = [] }

  // }


  getmasterdata() { }

  // **********************************************************************************************************
  //                     Get Master API 
  // **********************************************************************************************************
  getMasterData() {

    


    if (this.viewMasterData.valid || this.uploadDataMaster.valid) {

      
      // if (this.viewMasterData.value.cmpCode && this.viewMasterData.value.Master) {
      var head = { cmpCode: this.viewMasterData.value.cmpCode }
      
      var param = {};

      if (this.viewMasterData.controls.Masters.value == 'unitMaster' || this.viewMasterData.controls.Masters.value == 'labour' ||
        this.viewMasterData.controls.Masters.value == 'applysupply' || this.uploadDataMaster.controls.uploadMastersType.value == 'unitMaster' ||
        this.uploadDataMaster.controls.uploadMastersType.value == 'labour' || this.uploadDataMaster.controls.uploadMastersType.value == 'applysupply') {
        param = { cmpCode: this.viewMasterData.value.cmpCode };
      }
      

      var data = { head: head, param: param }
      
      this.projectmasterList = []
      let Api: any = null;
      this.spinnerService.show();
      // Account group master api
      // if (this.viewMasterData.controls.Masters.value == 'accountGroup') {
      //   Api = this.apiCallService.getAcountGroup;
      //   this.displayedColumns = ['action', 'cmpCode', 'actGroupCode', 'actGroupDesc'];
      // }
      //unit master get api 
      if (this.viewMasterData.controls.Masters.value == 'unitMaster' || this.uploadDataMaster.value.uploadMastersType == 'unitMaster') {
        
        Api = this.apiCallService.getunitMaster
        this.displayedColumns = ['action', 'cmpCode', 'standardrUnitCode', 'unitDesc', 'unitCode1', 'unitCode2', 'unitCode3']
      }
      // Departmet Master get api 
      else if (this.viewMasterData.controls.Masters.value == 'departmentDetails') {
        Api = this.apiCallService.getDepartmentById;
        this.displayedColumns = ['action', 'cmpCode', 'deptDesc'];
      }
      //Employee designation get api
      else if (this.viewMasterData.controls.Masters.value == 'empDesignation') {
        Api = this.apiCallService.getDesignationById;
        this.displayedColumns = ['action', 'cmpCode', 'designationDesc'];
      }
      // Labour MAster Get api 
      else if (this.viewMasterData.controls.Masters.value == 'labour' || this.uploadDataMaster.value.uploadMastersType == 'labourGroup' || this.uploadDataMaster.value.uploadMastersType == 'labourDetails') {
        Api = this.apiCallService.getLabourMasterById;
        this.displayedColumns = ['action', 'cmpCode', 'labourID', 'descripption'];
      }
      // Material Master Get Api
      else if (this.viewMasterData.controls.Masters.value == 'material' || this.uploadDataMaster.value.uploadMastersType == 'materialGroup' || this.uploadDataMaster.value.uploadMastersType == 'materialDetails' || this.uploadDataMaster.value.uploadMastersType == 'materialActualGroup') {
        Api = this.apiCallService.getMaterialMasterById;
        this.displayedColumns = ['action', 'cmpCode', 'materialID', 'descripption'];
      }
      // Apply supply master Get Api
      else if (this.viewMasterData.controls.Masters.value == 'applysupply') {
        Api = this.apiCallService.getApplySupply;
        this.displayedColumns = ['action', 'cmpCode', 'applySupplyID', 'descripption'];
      }
      // IDC Master Get Api
      else if (this.viewMasterData.controls.Masters.value == 'idc' || this.uploadDataMaster.value.uploadMastersType == 'idcGroup' || this.uploadDataMaster.value.uploadMastersType == 'idcDetails' || this.uploadDataMaster.value.uploadMastersType == 'idcActualGroup') {
        Api = this.apiCallService.getIDCActivityById;
        this.displayedColumns = ['action', 'cmpCode', 'cmpName', 'idcActivityID', 'description'];
      }

      // VMM cost master Get Api
      else if (this.viewMasterData.controls.Masters.value == 'vmm') {
        Api = this.apiCallService.getVmmMaster;
        this.displayedColumns = ['action', 'cmpCode', 'vmmID', 'description'];
      }
      // VMM cost master Get Api
      else if (this.viewMasterData.controls.Masters.value == 'mmm') {
        Api = this.apiCallService.getmmmMaster;
        this.displayedColumns = ['action', 'cmpCode', 'mmmID', 'description'];
      }
      
      Api(data).subscribe({
        next: async (result: any) => {
          if (result) {

            if (this.viewMasterData.controls.Masters.value == 'accountGroup') {
              if (result.length > 0) {
                this.projectmasterList = result;
                this.dataSource = new MatTableDataSource(this.projectmasterList);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.spinnerService.hide();
              }
              else {
                this.dataSource = new MatTableDataSource();
                this.projectmasterList = [];
                this.spinnerService.hide();
              }
            }
            else {
              if (result.length > 0) {

                this.projectmasterList = await this.commonService.filterIsDeleted(result);

                // Assign the data to the data source for the table to render             
                this.dataSource = new MatTableDataSource(this.projectmasterList);

                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.spinnerService.hide();
              } else {
                this.dataSource = new MatTableDataSource();
                this.spinnerService.hide();
              }
            }
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          this.spinnerService.hide();
        },
      })

    } else {
      this.projectmasterList = []; return;
    }

  }
  //Delete master moodal
  deleteMaster(row: any) {
    this.deleteRow = row;

    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  modal(data: any, index: any) {
    this.modaldata = data;
    this.grouopindex = index;

    this.openDialogWithTemplateRef(this.viewDialog);
  }

  // **********************************************************************************************************
  //                    Delete Masters API
  // **********************************************************************************************************

  deleteMasterData() {
    //Delete All mast
    this.deleteRow;

    var param = {};
    var head = {};
    var data = {};
    let Api: any = null;
    var URI = "";
    if (this.viewMasterData.controls.Masters.value == 'accountGroup') {
      head = { cmpCode: this.deleteRow.cmpCode, ID: this.deleteRow.actGroupCode }
      data = { param: param, head: head }
      Api = this.apiCallService.deleteAccountGroup;
    }
    else if (this.viewMasterData.controls.Masters.value == 'unitMaster') {
      head = { cmpCode: this.deleteRow.cmpCode, ID: this.deleteRow.standardrUnitCode }
      data = { param: param, head: head }
      Api = this.apiCallService.deleteunitMaster;
    }
    else if (this.viewMasterData.controls.Masters.value == 'departmentDetails') {
      head = { cmpCode: this.deleteRow.cmpCode, ID: this.deleteRow.deptCode }
      data = { param: param, head: head }
      Api = this.apiCallService.deleteDepartment;
    }
    else if (this.viewMasterData.controls.Masters.value == 'empDesignation') {
      head = { cmpCode: this.deleteRow.cmpCode, ID: this.deleteRow.descCode }
      data = { param: param, head: head }
      Api = this.apiCallService.deleteDesignation;
    }
    else if (this.viewMasterData.controls.Masters.value == 'labour') {
      URI = "LabourMaster/LabourMaster"
      param = { cmpCode: this.deleteRow.cmpCode, LabourID: this.deleteRow.labourID }
      data = { URI: URI, param: param }
      Api = this.apiCallService.deleteMaster;
    }
    else if (this.viewMasterData.controls.Masters.value == 'material') {
      URI = "MaterialMaster/MaterialMaster"
      param = { cmpCode: this.deleteRow.cmpCode, MaterialID: this.deleteRow.materialID }
      data = { URI: URI, param: param }
      Api = this.apiCallService.deleteMaster;
    }
    else if (this.viewMasterData.controls.Masters.value == 'applysupply') {
      URI = "ApplySupply/ApplySupply"
      param = { cmpCode: this.deleteRow.cmpCode, ApplySupplyID: this.deleteRow.applySupplyID }
      data = { URI: URI, param: param }
      Api = this.apiCallService.deleteMaster;

    }
    else if (this.viewMasterData.controls.Masters.value == 'idc') {
      URI = "IDCActivity/IDCActivity"
      param = { cmpCode: this.deleteRow.cmpCode, IDCActivityID: this.deleteRow.IDCActivityID }
      data = { URI: URI, param: param }
      Api = this.apiCallService.deleteMaster;

    }

    Api(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess(result.data);
          const index = this.projectmasterList.indexOf(this.deleteRow);
          if (index > -1) {
            this.projectmasterList.splice(index, 1);

          }

          if (this.projectmasterList.length > 0) {
            // this.projectmasterList = this.commonService.filterIsDeleted(result);
            this.dataSource = new MatTableDataSource(this.projectmasterList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }

        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    }
    )
  }

  resetFile() {
    this.uploadDataMaster.patchValue({ FileUpload: null })
  }
  //Filter only deleted items
  async filterIsDeleted(data: any) {
    return new Promise((resolve, reject) => {
      resolve(data.filter((obj: any) => obj.isDeleted == false));
    })
  }

  OnSelectLabourGroup(data: any) {
    this.datamaster.patchValue({
      labourID: data
    })
  }
}



export interface masterData {
  cmpCode: string;
  actGroupCode: string;
  actGroupDesc: string;

  standardrUnitCode: string,
  unitCode1: string,
  unitCode2: string,
  unitCode3: string,

  deptDesc: string;
  designationDesc: string,
  labourID: string,
  descripption: string,

  materialID: string,
  applySupplyID: string,
  vmmID: string,
  mmmID: string,
  description: string,
  isDeleted: string,
}
