import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectcreationRoutingModule } from './projectcreation-routing.module';

import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ProjectcreationComponent } from './projectcreation.component';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'src/app/Shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [ProjectcreationComponent],
  imports: [ 
    CommonModule,
    FormsModule,
    ProjectcreationRoutingModule,
    MatDialogModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ]
})
export class ProjectcreationModule { }
