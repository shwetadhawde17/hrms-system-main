import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectMaterialLabourComponent } from './project-material-labour.component';

describe('ProjectMaterialLabourComponent', () => {
  let component: ProjectMaterialLabourComponent;
  let fixture: ComponentFixture<ProjectMaterialLabourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectMaterialLabourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectMaterialLabourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
