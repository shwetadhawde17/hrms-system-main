import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoquploadComponent } from './boqupload.component';

describe('BoquploadComponent', () => {
  let component: BoquploadComponent;
  let fixture: ComponentFixture<BoquploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoquploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoquploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
