import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectcreationComponent } from './projectcreation.component';

const routes: Routes = [
  { path: '', component: ProjectcreationComponent, children: [] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectcreationRoutingModule { }
