import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { ComponentsComponent } from '../components.component';
import { JsonToXlsxService } from 'src/app/services/json-to-xlsx.service';
import { XlsxToJsonService } from 'src/app/services/xls-to-json.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-projectmaster',
  templateUrl: './projectmaster.component.html',
  styleUrls: ['./projectmaster.component.scss']
})
export class ProjectmasterComponent implements OnInit {
  master: FormGroup
  getmaster: FormGroup
  uploadMaster: FormGroup;

  submitted: any;
  CompanyList: any;
  masterDataType: any;
  isActiveTab: number = 0;
  updateRow: any;
  isCreate: number = 0;
  ELEMENT_DATA: any;
  convertedJSON: any = [];
  // View dataTabale
  projectmasterList: any=[];
  displayedColumns: string[] = [];
  ProjectDesignationJSON: any = [];
  ProjectTypeJSON: any = [];
  ProjectExecutionJSON: any = [];

  ProjectTypeList = [{ Code: 'projectExecution', Name: 'Project Execution' }, { Code: 'projectType', Name: 'Project Type' }, { Code: 'projectDesignation', Name: 'Project Designation' }]
  dataSource!: MatTableDataSource<masterData>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  // Mat Dialogs.
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  @ViewChild(FormGroupDirective) myForm: any;
  deleteRow: any;

  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  private xlsxToJsonService: XlsxToJsonService = new XlsxToJsonService();
  constructor(private JsonToXlsxService: JsonToXlsxService, private commomFunction: ComponentsComponent, private spinnerService: NgxSpinnerService, private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.master = this.fb.group({
      masterDataType: [null, [Validators.required]]
    })
    this.getmaster = this.fb.group({
      getExecutionType: [null, [Validators.required]],
      cmpCode: [null, [Validators.required]],
    })
    this.uploadMaster = this.fb.group({
      cmpCode: [null, Validators.required],
      uploadDataType: [null, Validators.required],
      FileUpload: [null, Validators.required]
    })


  }
  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany();
  }


  downloadFile() {
    var arr = [];
    var headersArray: any = [];
    var fileName = ""
    if (this.uploadMaster.value.uploadDataType && this.uploadMaster.value.cmpCode) {
      if (this.uploadMaster.value.uploadDataType == 'projectType' && this.uploadMaster.value.cmpCode) {
        for (var L = 1, LLen = 20; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadMaster.value.cmpCode, activityName: ""
          })
        }
        headersArray = ['Company Code', 'Project Type'];
        fileName = "Project Type"

      } else if (this.uploadMaster.value.uploadDataType == 'projectExecution' && this.uploadMaster.value.cmpCode) {
        for (var L = 1, LLen = 20; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadMaster.value.cmpCode, activityName: ""
          })
        }
        headersArray = ['Company Code', 'Activity Name'];
        fileName = "Project Execution"

      }
      else if (this.uploadMaster.value.uploadDataType == 'projectDesignation' && this.uploadMaster.value.cmpCode) {
        for (var L = 1, LLen = 20; L < LLen; L++) {
          arr.push({
            cmpCode: this.uploadMaster.value.cmpCode, activityName: ""
          })
        }
        headersArray = ['Company Code', 'Designation'];
        fileName = "Project Designation"

      }
      this.JsonToXlsxService.exportExcelWithoutHead(arr, headersArray, fileName)
    }
    else {
      this.commonService.showError("Plese select Masters and Company!")
    }
  }

  // add remove input field 
  addExecution() {
    if (this.master.value.masterDataType == 'projectExecution') {
      this.master.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.master.addControl('activityName', this.fb.control('', [Validators.required]));
      if (this.master.controls.projectTypeName) { this.master.removeControl('projectTypeName'); }
      if (this.master.controls.designation) { this.master.removeControl('designation'); }
    }
    else if (this.master.value.masterDataType == 'projectType') {
      this.master.addControl('cmpCode', this.fb.control(null, [Validators.required]))
      this.master.addControl('projectTypeName', this.fb.control('', [Validators.required]));
      if (this.master.controls.activityName) { this.master.removeControl('activityName'); }
      if (this.master.controls.designation) { this.master.removeControl('designation'); }
    }
    else if (this.master.controls.masterDataType.value == 'projectDesignation') {
      this.master.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.master.addControl('designation', this.fb.control('', [Validators.required]));
      if (this.master.controls.activityName) { this.master.removeControl('activityName'); }
      if (this.master.controls.projectTypeName) { this.master.removeControl('projectTypeName'); }
    }
  }
  // Active Deactive matTab change event for Add method or update method

  tabIsActive(value: any, change: any) {
    // console.log('add', value, change)
    this.isCreate = value;
    if (value != change) {
      this.updateRow = null
      if (value == 1 && change == 0) {
        this.isCreate = 0;
        // console.log('update', value)
      } else if (value == 2 && change == 0) {
        // console.log('upload', value)
      }
      this.master.reset();
      this.isActiveTab = value;
    }
    this.isActiveTab = value;
    this.master.enable();
    if (this.isCreate == 2) { this.uploadMaster.reset(); }
    // console.log(this.isActiveTab)
  }
  //Edit master 
  editMaster(row: any) {
    this.updateRow = null;
    this.updateRow = row;

    this.master.patchValue({ masterDataType: this.getmaster.value.getExecutionType });
    this.getmaster.patchValue({ getExecutionType: this.master.value.masterDataType });

    if (this.master.value.masterDataType == 'projectExecution') {
      this.master.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.master.addControl('activityName', this.fb.control('', [Validators.required]));
      if (this.master.controls.projectTypeName) { this.master.removeControl('projectTypeName'); }
      if (this.master.controls.designationCode) { this.master.removeControl('designationCode'); }
      if (this.master.controls.designation) { this.master.removeControl('designation'); }
      this.tabIsActive(1, 1);
      this.master.patchValue(row)
    }
    else if (this.master.value.masterDataType == 'projectType') {
      this.master.addControl('cmpCode', this.fb.control(null, [Validators.required]))
      this.master.addControl('projectTypeName', this.fb.control('', [Validators.required]));
      if (this.master.controls.activityName) { this.master.removeControl('activityName'); }
      if (this.master.controls.designationCode) { this.master.removeControl('designationCode'); }
      if (this.master.controls.designation) { this.master.removeControl('designation'); }
      this.tabIsActive(1, 1);
      this.master.patchValue(row)
    }
    else if (this.master.controls.masterDataType.value == 'projectDesignation') {
      this.master.addControl('cmpCode', this.fb.control(null, [Validators.required]));
      this.master.addControl('designationCode', this.fb.control('', [Validators.required]));
      this.master.addControl('designation', this.fb.control('', [Validators.required]));
      if (this.master.controls.activityName) { this.master.removeControl('activityName'); }
      if (this.master.controls.projectTypeName) { this.master.removeControl('projectTypeName'); }
      this.tabIsActive(1, 1);
      this.master.patchValue(row)
    }
  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
// Remove This code and use myform FormGroupDirective to reset the form 
  public validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  getmasterdata() { }

  //Get Project Masters.
  async getprojectmaster(cmpCode: any, ExecutionType: any) {
    if (this.getmaster.valid) {
      var head = { cmpCode: cmpCode };
      var data = { head: head }
      this.projectmasterList = []
      let Api: any = null;
      this.spinnerService.show();

      if (ExecutionType === 'projectExecution') {
        Api = this.apiCallService.getExecutionTypeById;
        this.displayedColumns = ['action', 'cmpName', 'activityName'];
      }
      else if (ExecutionType === 'projectType') {
        Api = this.apiCallService.getProjectTypeId
        this.displayedColumns = ['action', 'cmpName', 'projectTypeName'];
      }
      else if (ExecutionType === 'projectDesignation') {
        Api = this.apiCallService.getProjectDesignationById;
        this.displayedColumns = ['action', 'cmpName', 'designation'];
      }
      Api(data).subscribe({
        next: (result: any) => {
          if (result) {
            if (result.length > 0) {
              this.projectmasterList = result;
              this.dataSource = new MatTableDataSource(this.projectmasterList);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              this.spinnerService.hide();
            }
            else {
            
              this.dataSource = new MatTableDataSource(this.projectmasterList);
              this.projectmasterList = [];
              this.spinnerService.hide();
            }
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
          this.spinnerService.hide();
        },
      })

    } else { this.projectmasterList = []; return; }
  }
  //Add, Update and File Upload.
  async addMaster() {

    if (this.master.valid || this.ProjectTypeJSON.length > 0 || this.ProjectExecutionJSON.length > 0 || this.ProjectDesignationJSON.length > 0) {

      var JSON = null;
      var param = {};
      var head = {}
      var data = {}
      var Api: any = null;
      if (this.master.controls.masterDataType.value === 'projectExecution' || this.uploadMaster.value.uploadDataType == 'projectExecution') {

        if (this.ProjectExecutionJSON.length > 0) { JSON = this.ProjectExecutionJSON } else { JSON = [this.master.value]; }
        if (this.isCreate == 0 || this.ProjectExecutionJSON.length > 0) {

          data = { data: JSON, param: param };
          Api = this.apiCallService.addExecutionType;
        }
        else {
          JSON = JSON[0]
          JSON.activityCode = this.updateRow.activityCode;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.activityCode }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.updateExecutionType;
        }
      }
      else if (this.master.controls.masterDataType.value == 'projectType' || this.uploadMaster.value.uploadDataType == 'projectType') {
        if (this.ProjectTypeJSON.length > 0) { JSON = this.ProjectTypeJSON } else { JSON = [this.master.value]; }
        if (this.isCreate == 0 || this.ProjectTypeJSON.length > 0) {
          data = { data: JSON, param: param };
          Api = this.apiCallService.addProjectType;
        }
        else {
          JSON = JSON[0]
          JSON.projectTypeCode = this.updateRow.projectTypeCode;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.projectTypeCode }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.updateProjectType;
        }
      }
      else if (this.master.controls.masterDataType.value == 'projectDesignation' || this.uploadMaster.value.uploadDataType == 'projectDesignation') {
        if (this.ProjectDesignationJSON.length > 0) { JSON = this.ProjectDesignationJSON } else { JSON = [this.master.value]; }
        if (this.isCreate == 0 || this.ProjectDesignationJSON.length > 0) {
          data = { data: JSON, param: param };
          Api = this.apiCallService.addProjectDesignation;
        }
        else {
          JSON = JSON[0]
          JSON.DesignationCode = this.updateRow.designationCode;
          head = { cmpCode: this.updateRow.cmpCode, ID: this.updateRow.designationCode }
          data = { data: JSON, param: param, head: head }
          Api = this.apiCallService.updateProjectDesignation;
        }
      }
      Api(data).subscribe({
        next: async (result: any) => {
          if (result) {
            this.commonService.showSuccess("Record Inserted Succefully!");
            if (this.master.value.masterDataType) {
              this.getmaster.patchValue({ cmpCode: this.master.value.cmpCode, getExecutionType: this.master.value.masterDataType });
              this.getprojectmaster(this.master.value.cmpCode, this.master.value.masterDataType)
            } else {
              this.getmaster.patchValue({ cmpCode: this.uploadMaster.value.cmpCode, getExecutionType: this.uploadMaster.value.uploadDataType })
              this.getprojectmaster(this.uploadMaster.value.cmpCode, this.uploadMaster.value.masterDataType)
            }

            this.spinnerService.show();

            if (this.master.value.masterDataType == 'projectExecution' || this.uploadMaster.value.uploadDataType == 'projectExecution') {
              if (this.master.value.cmpCode) { this.projectmasterList = await this.commomFunction.getExecutaionList(this.master.value.cmpCode); }
              else { this.projectmasterList = await this.commomFunction.getExecutaionList(this.uploadMaster.value.cmpCode); }
              this.displayedColumns = ['action', 'cmpName', 'activityName'];
            } else if (this.master.value.masterDataType == 'projectType' || this.uploadMaster.value.uploadDataType == 'projectType') {
              if (this.master.value.cmpCode) { this.projectmasterList = await this.commomFunction.getprojectTypeList(this.master.value.cmpCode); }
              else { this.projectmasterList = await this.commomFunction.getprojectTypeList(this.uploadMaster.value.cmpCode); }
              this.displayedColumns = ['action', 'cmpName', 'projectTypeName'];
            } else if (this.master.value.masterDataType == 'projectDesignation' || this.uploadMaster.value.uploadDataType == 'projectDesignation') {
              if (this.master.value.cmpCode) { this.projectmasterList = await this.commomFunction.getdesignationList(this.master.value.cmpCode); }
              else { this.projectmasterList = await this.commomFunction.getdesignationList(this.uploadMaster.value.cmpCode); }
              this.displayedColumns = ['action', 'cmpName', 'designation'];
            } else {
              this.projectmasterList = []
              this.spinnerService.hide();
              this.dataSource = new MatTableDataSource();
            }
            this.tabIsActive(0, 0);
            if (this.projectmasterList.length > 0) {
              // Assign the data to the data source for the table to render
              this.dataSource = new MatTableDataSource(this.projectmasterList);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
              this.spinnerService.hide();
              this.master.reset();
              this.uploadMaster.reset();
              this.ProjectTypeJSON = [];
              this.ProjectExecutionJSON = []
              this.ProjectDesignationJSON = [];
            } else {
              this.projectmasterList = []
              this.spinnerService.hide();
              this.dataSource = new MatTableDataSource();
            }
          }
        }, error: (error: any) => {
          this.spinnerService.hide();
          this.commonService.showError(error);
        }
      })
    } else {
      this.validateAllFormFields(this.master);
    }
  }

  //Delete Model
  deleteMaster(row: any) {
    this.deleteRow = row;
    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  //Delete Project Masters
  deleteMasterData() {
    var param = {};
    var head = {};
    var data = {};
    let Api: any = null;

    if (this.getmaster.controls.getExecutionType.value == 'projectExecution') {
      head = { cmpCode: this.deleteRow.cmpCode, ID: this.deleteRow.activityCode }
      data = { param: param, head: head }
      Api = this.apiCallService.deleteExecutionType;

    }
    else if (this.getmaster.controls.getExecutionType.value == 'projectType') {
      head = { cmpCode: this.deleteRow.cmpCode, ID: this.deleteRow.projectTypeCode }
      data = { param: param, head: head }
      Api = this.apiCallService.deleteProjectType;
    }
    else if (this.getmaster.controls.getExecutionType.value == 'projectDesignation') {
      head = { cmpCode: this.deleteRow.cmpCode, ID: this.deleteRow.designationCode }
      data = { param: param, head: head }
      Api = this.apiCallService.deleteProjectDesignation;
    }
    Api(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.commonService.showSuccess(result.data);
          const index = this.projectmasterList.indexOf(this.deleteRow);
          if (index > -1) { // only splice array when item is found
            this.projectmasterList.splice(index, 1); // 2nd parameter means remove one item only   
          }

          if (this.projectmasterList.length > 0) {
            this.dataSource = new MatTableDataSource(this.projectmasterList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    }
    )
  }
  // File Upload
  async selectFile(event: any) {
    var files = event.target.files;
    if (files.length == 0) {
      this.commonService.showError("Please select excel file Ex.(.XLS,.XLSX).");
      return;
    }
    this.uploadMaster.patchValue({ FileUpload: files[0].name })
    
    var filename = files[0].name;
    var extension = filename.substring(filename.lastIndexOf(".")).toUpperCase();
    if (extension == '.XLS' || extension == '.XLSX') {

      const fileReader = new FileReader();
      fileReader.readAsBinaryString(files[0])
      fileReader.onload = (event: any) => {

        const BinaryData = event.target['result'];
        let workbook = XLSX.read(BinaryData, { type: 'binary' })
        // Read multiple sheets here ...
        workbook.SheetNames.forEach(async (sheetsName: any) => {
          const worksheet = workbook.Sheets[sheetsName];
          let xlsData: any = XLSX.utils.sheet_to_json(worksheet, { header: 1 });

          var arr: any = [];
          if (this.uploadMaster.value.uploadDataType == 'projectType') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], projectTypeName: xlsData[i][1] })
            }
            this.ProjectTypeJSON = await this.ProjectTypeSort(arr);
            
          }
          if (this.uploadMaster.value.uploadDataType == 'projectExecution') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], activityName: xlsData[i][1] })
            }
            this.ProjectExecutionJSON = await this.ProjectExecutionSort(arr);
            
          }
          if (this.uploadMaster.value.uploadDataType == 'projectDesignation') {
            for (let i = 1; i < xlsData.length; i++) {
              arr.push({ cmpCode: xlsData[i][0], designation: xlsData[i][1] })
            }
            this.ProjectDesignationJSON = await this.ProjectDesignationSort(arr);
            
          }

        });

      }
    } else {
      this.commonService.showError("Please select a valid excel file Ex.(.XLS,.XLSX).");
    }
  }
  // JSON Sort For Upload Project Master
  ProjectTypeSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": this.uploadMaster.value.cmpCode,
          "projectTypeName": element.projectTypeName,
        })
      });
      resolve(arr)
    })
  }
  ProjectExecutionSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": this.uploadMaster.value.cmpCode,
          "activityName": element.activityName,
        })
      });
      resolve(arr)
    })
  }
  ProjectDesignationSort(data: any) {
    var arr: any = []
    return new Promise(async (resolve, reject) => {
      await data.forEach((element: any) => {
        arr.push({
          "cmpCode": this.uploadMaster.value.cmpCode,
          "designation": element.designation,
        })
      });
      resolve(arr)
    })
  }

  //Filter only deleted items
  async filterIsDeleted(data: any) {
    return new Promise((resolve, reject) => {
      resolve(data.filter((obj: any) => obj.isDeleted == false));
    })
  }
}

export interface masterData {
  cmpCode: string,
  projectType: string,
  projectTypeName: string;
  activityCode: string;
  activityName: string;
  designationCode: string;
  designation: string;
  cmpName: string;
  projectDesignation: string;
}

