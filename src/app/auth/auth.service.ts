import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable()
export class AuthService {
    constructor(private myRoute: Router) { }
    sendToken(token: string) {
        sessionStorage.setItem("loginData", token)
    }
    getToken() {
        
        return sessionStorage.getItem("loginData")
        
    }
    isLoggednIn() {
        return this.getToken() !== null;
    }
    logout() {
        sessionStorage.removeItem("loginData");
        this.myRoute.navigate(["login"]);
    }
}