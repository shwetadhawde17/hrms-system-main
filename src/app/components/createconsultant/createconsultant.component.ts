import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective } from "@angular/forms";
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { mobileValidation, EmailValidation, panValidation, gstinValidation, ZipValidation } from '../../services/validations';
import { ViewChild, AfterViewInit, TemplateRef } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { ComponentsComponent } from '../components.component';

@Component({
  selector: 'app-createconsultant',
  templateUrl: './createconsultant.component.html',
  styleUrls: ['./createconsultant.component.scss']
})
export class CreateconsultantComponent implements OnInit {
  createConsultant: FormGroup;
  submitted: any;
  CompanyList: any;
  ConsultantList: any;
  FilterCompanyList: any;
  PlantList: any;
  CountryList: any;
  FilterCountryList: any;
  StateList: any;
  FilterStateList: any;


  updateRow: any;
  deleteRow: any;
  isActiveTab: number = 0;
  isCreate: number = 0;
  displayedColumns = ['action', 'bpCode', 'bpFName', 'pan', 'email', 'phone', 'street', 'city', 'postalCode'];
  TableConsultant:FormGroup;

  dataSource!: MatTableDataSource<CmpData>;
  viewDataSource!: MatTableDataSource<PlantData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(FormGroupDirective) myForm: any;
  // Create 100 CompanyList

  constructor(private commomFunction: ComponentsComponent, private spinnerService: NgxSpinnerService, private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createConsultant = this.fb.group({
      bpCode: [null],
      cmpCode: [null, [Validators.required]],
      // plantCode: [null, [Validators.required]],
      actGroupCode: ["CON", [Validators.required]],
      bpFName: [null, [Validators.required,Validators.maxLength(50)]],
      shortKey: [null, [Validators.required,Validators.maxLength(10)]],
      pan: [null, [Validators.required,panValidation]],
      gstNumber: [null, [Validators.required,gstinValidation]],
      addressLine1: [null,Validators.maxLength(50)],
      addressLine2: [null,Validators.maxLength(50)],
      street: [null, [Validators.required,Validators.maxLength(45)]],
      city: [null, [Validators.required,Validators.maxLength(45)]],
      postalCode: [null, [Validators.required, ZipValidation]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      status: [true, [Validators.required]],
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      isDeleted: [null],
      relationPersons: [null],
    })
    this.TableConsultant = this.fb.group({ cmpCode: [null, [Validators.required]],})
  }
  async ngOnInit() {
    document.body.style.backgroundColor = "#E5E7E9";
    this.CompanyList = await this.commomFunction.getCompany();
    this.CountryList = await this.commomFunction.getCountry();
  }
  // Active Deactive matTab change event for Add method or update method
  async tabIsActive(value: any, change: any) {
    if (value != change) {
      this.updateRow = null, value = 0
    }

    this.isActiveTab = value;
    this.isCreate = value;
    this.createConsultant.reset();
    this.myForm.resetForm();
    this.createConsultant.enable();

  }
  // Mat Dialogs.
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }

  async editConsultant(row: any) {
    this.updateRow = row;
    this.tabIsActive(1, 1);
    // Use add form for update patch values
    this.createConsultant.patchValue(row)
    this.StateList = await this.commomFunction.getState(row.countryCode);
  }

  viewConsultant(row: any) {
    this.updateRow = row;
    this.tabIsActive(1, 1);
    // Use add form for update patch values
    this.createConsultant.patchValue(row)
    this.createConsultant.disable();
    this.isCreate = 2;
  }
  deleteCon(row: any) {
    this.deleteRow = row;
    
    this.openDialogWithTemplateRef(this.deleteDialog);
  }

  public myError = (controlName: string, errorName: string) => {
    return this.createConsultant.controls[controlName].hasError(errorName);
  }

  changeState(event: any) {
    if (event.target.value)
      this.FilterStateList = this._filterState(event.target.value);
    else
      this.FilterStateList = this.StateList;
  }
  private _filterState(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.StateList.filter((item: any) => item.stateCode.toLowerCase().includes(filterValue) || item.stateName.toLowerCase().includes(filterValue));
  }

  async onChangeCountry(data: any) {
    this.StateList = await this.getState(data);
  }
  async getState(CountryCode: any) {
    return new Promise((resolve, reject) => {
      if (CountryCode) {
        var param = { CountryCode: CountryCode }
        var data = { param: param }
        this.apiCallService.getState(data).subscribe({
          next: async (result: any) => {
            if (result) {
              resolve(result)
            }
          }, error: (error: any) => {
            this.commonService.showError(error.error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let result: any = [];
        this.createConsultant.controls['stateCode'].setValue(null);
        resolve(result)
      }
    })
  }

  deleteConsultant() {
    this.deleteRow;
    
    // var head= {bpCode:this.deleteRow.bpCode}
    var head = { cmpCode: this.deleteRow.cmpCode, bpCode: this.deleteRow.bpCode }
    var data = { head: head }
    this.apiCallService.deleteBusinessPartner(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Deleted Succefully!");
          const index = this.ConsultantList.indexOf(this.deleteRow);
          if (index > -1) { // only splice array when item is found
            this.ConsultantList.splice(index, 1); // 2nd parameter means remove one item only
          }
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.ConsultantList);
        }
      },
      error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
  addConsultant() {
    this.isActiveTab = 1;
    
    this.createConsultant.patchValue(
      {
        actGroupCode: "CON",
        status: true
      }
    )
    if (this.createConsultant.invalid) {
      this.submitted = true;
      this.createConsultant.markAllAsTouched()
      return;
    }
    let Api = this.apiCallService.addBusinessPartner;
    
    var JSON = this.createConsultant.value;
    JSON.createBy = environment.createBy; // Loged IN User ID
    JSON.updateBy = environment.updateBy; // Loged IN User ID
    JSON.relationPersons = []
    var head = {}
    if (this.isCreate == 1) { Api = this.apiCallService.updateBusinessPartner, head = { cmpCode: this.updateRow.cmpCode, bpCode: this.updateRow.bpCode } }
    var data = { data: JSON, head: head }
    console.log('data',data)
    Api(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");

          this.tabIsActive(0, 0);
          this.createConsultant
          this.ConsultantList = await this.commomFunction.getBusinessPartners(JSON.cmpCode,'CON');
          if (this.ConsultantList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.ConsultantList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
          this.createConsultant.reset();
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })

  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  async onChangeCompany(data: any) {
    this.spinnerService.show();
    this.ConsultantList = await this.commomFunction.getBusinessPartners(data,'CON');
    if (this.ConsultantList.length > 0) {
      // Assign the data to the data source for the table to render
      this.dataSource = new MatTableDataSource(this.ConsultantList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinnerService.hide();
    } else {
      this.ConsultantList = [];
      this.spinnerService.hide();
    }
    // this.spinnerService.hide();
  }

}


export interface CmpData {
  bpCode: string;
  cmpCode: string;
  plantCode: string;
  bpFName: string;
  shortKey: string;
  pan: string;
  gstNumber: string;
  addressLine1: string;
  addressLine2: string;
  street: string;
  city: string;
  postalCode: string;
  email: string;
  phone: string;
  stateCode: string;
  countryCode: string;
  status: string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;

}

export interface PlantData {
  cmpCode: string;
  plantCode: string;
  plantName: string;
  shortKey: string;
  street: string;
  addressLine1: string;
  addressLine2: string;
  city: string;
  postalCode: string;
  email: string;
  phone: string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;
}

