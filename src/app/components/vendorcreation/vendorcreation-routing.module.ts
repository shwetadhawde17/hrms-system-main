import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VendorcreationComponent } from './vendorcreation.component';
const routes: Routes = [
  { path: '',component: VendorcreationComponent,children: [] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorcreationRoutingModule { }
