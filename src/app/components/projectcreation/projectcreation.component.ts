import { Component, OnInit, ViewChild } from '@angular/core';
import { TemplateRef } from '@angular/core';
// import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { CreateclientComponent } from '../createclient/createclient.component';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray, FormGroupDirective } from "@angular/forms";
import { Observable } from 'rxjs';
import { mobileValidation, EmailValidation, ZipValidation } from '../../services/validations';

import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from 'src/environments/environment';
import { MatAccordion } from "@angular/material/expansion";
import { ComponentsComponent } from '../components.component';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-projectcreation',
  templateUrl: './projectcreation.component.html',
  styleUrls: ['./projectcreation.component.scss'],
  providers: [ComponentsComponent],
})
export class ProjectcreationComponent implements OnInit {
  createProject: FormGroup;
  createPerson: FormGroup;
  ViewTablePrj:FormGroup;
  submitted: any;
  currentIndex: any;
  CompanyList: any;
  ExecutaionList: any;
  projectTypeList: any;
  PlantList: any;
  CountryList: any;
  FilterCompanyList: any;
  FilterCountryList: any;
  StateList: any;
  FilterStateList: any;
  PrjList: any;
  FilterPrjList: any;
  options: string[] = ['India'];
  filteredOptions?: Observable<string[]>;
  designationList: any;
  bpCodeList: any;
  clientList: any;
  isActiveTab: any;
  isCreate: any;
  updateRow: any;
  projectList: any;
  RoleList = ['Commercial', 'Technical']
  displayProjectColumns = ['action', 'projName', 'activityCode', 'projectTypeCode', 'addressLine1', 'addressLine2', 'street', 'city', 'postalCode', 'projectCost', 'projectStartDate', 'projectEndDate',];
  dataSource!: MatTableDataSource<PrjData>;
  clientContactPersonList: any;
  consultantList: any;
  startMinDate = new Date();
  endMinDate = new Date();
  @ViewChild('accordion', { static: true }) Accordion?: MatAccordion;
  @ViewChild(FormGroupDirective) myForm: any;
  deleteRow: any;
  constructor(private spinnerService: NgxSpinnerService, private commomFunction: ComponentsComponent, private fb: FormBuilder, private dialog: MatDialog, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createPerson = this.fb.group({
      persontype: [null, [Validators.required]],
      name: [null, [Validators.required]],
      phone: [null, [Validators.required, mobileValidation]],
      emailId: [null, [Validators.required, EmailValidation]],
    })
    this.ViewTablePrj = this.fb.group({cmpCode: [null, [Validators.required]]})
    //Project Creation Form Group
    this.createProject = this.fb.group({
      projCode: [null, [Validators.required,Validators.maxLength(10)]],
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      activityCode: [null, [Validators.required]],
      projectTypeCode: [null, [Validators.required]],
      projName: [null, [Validators.required,Validators.maxLength(45)]],
      projDesc: [null, [Validators.required,Validators.maxLength(45)]],
      addressLine1: [null,Validators.maxLength(50)],
      addressLine2: [null,Validators.maxLength(50)],
      street: [null, [Validators.required,Validators.maxLength(45)]],
      city: [null, [Validators.required,Validators.maxLength(45)]],
      postalCode: [null, [Validators.required, ZipValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      projectCost: [null, [Validators.required]],
      projectStartDate: [null, [Validators.required]],
      projectEndDate: [null, [Validators.required]],
      projectStatus: ["1"],
      clientCode: [null, [Validators.required]],
      consultantCode: [null, [Validators.required]],
      createdBy: null,
      createdOn: null,
      updatedBy: null,
      updatedOn: null,

      // Block Array 
      blocks: this.fb.array([this.BlockdataGroup()]),
      // Store Array
      store: this.fb.array([this.StoredataGroup()]),
      // Project Key Person Array
      projectKeypersons: this.fb.array([this.keyPersonGroup()]),
      // Client contact details array 
      projectContactPersns: this.fb.array([this.clientDataGroup()]),

    })
  }
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('clientDialog') clientDialog!: TemplateRef<any>;
  //Add new Contact Person Modal
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  openModal() {
    this.openDialogWithTemplateRef(this.clientDialog);
  }
  async ngOnInit() {
    // this.projectList = await this.commomFunction.getproject(null, null);

    this.CompanyList = await this.commomFunction.getCompany();
    // this.spinnerService.show();
    // 
    // if (this.projectList.length > 0) {
    //   // Assign the data to the data source for the table to render
    //   this.dataSource = new MatTableDataSource(this.projectList);
    //   this.dataSource.paginator = this.paginator;
    //   this.dataSource.sort = this.sort;
    // } else {
    //   this.projectList = []; this.spinnerService.hide();
    // }
    // this.spinnerService.hide();

    // On Patch Value Change Event Call
    this.createProject.controls['cmpCode'].valueChanges.subscribe((value) => {
      this.onChangeCmp(value)
    });

    this.createProject.controls['countryCode'].valueChanges.subscribe((value) => {
      this.onChangeCountry(value)
    });

  }

  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {
    if (value != change) {
      this.updateRow = null, value = 0
      this.createProject.reset();
    }

    this.isActiveTab = value;
    this.isCreate = value;

    this.createProject.enable();
  }

  submitForm() {
    if (this.createPerson.invalid) {
      this.submitted = true;
      return;
    }
  }
  async clientContactPerson(clientCode: any) {
    return new Promise((resolve, reject) => {
      if (clientCode) {

        var find = this.clientList.find((item: any) => item.bpCode == clientCode);
        resolve(find.relationPersons)
      } else {
        let res: any = [];
        resolve(res)
      }
    });
  }

  public myError = (controlName: string, errorName: string) => {
    return this.createProject.controls[controlName].hasError(errorName);
  }
  public Error = (controlName: string, errorName: string) => {
    return this.createPerson.controls[controlName].hasError(errorName);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  //*************Block Section****************//
  //get Blocks Form Array
  get blockArrayError(): any {
    return <FormArray>this.createProject.get('blocks') as FormArray;
  }
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.blockArrayError['controls'][i]['controls'][controlName].hasError(errorName)
  }

  blocks(): any {
    return <FormArray>this.createProject.get('blocks') as FormArray;
  }

  //add Blocks Form Array
  addBlock() {
    this.blocks().push(this.BlockdataGroup());
  }
  //remove Blocks Form Array
  removeBlock(index: number) {
    this.blocks().removeAt(index);
  }
  //Blocks Form Array function
  private BlockdataGroup(): FormGroup {
    return this.fb.group({
      blockCode: null,
      cmpCode: null,
      plantCode: null,
      projCode: null,
      blockName: [null, [Validators.required,Validators.maxLength(45)]],
      Description: [null, [Validators.required,Validators.maxLength(45)]],
      buildings: new FormArray([
        this.BuildingGroup()
      ])
    });
  }



  //*************Block Section END****************//

  //*************Building Section****************//
  //get Buildings Form Array
  get arrayErrorBuilding(): any {
    return <FormArray>this.createProject.get('buildings') as FormArray;
  }

  buildings(index: number): any {
    return <FormArray>this.blocks().at(index).get('buildings') as FormArray;
  }
  //add Buildings Form Array
  addBuilding(index: number) {
    this.buildings(index).push(this.BuildingGroup());
  }
  //remove Building Form Array
  removeBuildingArray(i: number, j: number) {
    this.buildings(i).removeAt(j);
  }
  //Buildings Form Array function
  private BuildingGroup(): FormGroup {
    return this.fb.group({
      buildingName: [null, [Validators.required,Validators.maxLength(25)]],
      Description: [null, [Validators.required,Validators.maxLength(45)]],
      buildCode: null,
      cmpCode: null,
      plantCode: null,
      projCode: null,
      blockCode: null,
      Floors: new FormArray([])
    })
  }
  //*************Building Section END****************//

  //*************Floor Section****************//

  Floors(i: any, j: any): any {
    // 
    return <FormArray>this.buildings(i).at(j).get('Floors') as FormArray;
  }
  //add Buildings Form Array
  addFloor(i: number, k: any) {
    this.Floors(i, k).push(this.FloorGroup());
  }
  // //remove Building Form Array
  removeFloorArray(i: number, j: number, k: number) {
    this.Floors(i, j).removeAt(k);
  }
  //Buildings Form Array function
  private FloorGroup(): FormGroup {
    return this.fb.group({
      floorCode: null,
      buildCode: null,
      cmpCode: null,
      plantCode: null,
      projCode: null,
      blockCode: null,
      floorName: null,
      floorDesc: null
    })
  }
  //*************Floor Section END****************//

  //*************Store Section****************//
  get storeArrayError(): any {
    return <FormArray>this.createProject.get('store') as FormArray;
  }
  public storeErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.storeArrayError['controls'][i]['controls'][controlName].hasError(errorName)
  }
  //get Store Form Array
  stores(): FormArray {
    return <FormArray>this.createProject.get('store') as FormArray;
  }
  // add store Form Array
  addStores() {
    this.stores().push(this.StoredataGroup());
  }
  //Store Form Array function
  StoredataGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      plantCode: null,
      projCode: null,
      storeCode: null,
      storeName: [null, [Validators.required,Validators.maxLength(45)]],
      Description: [null, [Validators.required,Validators.maxLength(50)]],
      subStore: new FormArray([
        // this.SubStoreGroup()
      ])
    });
  }
  //*************Store Section END****************//

  //*************Sub Store Section****************//
  //get substores Form Array
  subStores(index: number): FormArray {
    return this.stores().at(index).get('subStore') as FormArray;
  }
  //add Sub store Form Array
  addSubStore(index: number) {
    this.subStores(0).push(this.SubStoreGroup());
  }
  //remove Sub store Form Array
  removeSubStores(i: number, j: number) {
    this.subStores(0).removeAt(j);
  }

  //Sub store Form Array function
  private SubStoreGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      plantCode: null,
      projCode: null,
      storeCode: null,
      subStoreCode: null,
      storeName: [null, [Validators.required]],
      Description: [null, [Validators.required]],
    })
  }
  //*************Store Section END****************//

  //*************Key Person Section****************//
  get projectKeypersonsError(): any {
    return <FormArray>this.createProject.get('projectKeypersons') as FormArray;
  }
  public keyErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.projectKeypersonsError['controls'][i]['controls'][controlName].hasError(errorName)
  }
  //get addkeyPerson Form Array
  get keyPerson(): any {
    return this.createProject.get('projectKeypersons') as FormArray;
  }
  keyPersons(): any {
    return this.createProject.get('projectKeypersons') as FormArray;
  }
  //add addkeyPerson Form Array
  addkeyPerson() {
    this.keyPerson.push(this.keyPersonGroup());
  }
  //remove Key person Form Array
  removekeyPerson(index: number) {
    this.keyPersons().removeAt(index);
  }
  //Key person Form Array function
  private keyPersonGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      plantCode: null,
      projCode: null,
      designationCode: [null, [Validators.required]],
      bpCode: [null, [Validators.required]]
    });
  }
  //*************Key Person Section END****************//


  //*************Client Section****************//


  //get Store Form Array
  client(): any {
    return <FormArray>this.createProject.get('projectContactPersns') as FormArray;
  }
  //Add client contact person
  addContactPerson() {
    this.client().push(this.clientDataGroup());
  }
  //Remove client contact person
  removeContactPerson(index: number) {
    this.client().removeAt(index);
  }
  //client contact person Form Array function
  clientDataGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      plantCode: null,
      projCode: null,
      cpRole: [null, [Validators.required]],
      bpCode: [null, [Validators.required]],
      Client: [0]
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateclientComponent, {
      width: '100%',
      disableClose: false
    });
    // this.closeDialog()
    dialogRef.afterClosed().subscribe(result => {

    });
  }



  //*************CRUD Operation****************// 

  addProject() {
    this.isActiveTab = 1;
    //check if form valid
    if (this.createProject.invalid) {
      this.submitted = true;
      this.createProject.markAllAsTouched()
      return;
    }

    //Patch By Default values of Created & Updated BY users from storage & Update Project Status to 1
    this.createProject.patchValue({
      projectStatus: 1,
      createdBy: environment.createBy,
      updatedBy: environment.updateBy
    })

    // Patch CompanyCode PlantCode ProjectCode to KeyPersons Array
    this.keyPerson.value.forEach((element: any, index: any) => {
      (<FormGroup>this.keyPerson.at(index)).patchValue({
        cmpCode: this.createProject.value.cmpCode,
        plantCode: this.createProject.value.plantCode,
        projCode: this.createProject.value.projCode,
      });
    });

    // Convert ProjectCost to Numerical
    this.createProject.value.projectCost = Number(this.createProject.value.projectCost)

    // Patch CompanyCode PlantCode ProjectCode to Block Array
    this.blocks().value.forEach((element: any, index: any) => {
      (<FormGroup>this.blocks().at(index)).patchValue({
        cmpCode: this.createProject.value.cmpCode,
        plantCode: this.createProject.value.plantCode,
        projCode: this.createProject.value.projCode,
      })

      // Patch CompanyCode PlantCode ProjectCode to Building Array
      this.buildings(index).value.forEach((element: any, buildingIndex: any) => {
        (<FormGroup>this.buildings(index).at(buildingIndex)).patchValue({
          cmpCode: this.createProject.value.cmpCode,
          plantCode: this.createProject.value.plantCode,
          projCode: this.createProject.value.projCode,
        })
        //Check if any floors if length 0 set floors to null
        if (this.Floors(index, buildingIndex).value.length > 0) {
          // this.Floors(index, buildingIndex).value = null;
        }
        else {
          // Patch CompanyCode PlantCode ProjectCode to Floor Array
          this.Floors(index, buildingIndex).value.forEach((element: any, floorIndex: any) => {
            (<FormGroup>this.Floors(index, buildingIndex).at(floorIndex)).patchValue({
              cmpCode: this.createProject.value.cmpCode,
              plantCode: this.createProject.value.plantCode,
              projCode: this.createProject.value.projCode,
            })
          })
        }
      });
    });

    // Patch CompanyCode PlantCode ProjectCode to Stores Array
    this.stores().value.forEach((element: any, storeindex: any) => {
      (<FormGroup>this.stores().at(storeindex)).patchValue({
        cmpCode: this.createProject.value.cmpCode,
        plantCode: this.createProject.value.plantCode,
        projCode: this.createProject.value.projCode,
      })
    })

    if (this.subStores(0).value.length > 0) {
      this.subStores(0).value.forEach((element: any, subStoreIndex: any) => {
        // Patch CompanyCode PlantCode ProjectCode to SubStore Array
        (<FormGroup>this.subStores(0).at(subStoreIndex)).patchValue({
          cmpCode: this.createProject.value.cmpCode,
          plantCode: this.createProject.value.plantCode,
          projCode: this.createProject.value.projCode,
        })
      })
    }


    this.client().value.forEach((element: any, index: any) => {
      // Patch CompanyCode PlantCode ProjectCode to Client Array
      (<FormGroup>this.client().at(index)).patchValue({
        cmpCode: this.createProject.value.cmpCode,
        plantCode: this.createProject.value.plantCode,
        projCode: this.createProject.value.projCode
      })

    })

    // create json and remove all the empty arrays and add client details in the contact persons array
    let addProjectJson = this.createProject.value;

    addProjectJson.projectCost = Number(addProjectJson.projectCost)

    //push an element to client where key "Client" 1 will be Client 2 will be ClientContactPerson Commercial 3 will be ClientContactPerson Technical
    addProjectJson.projectContactPersns.push(
      {
        "cmpCode": this.createProject.value.cmpCode,
        "plantCode": this.createProject.value.plantCode,
        "projCode": this.createProject.value.projCode,
        "cpRole": "Client",
        "bpCode": this.createProject.value.clientCode,
        "Client": 1
      })

    addProjectJson.projectContactPersns.forEach(async (element: any) => {
      console.log(element)
      if (element.cpRole == 'Client') {
        element.Client = 1;
      }

      if (element.cpRole == 'Commercial') {
        element.Client = 2;
      }
      if (element.cpRole == 'Technical') {
        element.Client = 3;
      }

    });


    addProjectJson.store.forEach((element: any) => {
      if (element.subStore.length == 0) {
        element.subStore = []
      }
    });

    addProjectJson.store = addProjectJson.store[0];
    addProjectJson.blocks.forEach((element: any) => {
      element.buildings.forEach((element: any) => {
        if (element.Floors.length == 0) {
          element.Floors = []
        }
      });
    });

    let Api = this.apiCallService.addProject;
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>", addProjectJson)

    var head = {}
    if (this.isCreate == 1) { Api = this.apiCallService.updateProject, head = { ID: addProjectJson.projCode, cmpCode: addProjectJson.cmpCode, plantCode: addProjectJson.plantCode } }
    var data = { data: addProjectJson, head: head }

    Api(data).subscribe({

      next: async (result: any) => {
        if (result) {

          this.commonService.showSuccess("Record Inserted Succefully!");
          this.Accordion?.closeAll();


          this.tabIsActive(0, 0);
          this.projectList = await this.commomFunction.getproject(addProjectJson.cmpCode, addProjectJson.plantCode);
          if (this.projectList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.projectList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
          this.createProject.reset();
        }
      }, error: (error: any) => {

        this.commonService.showError(error);
      }
    })
  }

  async editProject(row: any, isView: any) {
    console.log(new Date(), row.projectStartDate)
    this.spinnerService.show();
    this.updateRow = row;

    if (isView == 'view') { this.createProject.disable(); this.isCreate = 2; } else { this.createProject.enable(); }

    this.tabIsActive(1, 1);
    this.CompanyList = await this.commomFunction.getCompany();
    this.CountryList = await this.commomFunction.getCountry();
    this.bpCodeList = await this.commomFunction.getEmployee(row.cmpCode);
    this.designationList = await this.commomFunction.getdesignationList(row.cmpCode);
    this.startMinDate = row.projectStartDate

    this.endMinDate = new Date();
    this.createProject.patchValue({
      projCode: row.projCode,
      cmpCode: row.cmpCode,
      plantCode: row.plantCode,
      activityCode: row.activityCode,
      projectTypeCode: row.projectTypeCode,
      projName: row.projName,
      projDesc: row.projDesc,
      addressLine1: row.addressLine1,
      addressLine2: row.addressLine2,
      street: row.street,
      city: row.city,
      postalCode: row.postalCode,
      stateCode: row.stateCode,
      countryCode: row.countryCode,
      projectCost: row.projectCost,
      projectStartDate: row.projectStartDate,
      projectEndDate: row.projectEndDate,
      projectStatus: row.projectStatus,
      clientCode: row.clientCode,
      consultantCode: row.consultantCode,
      createdBy: row.createdBy,
      createdOn: row.createdOn,
      updatedBy: row.updatedBy,
      updatedOn: row.updatedOn
    })

    await this.onChangeClient(this.createProject.value.clientCode);
    let clientcontactpersonlist = row.projectContactPersns.filter((obj: any) => obj.client != 1);
    clientcontactpersonlist.forEach((element: any, index: any) => {

      var cprole = null;
      if (element.client == 2) {
        cprole = 'Commertial'
      }
      if (element.client == 3) {
        cprole = 'Technical'
      }

      if (this.client().length !== 0) {
        this.client().removeAt(0)
      }
      for (let i = 0; i < clientcontactpersonlist.length; i++) {
        this.addContactPerson();

        this.client().at(i).patchValue({
          cmpCode: this.createProject.value.cmpCode,
          plantCode: this.createProject.value.plantCode,
          projCode: this.createProject.value.projCode,
          bpCode: clientcontactpersonlist[i].bpCode,
          cpRole: cprole
        });
        if (isView == 'view') { this.createProject.disable(); this.isCreate = 2; } else { this.createProject.enable(); }
        // if (isView == 'view') { this.client().at(i).disable() } else { this.client().at(i).enable() }
      }
    })

    row.projectKeypersons.forEach((element: any, index: any) => {

      if (this.keyPerson.length !== 0) {
        this.keyPerson.removeAt(0)
      }

      for (let i = 0; i < row.projectKeypersons.length; i++) {
        this.addkeyPerson();
        (<FormGroup>this.keyPerson.at(index)).patchValue({
          bpCode: element.bpCode,
          designationCode: element.designationCode
        });

        if (isView == 'view') { <FormGroup>this.keyPerson.at(index).disable() } else { <FormGroup>this.keyPerson.at(index).enable() }
      }

    })

    row.blocks.forEach((element: any, index: any) => {
      if (this.blocks().length !== 0) {
        this.blocks().removeAt(0)
      }
      this.addBlock();
      (<FormGroup>this.blocks().at(index)).patchValue({
        blockName: element.blockName,
        Description: element.description,
        blockCode: element.blockCode
      });
      if (isView == 'view') { this.createProject.disable(); this.isCreate = 2; } else { this.createProject.enable(); }
      // if (isView == 'view') { <FormGroup>this.blocks().at(index).disable() } else { <FormGroup>this.blocks().at(index).enable() }
      element.buildings.forEach((element: any, buildingindex: any) => {
        if (this.buildings(index).length !== 0) {
          this.buildings(index).removeAt(0)
        }
        this.addBuilding(index);
        (<FormGroup>this.buildings(index).at(buildingindex)).patchValue({
          buildingName: element.buildingName,
          Description: element.description,
          blockCode: element.blockCode,
          buildCode: element.buildCode
        });
        if (isView == 'view') { this.createProject.disable(); this.isCreate = 2; } else { this.createProject.enable(); }


        if (element.floors.length != 0) {
          element.buildings.floors.forEach((element: any, floorsindex: any) => {
            if (this.Floors(index, buildingindex).length !== 0) {
              this.Floors(index, buildingindex).removeAt(0)
            }
            this.addFloor(index, buildingindex);
            (<FormGroup>this.Floors(index, buildingindex).at(floorsindex)).patchValue({
              floorName: element.floorName,
              floorDesc: element.floorDesc,
              blockCode: element.blockCode,
              buildCode: element.buildCode,
              floorCode: element.floorCode

            });
            if (isView == 'view') { this.createProject.disable(); this.isCreate = 2; } else { this.createProject.enable(); }
          })
        }
      });
    });


    (<FormGroup>this.stores().at(0)).patchValue({
      storeCode: row.store.storeCode,
      storeName: row.store.storeName,
      Description: row.store.description
    });

    if (row.store.subStore.length !== 0) {
      this.subStores(0).removeAt(0);
      this.addSubStore(0);
      row.store.subStore.forEach((element: any, index: any) => {
        (<FormGroup>this.subStores(0).at(index)).patchValue({
          storeCode: element.storeCode,
          subStoreCode: element.subStoreCode,
          storeName: element.storeName,
          Description: element.description
        });
      });
    }

    if (isView == 'view') { this.createProject.disable(); this.isCreate = 2; } else { this.createProject.enable(); }

    this.spinnerService.hide();
  }

  deleteProject(row: any) {

    this.deleteRow = row.projCode;
    if (this.deleteRow) {
      var head = { cmpCode: row.cmpCode, plantCode: row.plantCode, ID: this.deleteRow }
      var data = { head: head }
      this.apiCallService.deleteProject(data).subscribe({
        next: async (result: any) => {
          if (result) {
            this.commonService.showSuccess(result.data);
            const index = this.projectList.indexOf(this.deleteRow);
            if (index > -1) { // only splice array when item is found
              this.projectList.splice(index, 1); // 2nd parameter means remove one item only   
            }

            if (this.projectList.length > 0) {
              this.dataSource = new MatTableDataSource(this.projectList);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            }
          }
        }, error: (error: any) => {
          this.commonService.showError(error);
        }
      }
      )

    }
  }

  async onChangeCmp(data: any) {
    if (data) {
      this.PlantList = await this.commomFunction.getPlant(data);
      this.CountryList = await this.commomFunction.getCountry();
      this.consultantList = await this.commomFunction.getBusinessPartners(data, 'CON');
      this.ExecutaionList = await this.commomFunction.getExecutaionList(data);
      this.projectTypeList = await this.commomFunction.getprojectTypeList(data);
      this.clientList = await this.commomFunction.getBusinessPartners(data, 'CL');

      this.bpCodeList = await this.commomFunction.getEmployee(data);
      this.designationList = await this.commomFunction.getdesignationList(data);
      if (!this.clientList) {
        this.createProject.controls['clientCode'].setValue(null);
        this.client().value.forEach((element: any, index: any) => {
          (<FormGroup>this.client().at(index)).patchValue({
            bpRole: null,
            cpRole: null
          })
        })
      }
    } else {
      this.PlantList = [];
      this.CountryList = [];
      this.consultantList = [];
      this.ExecutaionList = [];
      this.projectTypeList = [];
      this.clientList = [];
      this.bpCodeList = [];
      this.designationList = [];
      this.createProject.patchValue({ plantCode: null, activityCode: null, projectTypeCode: null, consultantCode: null })
      this.createProject.controls['clientCode'].setValue(null);
      this.client().value.forEach((element: any, index: any) => {
        (<FormGroup>this.client().at(index)).patchValue({
          bpRole: null,
          cpRole: null
        })
      })
    }
  }
  async onChangeCountry(data: any) {
    if (data) {
      this.StateList = await this.commomFunction.getState(data);
    } else {
      this.StateList = [];
      this.createProject.patchValue({ stateCode: null })
    }
  }
  async onChangeClient(data: any) {
    if (data) {
      this.clientList = await this.commomFunction.getBusinessPartners(this.createProject.value.cmpCode, 'CL');
      if (!this.clientList) {

        this.createProject.controls['clientCode'].setValue(null);
        this.client().value.forEach((element: any, index: any) => {
          (<FormGroup>this.client().at(index)).patchValue({
            bpRole: null,
            cpRole: null
          })
        })
      }
      this.clientContactPersonList = await this.clientContactPerson(data)
    } else {
      this.clientList = [];
      this.clientContactPersonList = [];
    }
  }

  async onChangeCompany(data: any) {
    this.spinnerService.show();
    this.projectList = await this.commomFunction.getproject(data, null);


    if (this.projectList.length > 0) {
      // Assign the data to the data source for the table to render
      this.dataSource = new MatTableDataSource(this.projectList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinnerService.hide();
    } else {
      this.projectList = [];
      this.spinnerService.hide();
    }
  }

}

export interface PrjData {
  projCode: string;
  cmpCode: string;
  plantCode: string;
  activityCode: string;
  projectTypeCode: string;
  projName: string;
  projDesc: string;
  addressLine1: string;
  addressLine2: string;
  street: string;
  city: string;
  postalCode: string;
  stateCode: string;
  countryCode: string;
  projectCost: string;
  projectStartDate: string,
  projectEndDate: string;
  projectStatus: string;
  clientCode: string;
  consultantCode: string;
  createBy: string;
  createOn: string;
  updateBy: string;
  updateOn: string;

}