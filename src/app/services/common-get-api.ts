import { Component, OnInit } from '@angular/core';
import { ApiCallService } from './api-call.service';
import { CommonService } from './common.service';

export class commonGetApi implements OnInit {

  constructor(private apiCallService:ApiCallService,private commonService:CommonService) { }

  ngOnInit(): void {
  }

  getCountry() {
    return new Promise((resolve, reject) => {
      var data = {}
      this.apiCallService.getCountry(data).subscribe({
        next: (result: any) => {
          if (result) {
            resolve(result)
          }
        }, error: (error: any) => {
          this.commonService.showError(error.error);
          let res: any = [];
          resolve(res);
        }
      })
    })
  }

  async getState(CountryCode: any) {
    return new Promise((resolve, reject) => {
      if (CountryCode) {
        var param = { CountryCode: CountryCode }
        var data = { param: param }
        this.apiCallService.getState(data).subscribe({
          next: async (result: any) => {
            if (result) {
              resolve(result)
            }
          }, error: (error: any) => {
            this.commonService.showError(error.error);
            let res: any = [];
            resolve(res);
          }
        })
      } else {
        let result: any = [];
        resolve(result)
      }
    })
  }

}