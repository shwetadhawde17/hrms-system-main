import { Injectable } from '@angular/core';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {

  constructor(private commonService: CommonService) { }

  get WindowRef() { return window }
  // All Masters
  getAccountGroup = (data: any) => this.commonService.doGet('AccountGroup' + '/' + data.cmpCode, data);

  getCountry = (data: any) => this.commonService.doGet('Country', data);

  getState = (data: any) => this.commonService.doGet('State', data);
  getStateId = (data: any) => this.commonService.doGet('State' + '/' + data.countryCode, data);

  getCompany = (data: any) => this.commonService.doGet('CompanyDetails', data);
  addCompany = (data: any) => this.commonService.doPost('CompanyDetails', data);
  updateCompany = (data: any) => this.commonService.doPut('CompanyDetails' + '/' + data.data.oldCmpCode, data);
  deleteCompany = (data: any) => this.commonService.doDelete('CompanyDetails' + '/' + data.cmpCode, data);

  getPlant = (data: any) => this.commonService.doGet('Plant', data);
  getPlantId = (data: any) => this.commonService.doGet('Plant' + '/' + data.cmpCode, data);
  addPlant = (data: any) => this.commonService.doPost('Plant', data);
  updatePlant = (data: any) => this.commonService.doPut('Plant' + '/' + data.cmpCode + '/' + data.plantCode, data);
  deletePlant = (data: any) => this.commonService.doDelete('Plant' + '/' + data.cmpCode + '/' + data.plantCode, data);

  getBusinessPartnerByCompPlant = (data: any) => this.commonService.doGet('BusinessPartner' + '/' + 'ByCompPlant', data);
  getBusinessPartner = (data: any) => this.commonService.doGet('BusinessPartner', data);
  addBusinessPartner = (data: any) => this.commonService.doPost('BusinessPartner', data);
  updateBusinessPartner = (data: any) => this.commonService.doPut('BusinessPartner' + '/' + data.head.cmpCode + '/' + data.head.bpCode, data);
  deleteBusinessPartner = (data: any) => this.commonService.doDelete('BusinessPartner' + '/' + data.head.cmpCode + '/' + data.head.bpCode, data);

  getEmployeeId = (data: any) => this.commonService.doGet('Employee' + '/' + data.param.cmpCode, data);
  addEmployee = (data: any) => this.commonService.doPost('Employee', data);
  updateEmployee = (data: any) => this.commonService.doPut('Employee' + '/' + data.head.cmpCode + '/' + data.head.bpCode, data);
  deleteEmployee = (data: any) => this.commonService.doDelete('Employee' + '/' + data.head.cmpCode + '/' + data.head.bpCode, data);

  getProject = (data: any) => this.commonService.doGet('Project', data);
  getProjectId = (data: any) => this.commonService.doGet('Project' + '/' + data.head.cmpCode, data);
  addProject = (data: any) => this.commonService.doPost('Project', data);
  updateProject = (data: any) => this.commonService.doPut('Project' + '/' + data.head.cmpCode + '/' + data.head.plantCode + '/' + data.head.ID, data);
  deleteProject = (data: any) => this.commonService.doDelete('Project' + '/' + data.head.cmpCode + '/' + data.head.plantCode + '/' + data.head.ID, data);

  // getBOQUpload = (data: any) => this.commonService.doGet('BOQ', data);
  getISBOQPresent= (data: any) => this.commonService.doGet('BOQ' + '/' + data.head.cmpCode + "/" + data.head.plantCode + "/" + data.head.projCode, data);
  getBOQUploadId = (data: any) => this.commonService.doGet('BOQ' + '/' + data.head.cmpCode , data);
  addBOQUpload = (data: any) => this.commonService.doPost('BOQ', data);
  updateBOQUpload = (data: any) => this.commonService.doPut('BOQ' + '/' + data.head.cmpCode + '/' + data.head.plantCode + '/' + data.head.projCode, data);
  deleteBOQUpload = (data: any) => this.commonService.doDelete('BOQ' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getProjectType = (data: any) => this.commonService.doGet('ProjectType', data);
  getProjectTypeId = (data: any) => this.commonService.doGet('ProjectType' + '/' + data.head.cmpCode, data);
  addProjectType = (data: any) => this.commonService.doPost('ProjectType', data);
  updateProjectType = (data: any) => this.commonService.doPut('ProjectType' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteProjectType = (data: any) => this.commonService.doDelete('ProjectType' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getProjectDesignation = (data: any) => this.commonService.doGet('ProjectDesignation', data);
  getProjectDesignationById = (data: any) => this.commonService.doGet('ProjectDesignation' + '/' + data.head.cmpCode, data);
  addProjectDesignation = (data: any) => this.commonService.doPost('ProjectDesignation', data);
  updateProjectDesignation = (data: any) => this.commonService.doPut('ProjectDesignation' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteProjectDesignation = (data: any) => this.commonService.doDelete('ProjectDesignation' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getExecutionType = (data: any) => this.commonService.doGet('ActivityType', data);
  getExecutionTypeById = (data: any) => this.commonService.doGet('ActivityType' + '/' + data.head.cmpCode, data);
  addExecutionType = (data: any) => this.commonService.doPost('ActivityType', data);
  updateExecutionType = (data: any) => this.commonService.doPut('ActivityType' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteExecutionType = (data: any) => this.commonService.doDelete('ActivityType' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getActivityHead = (data: any) => this.commonService.doGet('activityhead' + '/' + data.head.cmpCode, data);

  getAcountGroup = (data: any) => this.commonService.doGet('AccountGroup', data);
  getAccountGroupById = (data: any) => this.commonService.doGet('AccountGroup' + '/' + data.cmpCode, data);
  addAccountGroup = (data: any) => this.commonService.doPost('AccountGroup', data);
  updateAccountGroup = (data: any) => this.commonService.doPut('AccountGroup' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteAccountGroup = (data: any) => this.commonService.doDelete('AccountGroup' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getunitMaster = (data: any) => this.commonService.doGet('unitMaster', data);
  getunitMasterById = (data: any) => this.commonService.doGet('unitMaster' + '/' + data.head.cmpCode, data);
  addunitMaster = (data: any) => this.commonService.doPost('unitMaster', data);
  updateunitMaster = (data: any) => this.commonService.doPut('unitMaster' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteunitMaster = (data: any) => this.commonService.doDelete('unitMaster' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getDesignation = (data: any) => this.commonService.doGet('Designation', data);
  getDesignationById = (data: any) => this.commonService.doGet('Designation' + '/' + data.head.cmpCode, data);
  addDesignation = (data: any) => this.commonService.doPost('Designation', data);
  updateDesignation = (data: any) => this.commonService.doPut('Designation' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteDesignation = (data: any) => this.commonService.doDelete('Designation' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getDepartment = (data: any) => this.commonService.doGet('Department', data);
  getDepartmentById = (data: any) => this.commonService.doGet('Department' + '/' + data.head.cmpCode, data);
  addDepartment = (data: any) => this.commonService.doPost('Department', data);
  updateDepartment = (data: any) => this.commonService.doPut('Department' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteDepartment = (data: any) => this.commonService.doDelete('Department' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getLabourMaster = (data: any) => this.commonService.doGet('LabourMaster', data);
  getLabourMasterById = (data: any) => this.commonService.doGet('LabourMaster', data);
  addLabourMaster = (data: any) => this.commonService.doPost('LabourMaster', data);
  // updateLabourMaster = (data: any) => this.commonService.doPost('LabourMaster' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteSubLabour = (data: any) => this.commonService.doDelete('LabourMaster' + '/' +'LabourDetails', data);

  deleteLabourMaster = (data: any) => this.commonService.doDelete('LabourMaster' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getMaterialMaster = (data: any) => this.commonService.doGet('MaterialMaster', data);
  getMaterialMasterById = (data: any) => this.commonService.doGet('MaterialMaster' + '/' + data.head.cmpCode, data);
  getMateriaSubmateria = (data: any) => this.commonService.doGet('MaterialMaster' + '/' + data.head.materialID + '/' + data.head.materialSubID + '/' + data.head.materialDetailID, data);//
  addMaterialMaster = (data: any) => this.commonService.doPost('MaterialMaster', data);
  updateMaterialMaster = (data: any) => this.commonService.doPut('MaterialMaster' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteDetailsMaterialSub = (data: any) => this.commonService.doDelete('MaterialMaster' + '/' +'MaterialSub', data);
  deleteActualMaterial = (data: any) => this.commonService.doDelete('MaterialMaster' + '/' +'materialDetails', data);
  // Common For material,Labour, Apply Supply Masters
  deleteMaster = (data: any) => this.commonService.doDelete(data.URI, data);

  getApplySupply = (data: any) => this.commonService.doGet('ApplySupply', data);
  getApplySupplyById = (data: any) => this.commonService.doGet('ApplySupply' + '/' + data.head.cmpCode, data);
  addApplySupply = (data: any) => this.commonService.doPost('ApplySupply', data);
  updateApplySupply = (data: any) => this.commonService.doPut('ApplySupply' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteApplySupply = (data: any) => this.commonService.doDelete('ApplySupply' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteSubApplySupply= (data: any) => this.commonService.doDelete('ApplySupply' + '/' +'ApplySupplyDetails', data);

  addMaterialRates = (data: any) => this.commonService.doPost('MaterialRates', data);
  updateMaterialRates = (data: any) => this.commonService.doPut('MaterialRates' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteMaterialRates = (data: any) => this.commonService.doDelete('MaterialRates' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  addLabourRates = (data: any) => this.commonService.doPost('LabourRates', data);
  updateLabourRates = (data: any) => this.commonService.doPut('LabourRates' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteLabourRates = (data: any) => this.commonService.doDelete('LabourRates' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  addApplySupplyRates = (data: any) => this.commonService.doPost('ApplySupplyRates', data);
  updateApplySupplyRates = (data: any) => this.commonService.doPut('ApplySupplyRates' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteApplySupplyRates = (data: any) => this.commonService.doDelete('ApplySupplyRates' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  // Common Get Method For Material, Labour and Apply Supply Rate
  getRateMasters = (data: any) => this.commonService.doGet(data.URI + "/" + data.head.cmpCode + "/" + data.head.plantCode + "/" + data.head.projCode, data);

  getRateAnalysis = (data: any) => this.commonService.doGet('RateAnalysis' + "/" + data.head.cmpCode + "/" + data.head.plantCode, data);
  addRateAnalysis = (data: any) => this.commonService.doPost('RateAnalysis', data);
  updateRateAnalysis = (data: any) => this.commonService.doPut('RateAnalysis' + "/" + data.head.cmpCode + "/" + data.head.plantCode+ "/" + data.head.projCode, data);
  sendToApproval = (data: any) => this.commonService.doPut('RateAnalysis'  + '/' + 'sendToApproval'+ "/" + data.head.cmpCode + "/" + data.head.plantCode+ "/" + data.head.projCode, data);
  deleteRateAnalysis = (data: any) => this.commonService.doDelete('RateAnalysis' + '/' + 'DelPSE', data);
  deleteRateAnalysisHesdSeq = (data: any) => this.commonService.doDelete('RateAnalysis' + "/" + data.head.cmpCode + "/" + data.head.plantCode + "/" + data.head.projCode + "/" + data.head.actGroupId + "/" + data.head.actSeqNo, data);
  
  getIDCAnalysis = (data: any) => this.commonService.doGet('IDCAnalysis', data);
  getIDCAnalysisById = (data: any) => this.commonService.doGet('IDCAnalysis'+ "/" + data.head.cmpCode + "/" + data.head.plantCode, data);
  addIDCAnalysis = (data: any) => this.commonService.doPost('IDCAnalysis', data);
  updateIDCAnalysis = (data: any) => this.commonService.doPut('IDCAnalysis' + '/' + data.head.cmpCode + '/' + data.head.plantCode+ '/' + data.head.projCode, data);
  sendToApprovalIDC = (data: any) => this.commonService.doPut('IDCAnalysis'  + '/' + 'sendToApproval'+ "/" + data.head.cmpCode + "/" + data.head.plantCode+ "/" + data.head.projCode, data);

  getMiscellaneousRates = (data: any) => this.commonService.doGet('MiscellaneousRates' + "/"+ "/" + data.head.cmpCode + "/" + data.head.plantCode + "/" + data.head.projCode , data);
  addMiscellaneousRates = (data: any) => this.commonService.doPost('MiscellaneousRates', data);
  deleteMiscellaneousRates = (data: any) => this.commonService.doDelete('MiscellaneousRates', data);
  deleteMiscellaneousRatesHesdSeq = (data: any) => this.commonService.doDelete('MiscellaneousRates' + "/" + data.head.cmpCode + "/" + data.head.plantCode + "/" + data.head.projCode , data);
 
  getIDCActivity = (data: any) => this.commonService.doGet('IDCActivity', data);
  getIDCActivityById = (data: any) => this.commonService.doGet('IDCActivity' + '/' + data.head.cmpCode, data);
  addIDCActivity = (data: any) => this.commonService.doPost('IDCActivity', data);
  updateIDCActivity = (data: any) => this.commonService.doPut('IDCActivity' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getVmmMaster = (data: any) => this.commonService.doGet('VmmMaster', data);
  getVmmMasterID = (data: any) => this.commonService.doGet('VmmMaster' + '/' + data.head.cmpCode, data);
  // getLabourMasterById = (data: any) => this.commonService.doGet('VmmMaster', data);
  addVmmMaster = (data: any) => this.commonService.doPost('VmmMaster', data);
  // updateLabourMaster = (data: any) => this.commonService.doPost('VmmMaster' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteVmmMaster = (data: any) => this.commonService.doDelete('VmmMaster' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getmmmMaster = (data: any) => this.commonService.doGet('MmmMaster', data);
  getmmmMasterID = (data: any) => this.commonService.doGet('MmmMaster'+ '/' + data.head.cmpCode, data);
  addmmmMaster = (data: any) => this.commonService.doPost('MmmMaster', data);
  deletemmmMaster = (data: any) => this.commonService.doDelete('MmmMaster' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  getMiscellaneous = (data: any) => this.commonService.doGet('Miscellaneous', data);
  addMiscellaneous = (data: any) => this.commonService.doPost('Miscellaneous', data);
  deleteMiscellaneous = (data: any) => this.commonService.doDelete('Miscellaneous' + '/' + data.head.cmpCode + '/' + data.head.ID, data);

  //remove this api
  getIDCMaster = (data: any) => this.commonService.doGet('IDCActivity', data);
  getIDCMasterById = (data: any) => this.commonService.doGet('IDCActivity' + '/' + data.head.cmpCode, data);
  // getMateriaSubmateria = (data: any) => this.commonService.doGet('IDCActivity' + '/' + data.head.materialID + '/' + data.head.materialSubID + '/' + data.head.materialDetailID, data);//
  addIDCMaster = (data: any) => this.commonService.doPost('IDCActivity', data);
  updateIDCMasterById = (data: any) => this.commonService.doPut('IDCActivity' + '/' + data.head.cmpCode + '/' + data.head.ID, data);
  deleteDetailsIDCSub = (data: any) => this.commonService.doDelete('IDCActivity' + '/' +'IDCActivitySub', data);
  deleteActualIDC = (data: any) => this.commonService.doDelete('IDCActivity' + '/' +'IDCActivityDetails', data);

  getSOMLA = (data: any) => this.commonService.doGet('SOMLA', data);

  getVendor = (data: any) => this.commonService.doGet('Vendor' + '/' + data.param.cmpCode, data);
  addVendor = (data: any) => this.commonService.doPost('Vendor', data);
  updateVendor = (data: any) => this.commonService.doPut('Vendor' + '/' + data.head.cmpCode + '/' + data.head.bpCode, data);
  deleteVendor = (data: any) => this.commonService.doDelete('Vendor' + '/' + data.head.cmpCode + '/' + data.head.bpCode, data);


  getServiceRequestNote = (data: any) => this.commonService.doGet('ServiceRequestNote', data);
  addServiceRequestNote = (data: any) => this.commonService.doPost('ServiceRequestNote', data);
  updateServiceRequestNote = (data: any) => this.commonService.doPut('ServiceRequestNote' + '/' + data.head.srnid, data);
  updateForApproval = (data: any) => this.commonService.doPut('ServiceRequestNote' + '/'  + 'ForApproval' + '/'  + data.head.srnid, data);
  deleteServiceRequestNote = (data:any) => this.commonService.doDelete('ServiceRequestNote' + '/' + data.head.cmpCode + '/' + data.head.srnid,data)


}


// IDCActivity, IDCActivitySub, IDCActivityDetails three delete