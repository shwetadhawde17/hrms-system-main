import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatAccordion } from '@angular/material/expansion';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { JsonToXlsxService } from 'src/app/services/json-to-xlsx.service';
import { json } from 'stream/consumers';
import { ComponentsComponent } from '../components.component';
import { error } from 'console';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-prestartestimation',
  templateUrl: './prestartestimation.component.html',
  styleUrls: ['./prestartestimation.component.scss']
})
export class PrestartestimationComponent implements OnInit {
  prestartestimation: FormGroup;
  prestartestimationView: FormGroup;
  updateRow: any;
  isActiveTab: number;
  BOQList: any;
  CompanyList: any;
  PlantList: any;
  ProjectList: any;
  subHead: any;
  mastersList: any;
  UnitMasterList: any;
  mastersratesList: any;
  isCreate: any;
  RateAnalysisList: any = [];
  jsonsublist: any = [];
  MaterialList: any = [];
  LabourList: any = [];
  ApplySupplyList: any = [];
  GroupType = ['Material', 'Labour', 'Apply Supply']
  dataSource!: MatTableDataSource<RateAnalysisData>;
  displayedColumns = ['isApproved', 'cmpName', 'plantName', 'projName'];
  showSub: boolean = false;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('accordion', { static: true }) Accordion?: MatAccordion;
  @ViewChild(FormGroupDirective) myForm: any;

  constructor(private JsonToXlsxService: JsonToXlsxService, private fb: FormBuilder, private commomFunction: ComponentsComponent, private commonService: CommonService, private apiCallService: ApiCallService,private spinnerService: NgxSpinnerService) {
    this.isActiveTab = 0;
    this.prestartestimation = this.fb.group({
      cmpCode: [null, Validators.required],
      plantCode: [null, Validators.required],
      projCode: [null, Validators.required],
      header: new FormArray([this.createHeadArray()])
    })
    this.prestartestimationView = this.fb.group({
      cmpCode: [null, Validators.required],
      plantCode: [null, Validators.required],
      projCode: [null, Validators.required],
      DocumentFileUpload: [null, Validators.required]
    })
  }

  async ngOnInit() {

    this.CompanyList = await this.commomFunction.getCompany();
  }
  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {

    if (this.headerArrayFun().length > 0) {
      while (this.headerArrayFun().length !== 0) {
        <FormGroup>this.headerArrayFun().removeAt(0)
      }
    }
    if (value != change) {
      this.updateRow = null, value = 0
      this.myForm.resetForm();
    } else if (value == change) {

    }
    this.isActiveTab = value;
    this.isCreate = value;

    this.prestartestimation.reset();
    this.Accordion?.closeAll();
    this.myForm.resetForm();
    this.prestartestimation.enable();
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.headerArray['controls'][i]['controls'][controlName].hasError(errorName)
  }

  // Get headerArray Form Array
  get headerArray(): any {
    return <FormArray>this.prestartestimation.get('header') as FormArray;
  }
  headerArrayFun(): any {
    return <FormArray>this.prestartestimation.get('header') as FormArray;
  }
  // create form head array
  private createHeadArray(): FormGroup {
    return this.fb.group({
      actGroupId: [null, Validators.required],
      actSeqNo: [null, Validators.required],
      quntity: [null, Validators.required],
      newQuntity: [null],
      boqUnitCode: [null, Validators.required],
      rate: [null, Validators.required],
      materialAmt: [null],
      labourAmt: [null],
      applysupplyAmt: [null],
      totalAmt: [null],
      details: new FormArray([]),
    })
  }
  // add form head array
  addHeadArray() {
    this.headerArrayFun().push(this.createHeadArray());
  }

  // remove form details array
  removeheaderArray(index: number) {
    if (this.isCreate == 1) {
      var row = {
        cmpCode: this.prestartestimation.value.cmpCode,
        plantCode: this.prestartestimation.value.plantCode,
        projCode: this.prestartestimation.value.projCode,
        actGroupId: this.headerArrayFun().at(index).value.actGroupId,
        actSeqNo: this.headerArrayFun().at(index).value.actSeqNo,
      }
      if (this.headerArrayFun().at(index).value.actGroupId) {
        this.deleteRateAnalysisHead(row);
      } else {
        let control = <FormArray>this.prestartestimation.controls.header;
        control.removeAt(index)
      }
    } else {
      let control = <FormArray>this.prestartestimation.controls.header;
      control.removeAt(index)
    }
  }

  // Get DetailsArray Form Array
  // get detailsArrays(index:any): any {
  //    return <FormArray>this.headerArrayFun().at(index).get('details') as FormArray;
  // }
  DetailsArray(index: number): any {
    return <FormArray>this.headerArrayFun().at(index).get('details') as FormArray;
  }
  // create form details array
  private createDetailsArray(): FormGroup {
    return this.fb.group({
      groupType: [null, [Validators.required]],
      materialDetailID: [null],
      labourDetailsID: [null],
      applySupplyDetailsID: [null],
      unitCode: [null, [Validators.required]],
      constant: [null, [Validators.required]],
      rate: [null, [Validators.required]],
      amount: [null, [Validators.required]] //  rate * constant
    })
  }

  // add form details array
  addDetailsArray(index: number) {
    this.DetailsArray(index).push(this.createDetailsArray());
  }
  // remove form details array
  removeDetailsArray(index: number, index2: any) {
    if (this.isCreate == 1) {
      var row = {
        cmpCode: this.prestartestimation.value.cmpCode,
        plantCode: this.prestartestimation.value.plantCode,
        projCode: this.prestartestimation.value.projCode,
        Type: this.DetailsArray(index).at(index2).value.groupType,
        ID: this.DetailsArray(index).at(index2).value.materialDetailID,
      }
      // let control = <FormArray>this.prestartestimation.controls.header;
      // control.removeAt(index)
      if (this.DetailsArray(index).at(index2).value.materialDetailID) {
        this.deleteRateAnalysisHeadDetails(row);
      } else {
        this.DetailsArray(index).removeAt(index2);
      }
    } else {
      this.DetailsArray(index).removeAt(index2);
    }
  }

  async OnChangeCompany(cmpCode: any) {

    if (cmpCode) {
      this.PlantList = await this.commomFunction.getPlant(cmpCode);
      this.UnitMasterList = await this.commomFunction.getunitMaster(cmpCode);
    } else {
      this.prestartestimation.controls['plantCode'].setValue(null); this.prestartestimation.controls['projCode'].setValue(null);
      this.prestartestimationView.controls['plantCode'].setValue(null); this.prestartestimationView.controls['projCode'].setValue(null);
      this.ProjectList = []; this.PlantList = []; this.BOQList = []; this.UnitMasterList = []
    }
  }
  async OnChangePlant(cmpCode: any, plantCode: any) {
    if (cmpCode && plantCode) {
      this.ProjectList = await this.commomFunction.getproject(cmpCode, plantCode);
    } else {
      this.prestartestimationView.controls['projCode'].setValue(null);
      this.prestartestimation.controls['projCode'].setValue(null);
      this.RateAnalysisList = []; this.dataSource = new MatTableDataSource();
    }
  }
  async OnChangeProject(cmpCode: any, plantCode: any, projCode: any) {
    console.log(cmpCode, plantCode, projCode);
    this.spinnerService.show();
    if (cmpCode && plantCode && projCode) {
      if (this.headerArrayFun().length < 1) { this.addHeadArray(); }
      this.showSub = true;
      this.BOQList = await this.commomFunction.getBOQ(cmpCode, plantCode, projCode, false);
      this.BOQList = this.BOQList[0]   // Change is required here
      this.RateAnalysisList = await this.commomFunction.getRateAnalysis(cmpCode, plantCode, projCode, false);
      console.log(this.RateAnalysisList)
      if (this.RateAnalysisList.length > 0) {

        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.RateAnalysisList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide()
      } else {
        this.RateAnalysisList = []; this.dataSource = new MatTableDataSource();
        this.spinnerService.hide()

      }

    } else {
      if (this.headerArrayFun().length > 0) {
        while (this.headerArrayFun().length !== 1) {
          <FormGroup>this.headerArrayFun().removeAt(0)
        }
        this.spinnerService.hide()

      }
      this.RateAnalysisList = []; this.dataSource = new MatTableDataSource();
      this.BOQList = []; this.subHead = []
    }
  }
  async OnChangeBOQHead(head: any, index: any) {
    if (head) {
      var sub = this.BOQList.find((item: any) => { if (item.actGroupId == head) { return item } })
      this.subHead = await this.commonService.filterActSubSeq(sub.details);

      if (this.jsonsublist[index]) {
        if (this.jsonsublist[index]) {
          this.jsonsublist[index] = this.subHead
        }
        else {
          this.jsonsublist[index].push(this.subHead)
        }
      } else {
        this.jsonsublist.push(this.subHead)
      }
    } else {
      this.subHead = []
    }
  }
  async OnChangeBOQSubHead(subHead: any, index: any) {

    if (subHead) {
      if (this.jsonsublist[index]) {
        var details = await this.jsonsublist[index].find((item: any) => {
          if (item.head == this.headerArray['controls'][index].value.actGroupId || item.nActSeqNo == subHead) { return item }
        })

        if (details) {
          this.headerArray['controls'][index].patchValue(
            { boqUnitCode: details.displayUnit, quntity: details.quntity, rate: parseFloat(String(details.rate).replace(',', '')) })
        } else {
          this.headerArray['controls'][index].patchValue({ boqUnitCode: null, tenderQty: null })
        }
      }
    }
  }
  patchSingleTotal(index: any, index2: any) {
    let zero = 0;
    let amt: any = parseFloat(this.DetailsArray(index)['controls'][index2].value.constant) * parseFloat(this.DetailsArray(index)['controls'][index2].value.rate)
    if (amt) { this.DetailsArray(index)['controls'][index2].patchValue({ amount: amt.toFixed(2) }) } else { this.DetailsArray(index)['controls'][index2].patchValue({ amount: zero.toFixed(2) }) };
    this.patchAmountTotal();
  }
  async patchAmountTotal() {

    this.headerArrayFun().controls.forEach(async (element: any, index: any) => {
      let totmaterialAmt: number = 0;
      let totlabourAmt: number = 0;
      let totapplysupplyAmt: number = 0;
      let totAmt = 0;

      await element.controls.details.controls.forEach((element1: any) => {
        if (element1.controls.groupType.value == 'Material') {
          totmaterialAmt += Number(String(element1.value.amount).replace(',', ''));
          if (!totmaterialAmt) totmaterialAmt = 0;
        }
        else if (element1.controls.groupType.value == 'Labour') {
          totlabourAmt += Number(String(element1.value.amount).replace(',', ''));
          if (!totlabourAmt) totlabourAmt = 0;
        } else if (element1.controls.groupType.value == 'Apply Supply') {
          totapplysupplyAmt += Number(String(element1.value.amount).replace(',', ''));
          if (!totapplysupplyAmt) totapplysupplyAmt = 0;
        }
      });
      totAmt = totmaterialAmt + totlabourAmt + totapplysupplyAmt;
      if (!totAmt) totAmt = 0;
      this.headerArrayFun().controls[index].patchValue({ materialAmt: totmaterialAmt.toFixed(2), labourAmt: totlabourAmt.toFixed(2), applysupplyAmt: totapplysupplyAmt.toFixed(2), totalAmt: totAmt.toFixed(2) })
    });
  }

  async patchMasterRate(event: any, groupType: any, index: any, index2: any) {
    if (event) {

      var found = null
      if (groupType == 'Material') {
        found = await this.MaterialList.find((item: any) => { if (item.materialDetailID == event) { return item } })
      }
      else if (groupType == 'Labour') {
        found = await this.LabourList.find((item: any) => { if (item.labourDetailsID == event) { return item } })
      }
      else if (groupType == 'Apply Supply') {
        found = await this.ApplySupplyList.find((item: any) => { if (item.applySupplyDetailsID == event) { return item } })
      }
      if (found) {
        this.DetailsArray(index)['controls'][index2].patchValue({ rate: found.rate, unitCode: found.standardrUnitCode });
        this.patchSingleTotal(index, index2);
      }
      else { this.commonService.showError("Master not having rate !") }
    }
  }

  async getMasterRates(cmpCode: any, plantCode: any, projCode: any, event: any, index: any, index2: any) {
    if (event == 'Material') {
      this.MaterialList = await this.commomFunction.getRateMasters(cmpCode, plantCode, projCode, 'MaterialRates');
    } else if (event == 'Labour') {
      this.LabourList = await this.commomFunction.getRateMasters(cmpCode, plantCode, projCode, 'LabourRates');
    } else if (event == 'Apply Supply') {
      this.ApplySupplyList = await this.commomFunction.getRateMasters(cmpCode, plantCode, projCode, 'ApplySupplyRates');
    }
  }

  async AddRateAnalysis() {
    this.isActiveTab = 1;
    if (this.prestartestimation.invalid) {
      this.prestartestimation.markAllAsTouched()
      return;
    }
    var Json: any = await this.createJDON(this.prestartestimation.value);
    var datas: any = { data: Json }

    this.apiCallService.addRateAnalysis(datas).subscribe({
      next: (result: any) => {
        if (result) {

          console.log(Json[0], Json[0].cmpCode)
          this.OnChangeCompany(Json[0].cmpCode);
          this.OnChangePlant(Json[0].cmpCode, Json[0].plantCode);
          this.OnChangeProject(Json[0].cmpCode, Json[0].plantCode, Json[0].projCode);

          this.tabIsActive(0, 0);
          this.prestartestimationView.patchValue({
            cmpCode: Json[0].cmpCode,
            plantCode: Json[0].plantCode,
            projCode: Json[0].projCode
          });
          if (this.isCreate == 1) {
            this.commonService.showSuccess("Rate Analysis updated successfully!");
          }
          else {
            this.commonService.showSuccess("Rate Analysis inserted successfully!");
          }
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }

  createJDON(json: any) {

    let arr: any = []

    return new Promise(async (resolve, reject) => {
      json.header.forEach(async (element: any) => {
        await element.details.forEach(async (element2: any, index: any) => {

          var MaterialDetailID = null;
          var LabourDetailsID = null;
          var ApplySupplyDetailsID = null;
          if (element2.groupType == 'Material') { MaterialDetailID = element2.materialDetailID }
          else if (element2.groupType == 'Labour') { LabourDetailsID = element2.labourDetailsID }
          else if (element2.groupType == 'Apply Supply') { ApplySupplyDetailsID = element2.applySupplyDetailsID }
          await arr.push({
            cmpCode: json.cmpCode,
            plantCode: json.plantCode,
            projCode: json.projCode,
            actGroupId: element.actGroupId,
            actSeqNo: element.actSeqNo,
            groupType: element2.groupType,
            unitCode: element2.unitCode,
            constant: Number(Number(element2.constant).toFixed(2)),
            MaterialDetailID: MaterialDetailID,
            LabourDetailsID: LabourDetailsID,
            ApplySupplyDetailsID: ApplySupplyDetailsID
          })
        });
      });
      resolve(arr)
    })
  }
  sendToApproval(row: any) {

    var head = { cmpCode: row.cmpCode, plantCode: row.plantCode, projCode: row.projCode }
    var data = { head: head }

    this.apiCallService.sendToApproval(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("RateAnalysis send for approval successfully!");
          this.OnChangeProject(row.cmpCode, row.plantCode, row.projCode)
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })

  }
  async editView(row: any, iseditView: any) {
    if (this.headerArrayFun().length > 0) {
      while (this.headerArrayFun().length !== 0) {
        <FormGroup>this.headerArrayFun().removeAt(0)
      }
    }

    // get require details
    this.updateRow = row;
    this.MaterialList = await this.commomFunction.getRateMasters(row.cmpCode, row.plantCode, row.projCode, 'MaterialRates');
    this.LabourList = await this.commomFunction.getRateMasters(row.cmpCode, row.plantCode, row.projCode, 'LabourRates');
    this.ApplySupplyList = await this.commomFunction.getRateMasters(row.cmpCode, row.plantCode, row.projCode, 'ApplySupplyRates');
    this.showSub = true;
    this.BOQList = await this.commomFunction.getBOQ(row.cmpCode, row.plantCode, row.projCode, false);
    this.BOQList = this.BOQList[0]

    this.tabIsActive(1, 1);
    // Add array and controls
    this.prestartestimation.patchValue({
      projCode: row.projCode,
      cmpCode: row.cmpCode,
      plantCode: row.plantCode
    })
    await row.header.forEach(async (element: any, index: any) => {
      this.addHeadArray();
      this.OnChangeBOQHead(element.actGroupId, index);
      this.OnChangeBOQSubHead(element.actGroupId, index);
      (<FormGroup>this.headerArray.at(index)).patchValue({
        actGroupId: element.actGroupId,
        actSeqNo: element.actSeqNo,
        boqUnitCode: element.boqUnitCode,
        quntity: element.quntity,
        rate: element.rate
      });

      await element.details.forEach(async (element1: any, index2: any) => {
        this.addDetailsArray(index);
        if (element1.groupType == 'Material') {
          this.patchMasterRate(element1.materialDetailID, element1.groupType, index, index2);
        }
        else if (element1.groupType == 'Labour') {
          this.patchMasterRate(element1.labourDetailsID, element1.groupType, index, index2);
        }
        else if (element1.groupType == 'Apply Supply') {
          this.patchMasterRate(element1.applySupplyDetailsID, element1.groupType, index, index2);
        }

        (<FormGroup>this.DetailsArray(index).at(index2)).patchValue({
          groupType: element1.groupType,
          constant: element1.constant,
          labourDetailsID: element1.labourDetailsID,
          materialDetailID: element1.materialDetailID,
          applySupplyDetailsID: element1.applySupplyDetailsID,
          unitCode: element1.unitCode
        });
      })
    });
    this.patchAmountTotal();


    if (iseditView == 'view') { this.prestartestimation.disable(); this.isCreate = 2; } else { this.prestartestimation.enable(); }

  }

  deleteRateAnalysis(row: any) {

    var param = { cmpCode: row.cmpCode, plantCode: row.plantCode, projCode: row.projCode }
    var data = { param: param }

    this.apiCallService.deleteRateAnalysis(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("RateAnalysis deleted successfully!");
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
  deleteRateAnalysisHead(row: any) {

    var head = { cmpCode: row.cmpCode, plantCode: row.plantCode, projCode: row.projCode, actGroupId: row.actGroupId, actSeqNo: row.actSeqNo }
    var data = { head: head }

    this.apiCallService.deleteRateAnalysisHesdSeq(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("RateAnalysis Head Sequence deleted successfully!");
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
  deleteRateAnalysisHeadDetails(row: any) {

    var param = { cmpCode: row.cmpCode, plantCode: row.plantCode, projCode: row.projCode, actGroupId: row.actGroupId, actSeqNo: row.actSeqNo, Type: row.Type, ID: row.ID }
    var data = { param: param }

    this.apiCallService.deleteRateAnalysis(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("RateAnalysis Head Sequence deleted successfully!!");
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
  async prestartestimationViews() {
    if (this.prestartestimationView.invalid) {
      return;
    }

  }
  downloadUploadRA() {
    var arr = []

    for (var i = 0; i < this.BOQList.length; i++) {
      var dt: any = this.BOQList[i]
      arr.push({ SR_NO: dt.actGroupId, SeqNO: "", subSeqNO: "", Description: dt.actGroupDesc, Qty: "", Unit: "", Rate: "", Amount: "", GroupType: "", MaterialIDLabourIDApplySupplyIDMesclinous: "", SubUnit: "", Constant: "", SubRate: "", NewTenderQty: "" });
      for (var j = 0; j < dt.details.length; j++) {

        arr.push({
          SR_NO: "", SeqNO: dt.details[j].actSeqNo, subSeqNO: "", Description: dt.details[j].actDesc, Qty: "", Unit: "", Rate: "", Amount: "", GroupType: "", MaterialIDLabourIDApplySupplyIDMesclinous: "", SubUnit: "", Constant: "", SubRate: "", NewTenderQty: ""
        })
        // for (var k = 1, kLen = 4; k < kLen; k++) {
        //   arr.push({
        //     SR_NO: "", SeqNO: k, subSeqNO: "", Description: "", Qty: "", Unit: "", Rate: "", Amount: ""
        //   })


        for (var M = 1, MLen = 4; M < MLen; M++) {
          var dd
          if (M == 1) { dd = 'a' } else if (M == 2) { dd = 'b' } else if (M == 3) { dd = 'c' } else if (M == 4) { dd = 'd' }
          arr.push({
            SR_NO: "", SeqNO: "", subSeqNO: dd, Description: "", Qty: "", Unit: "", Rate: "", Amount: "", GroupType: "", MaterialIDLabourIDApplySupplyIDMesclinous: "", SubUnit: "", Constant: "", SubRate: "", NewTenderQty: ""
          })
        }


        // }
      }


    }

    var headersArray = ['ActGroupId', 'ActSeqNo', 'DisplaySequence', 'ActDesc', 'Quntity', 'UnitCode', 'Rate', 'Amount', 'Group Type', 'MaterialID / LabourID / ApplySupplyID / Miscellaneous', 'Unit', 'Constant', 'Rate', 'New Tender Qty'];

    this.JsonToXlsxService.exportExcelWithoutHead(arr, headersArray, "Rate Analysis")
  }
  download(row: any) {

    var head = ['Numbers', 'ActGroupId', 'ActSeqNo', 'DisplaySequence', 'ActDesc', 'Quntity', 'UnitCode', 'Rate', 'Amount', 'Group Type', 'MaterialID / LabourID / ApplySupplyID / Miscellaneous', 'Unit', 'Constant', 'Rate', 'New Tender Qty']
    var arr = []
    for (var i = 0; i < row.header.length; i++) {
      var dt: any = row.header[i]
      if (dt.actGroupId || dt.actGroupDesc) {

        arr.push({ SR_NO: dt.actGroupId, SeqNO: "", subSeqNO: "", Description: dt.actGroupDesc, Qty: "", Unit: "", Rate: "", Amount: "" });
        for (var k = 1, kLen = 4; k < kLen; k++) {
          arr.push({
            SR_NO: "", SeqNO: k, subSeqNO: "", Description: "", Qty: "", Unit: "", Rate: "", Amount: ""
          })
          for (var L = 1, LLen = 3; L < LLen; L++) {
            var dd
            if (L == 1) { dd = 'a' } else if (L == 2) { dd = 'b' } else if (L == 3) { dd = 'c' }
            arr.push({
              SR_NO: "", SeqNO: k, subSeqNO: dd, Description: "", Qty: "", Unit: "", Rate: "", Amount: ""
            })
          }
        }
      }
    }

    var headersArray = ['ActGroupId', 'ActSeqNo', 'DisplaySequence', 'ActDesc', 'Quntity', 'UnitCode', 'Rate', 'Amount'];

    this.JsonToXlsxService.exportExcelWithoutHead(arr, headersArray, "Rate Analysis")
  }
}

interface RateAnalysisData {
  cmpName: string,
  plantName: string,
  projName: string
}