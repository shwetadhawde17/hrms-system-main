import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../app/Shared/material.module';
import { ApiCallService } from '../services/api-call.service';
import { CommonService } from '../services/common.service';
import { NavbarComponent } from './Shared/navbar/navbar.component';
import { FlexLayoutModule } from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ComponentsComponent } from './components.component';
import { ComponentsRoutingModule } from './components-routing.module';

import { NgxSpinnerModule } from 'ngx-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { IDCComponent } from './idc/idc.component';
import { WorkorderComponent } from './workorder/workorder.component';
import { DDSComponent } from './dds/dds.component';
import { EmployeedesignationComponent } from './employeedesignation/employeedesignation.component';
import { ReportsComponent } from './reports/reports.component';
import { TimetrackComponent } from './timetrack/timetrack.component';
import { AttendenceComponent } from './attendence/attendence.component';
import { LeaveComponent } from './leave/leave.component';
@NgModule({
  declarations: [ComponentsComponent, NavbarComponent, IDCComponent, WorkorderComponent, DDSComponent, EmployeedesignationComponent, ReportsComponent, TimetrackComponent, AttendenceComponent, LeaveComponent],
  imports: [
    MatDialogModule,
    HttpClientModule,
    CommonModule,
    ComponentsRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
  ],
  providers:[
    ApiCallService,
    CommonService
  ]
})
export class ComponentsModule { }
