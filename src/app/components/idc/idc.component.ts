import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective, FormControl, FormArray } from '@angular/forms';
import { ComponentsComponent } from '../components.component';
import { TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-idc',
  templateUrl: './idc.component.html',
  styleUrls: ['./idc.component.scss']
})
export class IDCComponent implements OnInit {
  isActiveTab: number = 0;
  updateRow: any;
  isCreate: number = 0;
  IDCmaster: FormGroup
  IDCViewmaster: FormGroup;
  modaldata: any;
  grouopindex: any;
  IDCDeailsDesc: any;
  UnitList: any;
  UnitMasterList: any;
  PlantList: any;
  ProjectList: any;
  IDCActivityIDList: any = [];
  IDCSubHeadIDList: any = [];
  idcDetailsList: any = [];
  IDCList: any = [];
  mastersRatesList: any = [];
  MaterialList: any = [];
  LabourList: any = [];
  ApplySupplyList: any = [];
  displayedColumns = ['action', 'cmpName', 'plantName', 'projName'];


  grouptypeList = ['Material', 'Labour', 'Apply Supply']
  dataSource!: MatTableDataSource<IDCDATA>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  @ViewChild('viewDialog') viewDialog!: TemplateRef<any>;
  CompanyList: any;
  constructor(private fb: FormBuilder, private commomFunction: ComponentsComponent, private spinnerService: NgxSpinnerService, private commonFunction: ComponentsComponent, private dialog: MatDialog, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.IDCmaster = this.fb.group({
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      projCode: [null, [Validators.required]],
      subHead: new FormArray([this.IDCActivityGroup()]),

    })
    this.IDCViewmaster = this.fb.group({
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      projCode: [null, [Validators.required]],
    })
  }
  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany();
  }


  async OnChangeCompany(cmpCode: any) {

    if (cmpCode) {
      this.PlantList = await this.commomFunction.getPlant(cmpCode);
      this.IDCActivityIDList = await this.commonFunction.getIDCActivity(cmpCode);

      this.UnitMasterList = await this.commomFunction.getunitMaster(cmpCode);
    } else {
      this.IDCmaster.controls['plantCode'].setValue(null); this.PlantList = [];
      this.IDCmaster.controls['projCode'].setValue(null); this.ProjectList = [];
      this.UnitMasterList = [];
    }
  }

  async onChangePlant(cmpCode: any, plantCode: any) {
    if (cmpCode && plantCode) {

      this.ProjectList = await this.commomFunction.getproject(cmpCode, plantCode)
      this.IDCList = await this.commomFunction.getIDCAnalysis(cmpCode, plantCode, null, true);

      if (this.IDCList.length > 0) {
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.IDCList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.IDCList = []; this.dataSource = new MatTableDataSource();
      }
    } else {
      this.IDCViewmaster.controls['projCode'].setValue(null);
      this.IDCmaster.controls['projCode'].setValue(null);
      this.IDCList = []; this.dataSource = new MatTableDataSource();
    }

  }

  async OnChangeProject(cmpCode: any, plantCode: any, projCode: any) {
    if (cmpCode && plantCode && projCode) {
      // Change is required here
      this.IDCList = await this.commomFunction.getIDCAnalysis(cmpCode, plantCode, projCode, false);

      if (this.IDCList.length > 0) {
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(this.IDCList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      } else {
        this.IDCList = []; this.dataSource = new MatTableDataSource();
      }
    } else {
      this.IDCList = []; this.dataSource = new MatTableDataSource();
    }
  }

  async onChangeActivityIDList(data: any, index: any) {
    var found = this.IDCActivityIDList.find((item: any) => { if (item.idcActivityID == data) { return item } })

    if (found) {

      // this.IDCSubHeadIDList = found.subHead;
      if (this.IDCSubHeadIDList[index]) {

        this.IDCSubHeadIDList[index] = await found.subHead;
      }
      else {
        await this.IDCSubHeadIDList.push(found.subHead);
      }

    }
  }
  async onChangeSubActivityIDList(data: any, index: any, index2: any) {
    var found = this.IDCSubHeadIDList[index].find((item: any) => { if (item.idcSubHeadID == data) { return item } })

    if (found) {
      // this.idcDetailsList = found.idcDetails;
      // this.IDCDeailsDesc = found.description;

      if (this.idcDetailsList[index]) {
        if (this.idcDetailsList[index][index2]) {

          this.idcDetailsList[index][index2] = await found.idcDetails;
        }
        else {
          await this.idcDetailsList[index].push(found.idcDetails);
        }
      }
      else {
        let json: any = [];
        await json.push(found.idcDetails);
        this.idcDetailsList.push(json);

      }

    } else {

    }

  }

  //get material Form Array
  IDCActivity(): any {
    return <FormArray>this.IDCmaster.get('subHead') as FormArray;
  }
  // add material Form Array
  addIDCActivity() {
    this.IDCActivity().push(this.IDCActivityGroup());
  }
  // remove material Form Array
  removeIDCActivity(index: number) {
    this.IDCActivity().removeAt(index);
  }
  //material Form group Array function
  IDCActivityGroup(): FormGroup {
    return this.fb.group({
      idcActivityID: [null, [Validators.required]],
      idcDetails: new FormArray([])
    });
  }

  //get Sub store Form Array
  IDCSubHead(index: number): any {
    return <FormArray>this.IDCActivity().at(index).get('idcDetails') as FormArray;
  }
  //add Sub store Form Array
  addIDCSubHead(index: number) {
    this.IDCSubHead(index).push(this.IDCSubHeadGroup());
  }
  //remove Sub store Form Array
  removeIDCSubHead(i: number, j: number) {
    this.IDCSubHead(0).removeAt(j);
  }
  //Sub store Form Array function
  private IDCSubHeadGroup(): FormGroup {
    return this.fb.group({
      idcActivityID: [null],
      idcSubHeadID: [null, [Validators.required]],
      idcDetailsID: [null, [Validators.required]],
      grouptype: [null, [Validators.required]],
      materialDetailID: [null],
      labourDetailsID: null,
      applySupplyDetailsID: null,
      standardrUnitCode: [null, [Validators.required]],
      quantity: null,
      days: null,
      months: null,
      rate: null
    })
  }
  // mat table datasource filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  tabIsActive(value: any, change: any) {

    if (value == 0 && change == 0) {
      this.IDCmaster.reset();
      this.IDCSubHeadIDList = [];
      this.idcDetailsList = []
    } else if (value == 1 && change == 0) {
      if (this.IDCActivity().length > 0) {
        while (this.IDCActivity().length !== 0) {
          <FormGroup>this.IDCActivity().removeAt(0)
        }
      }
      this.updateRow = null, value = 0;
    }
    // if (value != change) {
    // }
    this.isActiveTab = value;
    this.isCreate = value;

  }


  async addIDCMaster(val: any) {


    this.isActiveTab = 1;

    if (this.IDCmaster.invalid) {
      this.IDCmaster.markAllAsTouched()
      return;
    }
    let Api = this.apiCallService.addIDCAnalysis;
    var JSON: any = await this.convertJSON(this.IDCmaster.value);

    var data = { data: JSON }

    Api(data).subscribe({
      next: async (result: any) => {
        if (result) {

          this.commonService.showSuccess("Record Inserted Successfully!");
          this.tabIsActive(0, 0);

          this.IDCViewmaster.patchValue({ cmpCode: JSON[0].cmpCode, plantCode: JSON[0].plantCode, projCode: JSON[0].projCode });
          this.OnChangeProject(JSON[0].cmpCode, JSON[0].plantCode, JSON[0].projCode);

        }
        this.IDCmaster.reset();
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
  async convertJSON(data: any) {

    return new Promise(async (resolve, reject) => {
      if (data.subHead.length > 0) {
        var arr: any = []
        await data.subHead.forEach((element: any) => {
          if (element.idcDetails) {
            element.idcDetails.forEach(async (element2: any) => {
              if (element2.grouptype == "Labour") {
                element2.labourDetailsID = element2.labourDetailsID;
                element2.applySupplyDetailsID = null;
                element2.materialDetailID = null;
              } else if (element2.grouptype == "Apply Supply") {
                element2.applySupplyDetailsID = element2.applySupplyDetailsID;
                element2.materialDetailID = null;
                element2.labourDetailsID = null;
              } else if (element2.materialDetailID) {
                element2.materialDetailID = element2.materialDetailID;
                element2.applySupplyDetailsID = null;
                element2.labourDetailsID = null;
              }
              await arr.push({
                "cmpCode": data.cmpCode,
                "plantCode": data.plantCode,
                "projCode": data.projCode,
                "idcActivityID": element.idcActivityID,
                "idcSubHeadID": element2.idcSubHeadID,
                "grouptype": element2.grouptype,
                "idcDetailsID": element2.idcDetailsID,
                "materialDetailID": element2.materialDetailID,
                "labourDetailsID": element2.labourDetailsID,
                "applySupplyDetailsID": element2.applySupplyDetailsID,
                "standardrUnitCode": element2.standardrUnitCode,
                "quantity": Number(parseFloat(element2.quantity).toFixed(2)),
                "days": Number(parseFloat(element2.days).toFixed(2)),
                "months": Number(parseFloat(element2.months).toFixed(2)),
                "rate": Number(parseFloat(element2.rate).toFixed(2)),
              })
            })
          }
        });
        resolve(arr)
      } else {
        var res: any = []
        resolve(res);
      }
    })
  }

  async getMasterRates(cmpCode: any, plantCode: any, projCode: any, event: any, index: any, index2: any) {
    if (event == 'Material') {
      this.MaterialList = await this.commomFunction.getRateMasters(cmpCode, plantCode, projCode, 'MaterialRates');
    } else if (event == 'Labour') {
      this.LabourList = await this.commomFunction.getRateMasters(cmpCode, plantCode, projCode, 'LabourRates');
    } else if (event == 'Apply Supply') {
      this.ApplySupplyList = await this.commomFunction.getRateMasters(cmpCode, plantCode, projCode, 'ApplySupplyRates');
    }
  }

  async patchMasterRate(event: any, groupType: any, index: any, index2: any) {
    if (event) {

      var found = null
      if (groupType == 'Material') {
        found = await this.MaterialList.find((item: any) => { if (item.materialDetailID == event) { return item } })
      }
      else if (groupType == 'Labour') {
        found = await this.LabourList.find((item: any) => { if (item.labourDetailsID == event) { return item } })
      }
      else if (groupType == 'Apply Supply') {
        found = await this.ApplySupplyList.find((item: any) => { if (item.applySupplyDetailsID == event) { return item } })
      }

      if (found) {
        this.IDCSubHead(index)['controls'][index2].patchValue({ rate: found.rate, standardrUnitCode: found.standardrUnitCode });

      }
      else { this.commonService.showError("Master not having rate !") }
    }
  }


  async editView(row: any, iseditView: any) {

    if (this.IDCActivity().length > 0) {
      while (this.IDCActivity().length !== 0) {
        <FormGroup>this.IDCActivity().removeAt(0)
      }
    }

    // get require details
    this.updateRow = row;
    this.MaterialList = await this.commomFunction.getRateMasters(row.cmpCode, row.plantCode, row.projCode, 'MaterialRates');
    this.LabourList = await this.commomFunction.getRateMasters(row.cmpCode, row.plantCode, row.projCode, 'LabourRates');
    this.ApplySupplyList = await this.commomFunction.getRateMasters(row.cmpCode, row.plantCode, row.projCode, 'ApplySupplyRates');


    // Add array and controls
    this.IDCmaster.patchValue({
      projCode: row.projCode,
      cmpCode: row.cmpCode,
      plantCode: row.plantCode
    })
    await row.iDCHeads.forEach(async (element: any, index: any) => {
      this.addIDCActivity();
      this.onChangeActivityIDList(element.idcActivityID, index);
      (<FormGroup>this.IDCActivity().at(index)).patchValue({
        idcActivityID: element.idcActivityID
      });

      await element.subHeads.forEach(async (element1: any, index2: any) => {
        this.addIDCSubHead(index);
        this.onChangeSubActivityIDList(element1.idcSubHeadID, index, index2);
        if (element1.groupType == 'Material') {
          this.patchMasterRate(element1.materialDetailID, element1.groupType, index, index2);
        }
        else if (element1.groupType == 'Labour') {
          this.patchMasterRate(element1.labourDetailsID, element1.groupType, index, index2);
        }
        else if (element1.groupType == 'Apply Supply') {
          this.patchMasterRate(element1.applySupplyDetailsID, element1.groupType, index, index2);
        }

        (<FormGroup>this.IDCSubHead(index).at(index2)).patchValue({

          idcActivityID: element.idcActivityID,
          idcSubHeadID: element1.idcSubHeadID,
          idcDetailsID: element1.idcDetailsID,
          grouptype: element1.groupType,
          materialDetailID: element1.materialDetailID,
          labourDetailsID: element1.labourDetailsID,
          applySupplyDetailsID: element1.applySupplyDetailsID,
          standardrUnitCode: element1.standardrUnitCode,
          quantity: element1.quantity,
          days: element1.days,
          months: element1.months,
          rate: element1.rate
        });
      })
    });

    this.tabIsActive(1, 1);

    if (iseditView == 'view') { this.IDCmaster.disable(); this.isCreate = 2; } else { this.IDCmaster.enable(); }

  }


  sendToApproval(row: any) {

    var head = { cmpCode: row.cmpCode, plantCode: row.plantCode, projCode: row.projCode }
    var data = { head: head }

    this.apiCallService.sendToApprovalIDC(data).subscribe({
      next: (result: any) => {
        if (result) {
          this.commonService.showSuccess("IDC send for approval successfully!");
          this.OnChangeProject(row.cmpCode, row.plantCode, row.projCode)
        }
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })

  }


  async ADDIDCViewmaster() {
    if (this.IDCViewmaster.invalid) {
      return;
    }

  }
  editMasterData() { }

  deleteMaster() { }

  addmodalDescription() {

  }

}

export interface IDCDATA {
  cmpCode: string;
  cmpName: string;


  plantCode: string,
  projCode: string,
  idcActivityID: string,
  idcSubHeadID: string,
  idcDetailsID: string,
  standardrUnitCode: string,
  quantity: string,
  days: string,
  months: string,
  rate: string,

  plantName: string,
  projName: string
}
