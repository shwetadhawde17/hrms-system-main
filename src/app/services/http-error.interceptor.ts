import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
  } from '@angular/common/http';
import { Injectable } from '@angular/core';
  import { Observable, throwError } from 'rxjs';
  import { retry, catchError } from 'rxjs/operators';
  @Injectable()
  export class HttpErrorInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request)
        .pipe(
          retry(1),
          catchError((error: HttpErrorResponse) => {
            let errorMessage = '';
            if (error.error instanceof ErrorEvent) {
              // console.log("?>>>>>>>>>>>>>>>>>>>>>",error.error)
              // client-side error
              errorMessage = `Error: ${error.error.message}`;
            } else {
              console.log("?>>>>>>>>>>>>>>>>>>>>>",error.error)
              // server-side error
              errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            }
            window.alert(errorMessage);
            // console.log("?>>>>>>>>>>>>>>>>>>>>>",error.error)
            return throwError(error.error);
          })
        )
    }
  }