import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ComponentsComponent } from '../components.component';
import { Console } from 'console';

@Component({
  selector: 'app-dds',
  templateUrl: './dds.component.html',
  styleUrls: ['./dds.component.scss']
})
export class DDSComponent implements OnInit {
  isActiveTab = 0;
  isCreate = 0;
  CompanyList: any;
  PlantList: any;
  ProjectList: any;
  BOQList: any;
  CPNO: any;
  subCpList: any;
  subIDCList: any
  IDCList: any;
  subList: any;
  ABCList: any;
  isChecked: boolean = false;
  createDailyDuetySlip: FormGroup;
  constructor(private fb: FormBuilder, private commomFunction: ComponentsComponent) {
    this.createDailyDuetySlip = this.fb.group({
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      ProjCode: [null, [Validators.required]],
      actGroupId: [null, [Validators.required]],
      actSeqNo: [null, [Validators.required]],
      idcActivityID: [null, [Validators.required]],
      idcSubHeadID: [null, [Validators.required]],
      isChecked: false,

    })

  }
  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany()
  }

  async onchangeCmp(cmpCode: any) {
    if(cmpCode){
      this.PlantList = await this.commomFunction.getPlant(cmpCode)
    }else{
      this.createDailyDuetySlip.controls['plantCode'].setValue(null);
      this.createDailyDuetySlip.controls['ProjCode'].setValue(null)
    }
  }

  async onChangePlant(cmpCode: any, plantCode: any) {
if(plantCode){
  this.ProjectList = await this.commomFunction.getproject(cmpCode, plantCode);
}
    else{
      this.createDailyDuetySlip.controls['ProjCode'].setValue(null)
  
    }
  }

 

  async onChangeProject(cmpCode: any, palntCode: any, projCode: any) {
    this.BOQList = await this.commomFunction.getBOQ(cmpCode, palntCode, projCode, false);
    this.BOQList = this.BOQList[0];
    this.IDCList = await this.commomFunction.getIDCActivity(cmpCode);
    // this.IDCList = this.IDCList;
    console.log("????????????????????", this.IDCList)

  }

  async onChangeBOQHead(data: any) {
    let BOQSubList = await this.BOQList.find((item: any) => { if (item.actGroupId == data) { return item } })
    this.subCpList = BOQSubList.details
    // if (BOQSubList) { this.subCpList = BOQSubList.details;this.CPNO = BOQSubList.nActSeqNo;} else {this.subCpList = [];this.CPNO = null}
    //  let BOQSubListList = await this.BOQList.filter((element:any)=> e == element.actSeqNo);
    //   console.log("AAAAAAAAAAAA",BOQSubListList[0])
  }

  async onChangeIDCHead(data: any) {
    console.log("lllllllllllllllllll", this.IDCList)
    var IDCSubList = await this.IDCList.find((item: any) => { if (item.idcActivityID == data) { return item } })
    this.subList = IDCSubList.subHead;
    // if (IDCSubList) { this.subList = IDCSubList.subHead; this.CPNO = IDCSubList.idcSubHeadID; } else { this.subList = []; this.CPNO = null }
    // console.log('WWWWWW',this.subList);

  }
  activityHead() {
    this.isChecked = !this.isChecked;
    if (this.isChecked == !true) {
      // console.log("<<<<<<<<<<<<<<<<<<<<<", this.isChecked);
      this.createDailyDuetySlip.addControl('actGroupId', this.fb.control(null, [Validators.required]));
      this.createDailyDuetySlip.addControl('actSeqNo', this.fb.control(null, [Validators.required]))
      if (this.createDailyDuetySlip.controls.idcActivityID) { this.createDailyDuetySlip.removeControl('idcActivityID') }
      if (this.createDailyDuetySlip.controls.idcSubHeadID) { this.createDailyDuetySlip.removeControl('idcSubHeadID') }
    }

    else if (this.isChecked) {
      // console.log(">>>>>>>>>>>>>>>>>>>>", this.isChecked);
      this.createDailyDuetySlip.addControl('idcActivityID', this.fb.control(null, [Validators.required]));
      this.createDailyDuetySlip.addControl('idcSubHeadID', this.fb.control(null, [Validators.required]));
      if (this.createDailyDuetySlip.controls.actGroupId) { this.createDailyDuetySlip.removeControl('actGroupId'); }
      if (this.createDailyDuetySlip.controls.actSeqNo) { this.createDailyDuetySlip.removeControl('actSeqNo'); }

    }

  }
  tabIsActive(value: any, change: any) {

  }

  addDailyDuetySlip() {
    if (this.createDailyDuetySlip.invalid) {
      // this.submitted = true;
      this.createDailyDuetySlip.markAllAsTouched()
      return;
    }
    console.log("data", this.createDailyDuetySlip.value)
  }
}
