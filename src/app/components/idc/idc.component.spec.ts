import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IDCComponent } from './idc.component';

describe('IDCComponent', () => {
  let component: IDCComponent;
  let fixture: ComponentFixture<IDCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IDCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IDCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
