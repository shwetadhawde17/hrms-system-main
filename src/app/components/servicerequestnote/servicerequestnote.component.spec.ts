import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicerequestnoteComponent } from './servicerequestnote.component';

describe('ServicerequestnoteComponent', () => {
  let component: ServicerequestnoteComponent;
  let fixture: ComponentFixture<ServicerequestnoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicerequestnoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicerequestnoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
