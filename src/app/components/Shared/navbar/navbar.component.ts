import { Component, ViewChild, HostListener, EventEmitter, OnInit, Output } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import screenfull from 'screenfull';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  isExpanded = true;
  hide = true;
  menus: any;
  screenWidth?: number;
  private screenWidth$ = new BehaviorSubject<number>(window.innerWidth);

  menuList: any = []
  @ViewChild('sidenav') sidenav?: MatSidenav;
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.screenWidth$.next(event.target.innerWidth);
  }
  constructor(private router: Router) {
    this.menuList = [{
      "text": "Dashboard",
      "icon": "dashboard",
      "routerLink": "/components/dashboard"
    },

    {
      "text": "HR-Administration",
      "icon": "layers",
      "children": [{
        "text": 'Create Company',
        "routerLink": '/components/createcompany',
        "icon": 'business'
      },
      {
        "text": 'Create Client',
        "routerLink": '/components/createclient',
        "icon": 'bar_chart'
      },
      // {
      //   "text": 'Create Consultant',
      //   "routerLink": '/components/createconsultant',
      //   "icon": 'bar_chart'
      // },
      {
        "text": 'Create Employee',
        "routerLink": '/components/createemployee',
        "icon": 'person'
      },
      {
        "text": 'Create Project',
        "routerLink": '/components/projectcreation',
        "icon": 'dvr'
      },

      // {
      //   "text": 'Create BOQ',
      //   "routerLink": '/components/boqupload',
      //   "icon": 'bar_chart'
      // },
      // {
      //   "text": 'Project Rate Allocation',
      //   "routerLink": '/components/projectrateallocation',
      //   "icon": 'bar_chart'
      // },
      // {
      //   "text": 'Rate Analysis',
      //   "routerLink": '/components/rateanalysis',
      //   "icon": 'bar_chart'
      // },
      // {
      //   "text": 'IDC',
      //   "routerLink": '/components/idc',
      //   "icon": 'bar_chart'
      // },
      
      // {
      //   "text": 'Approver',
      //   "routerLink": '/components/approver',
      //   "icon": 'bar_chart'
      // },{
      //   "text": 'Vendor Creation',
      //   "routerLink": '/components/vendorcreation',
      //   "icon": 'bar_chart'
      // },{
      //   "text": 'Summary Of Labour',
      //   "routerLink": '/components/summaryoflabour',
      //   "icon": 'bar_chart'
      // }
    ]
    },
    //  {
    //   "text": "Labour Module",
    //   "icon": "inventory_2",
    //   "children": [
    //     {
    //       "text": 'Service Request Note',
    //       "routerLink": '/components/servicerequestnote',
    //       "icon": 'bar_chart'
    //     },
    //     {
    //       "text": 'Work Order',
    //       "routerLink": '/components/workorder',
    //       "icon": 'bar_chart'
    //     },
    //     {
    //       "text": 'Daily Duety Slip',
    //       "routerLink": '/components/dds',
    //       "icon": 'bar_chart'
    //     }

    //   ]
    // },
    {
      "text": "Masters",
      "icon": "inventory_2",
      "children": [
        {
          "text": 'Project Master',
          "routerLink": '/components/projectmaster',
          "icon": "bar_chart"

        },
        {
          "text": 'Data Master',
          "routerLink": '/components/datamaster',
          "icon": 'bar_chart'
        }]
    }]

  }

  ngOnInit(): void {
    this.screenWidth$.subscribe(width => {
      this.screenWidth = width;
    });
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
  toggleFullscreen() {
    if (screenfull.isEnabled) {
      screenfull.toggle();
      this.hide = !this.hide
    }
  }

}