import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PrestartestimationComponent } from './prestartestimation.component';

const routes: Routes = [{path:'',component:PrestartestimationComponent,children:[]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrestartestimationRoutingModule { }
