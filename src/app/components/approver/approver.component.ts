import { ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { JsonToXlsxService } from 'src/app/services/json-to-xlsx.service';
import { ComponentsComponent } from '../components.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-approver',
  templateUrl: './approver.component.html',
  styleUrls: ['./approver.component.scss']
})
export class ApproverComponent implements OnInit {
  approvalForm: FormGroup;
  remarkForm: FormGroup;
  ProjectList: any;
  convertedJSON: any = [];
  ActivityHead: any = [];
  BtnDisabled = 0;
  SelectedActivityHead: any = [];
  arr: any;
  UnitMasterList: any;
  updateRow: any;
  isActiveTab: any;
  isCreate: any;
  CompanyList: any;
  PlantList: any;
  BOQList: any = [];
  displayedColumns = ['action', 'cmpCode', 'plantCode', 'projCode'];
  displayedColumnss = ['action', 'cmpCode', 'cmpName', 'plantName', 'projName'];
  displayedColumnsview = ['actGroupId', 'actSeqNo', 'displaySequence', 'actDesc', 'quntity', 'displayUnit', 'rate', 'amount'];
  dataSource!: MatTableDataSource<BOQData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('viewDialog') viewDialog!: TemplateRef<any>;
  @ViewChild('RemarkDialog') RemarkDialog!: TemplateRef<any>;
  ApproveorReject: any;
  approveorrej: any;
  RateAnalysisList: any = [];
  ApproveorType: any;
  IDCList: any = [];
  tabIsActiveChar: string = 'Approve BOQ';
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef, {
      disableClose: true,
    });
  }

  openRemarkDialog(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef, {
      disableClose: true,
    });
  }

  viewDataSource: any = [];
  constructor(private changeDetectorRefs: ChangeDetectorRef, private dialog: MatDialog, private commomFunction: ComponentsComponent, private JsonToXlsxService: JsonToXlsxService, private cdRef: ChangeDetectorRef, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService, private spinnerService: NgxSpinnerService) {
    this.approvalForm = fb.group({
      cmpCode: [null, Validators.required],
      plantCode: [null, Validators.required],
      projCode: [null, Validators.required]
    })
    this.remarkForm = fb.group({
      remark: [null, Validators.required]
    })
  }

  async ngOnInit() {
    this.CompanyList = await this.commomFunction.getCompany();
  }

  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {

    this.tabIsActiveChar = value.tab.textLabel;
    if (value != change) {
      this.updateRow = null, value = 0;
    }
    this.isActiveTab = value;
    this.isCreate = value;
    this.IDCList = []
    this.RateAnalysisList = [];
    this.BOQList = []
    this.approvalForm.reset();
  }
  Approval() {

  }
  ApproveBOQ(data: any, ApproveorType: any) {
    this.ApproveorReject = 'Approve';
    this.ApproveorType = ApproveorType;
    this.openRemarkDialog(this.RemarkDialog);

    this.approveorrej = data;
  }
  DisApprove(data: any, ApproveorType: any) {
    this.ApproveorReject = 'DisApprove';
    this.ApproveorType = ApproveorType;
    this.openRemarkDialog(this.RemarkDialog);

    this.approveorrej = data;

  }
  SubmitArpprovalOrDisapproval() {
    let param;
    var Api = null;
    this.spinnerService.show();

    if (this.ApproveorType == 'BOQ') {
      if (this.ApproveorReject == 'Approve') {
        param = { Version: this.approveorrej.version, IsApproved: true, Msg: this.remarkForm.value.remark }
      }
      else {
        param = { Version: this.approveorrej.version, IsApproved: false, Msg: this.remarkForm.value.remark }
      }
      Api = this.apiCallService.updateBOQUpload

    }
    else if (this.ApproveorType == 'RateAnalysis') {
      if (this.ApproveorReject == 'Approve') {
        param = { IsAproved: true }
      }
      else {
        param = { IsAproved: false }
      }

      Api = this.apiCallService.updateRateAnalysis
    }
    else if (this.ApproveorType == 'IDCAnalysis') {
      if (this.ApproveorReject == 'Approve') {
        param = { IsAproved: true }
      }
      else {
        param = { IsAproved: false }
      }

      Api = this.apiCallService.updateIDCAnalysis

    }
    var data = { param: param, head: { cmpCode: this.approvalForm.value.cmpCode, plantCode: this.approvalForm.value.plantCode, projCode: this.approvalForm.value.projCode } };

    Api!(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.spinnerService.hide();
          this.commonService.showSuccess('Approved Successfully!')
          this.approvalForm.reset();
          this.BOQList = [];
          this.RateAnalysisList = [];
          this.IDCList = [];
        }
      }, error: (error: any) => {
        this.spinnerService.hide();
        this.commonService.showError(error);
      }
    })
  }
  async onChangeCompany(cmpCode: any) {
    if (cmpCode) {
      this.PlantList = await this.commomFunction.getPlant(cmpCode);
      this.UnitMasterList = await this.commomFunction.getunitMaster(cmpCode);
    } else {
      this.approvalForm.controls['plantCode'].setValue(null); this.PlantList = [];
      this.approvalForm.controls['projCode'].setValue(null); this.ProjectList = [];
      this.BOQList = [];
      this.dataSource = new MatTableDataSource();
    }
  }
  async onChangePlant() {
    if (this.approvalForm.controls.cmpCode.value && this.approvalForm.controls.plantCode.value) {
      this.ProjectList = await this.commomFunction.getproject(this.approvalForm.controls.cmpCode.value, this.approvalForm.controls.plantCode.value)

      if (this.ProjectList.length < 1) {
        this.approvalForm.controls['projCode'].setValue(null);
        this.ProjectList = [];
        this.RateAnalysisList = [];
        this.dataSource = new MatTableDataSource();
      }
    }
    else {
      this.approvalForm.controls['projCode'].setValue(null);
      this.ProjectList = [];
      this.BOQList = []
      this.dataSource = new MatTableDataSource();
    }
  }
  async onChangeProject(projCode: any) {
    // Approve Rate Analysis Approve BOQ
    if (this.approvalForm.valid) {
      this.spinnerService.show();
      if (this.tabIsActiveChar == 'Approve BOQ') {
        let response = await this.getBOQByID(this.approvalForm.value.cmpCode, this.approvalForm.value.plantCode, this.approvalForm.value.projCode);
        this.spinnerService.hide();
      } else if (this.tabIsActiveChar == 'Approve Rate Analysis') {
        this.RateAnalysisList = await this.commomFunction.getRateAnalysis(this.approvalForm.value.cmpCode, this.approvalForm.value.plantCode, this.approvalForm.value.projCode, true);

        if (this.RateAnalysisList.length > 0) {
          this.spinnerService.hide();
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.RateAnalysisList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        } else {
          this.spinnerService.hide();
          this.RateAnalysisList = [];
          this.dataSource = new MatTableDataSource();
        }
      } else if (this.tabIsActiveChar == 'IDC Analysis') {
        this.IDCList = await this.commomFunction.getIDCAnalysis(this.approvalForm.value.cmpCode, this.approvalForm.value.plantCode, this.approvalForm.value.projCode, true);
        console.log(this.IDCList)
        if (this.IDCList.length > 0) {
          this.spinnerService.hide();
          // Assign the data to the data source for the table to render
          this.dataSource = new MatTableDataSource(this.IDCList);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        } else {
          this.spinnerService.hide();
          this.IDCList = [];
          this.dataSource = new MatTableDataSource();
        }
      }
    } else {
      this.spinnerService.hide();
      this.RateAnalysisList = [];
      this.IDCList = [];
      this.BOQList = []
      this.dataSource = new MatTableDataSource();
    }

  }


  async getBOQByID(cmpCode: any, plantCode: any, projCode: any) {

    this.BOQList = []
    this.spinnerService.show();

    if (cmpCode && plantCode && projCode) {
      this.BOQList = await this.commomFunction.getBOQ(cmpCode, plantCode, projCode, true);

      var NewBOQList: any = await this.commonService.getFilterBOQApprover(this.BOQList)

      if (NewBOQList.length > 0) {
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(NewBOQList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide();
      } else {
        this.BOQList = [];
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.spinnerService.hide();
      }
    } else {
      this.spinnerService.hide();
    }
  }

  async downloadUplodedView(row: any) {
    this.spinnerService.show();
    var find: any = await this.BOQList.map((item: any) => { if (item[0].cmpCode == row.cmpCode && item[0].plantCode == row.plantCode && item[0].projCode == row.projCode) { return item } });
    if (find.length > 0) {
      var finalList: any = await this.commonService.getExcelJSONList(find[0]);

      if (finalList.length > 0) {
        var headersArray = ['Numbers', 'ActGroupId', 'ActSeqNo', 'DisplaySequence', 'ActDesc', 'UnitCode', 'Quntity', 'Rate', 'Amount'];

        var JSONList = [{ company: "" }, { subheader: "Bill of Quantities & Schedule of Rates" }, { subtitle: "" }]
        this.JsonToXlsxService.exportExcel(finalList, headersArray, JSONList, "BOQ");
        this.spinnerService.hide();
      } else { this.spinnerService.hide(); }
    } else { this.spinnerService.hide(); }
  }

  // view BOQ Details
  async view(row: any) {
    this.spinnerService.show();
    this.BtnDisabled = 1;
    var find: any = await this.BOQList.map((item: any) => { if (item[0].cmpCode == row.cmpCode && item[0].plantCode == row.plantCode && item[0].projCode == row.projCode) { return item } });
    if (find.length > 0) {
      var finalList: any = await this.commonService.getExcelJSONList(find[0]);

      if (finalList.length > 0) {
        // Assign the data to the data source for the table to render
        this.viewDataSource = finalList;
        this.dataSource.sort = this.sort;
        this.openDialogWithTemplateRef(this.viewDialog);
        this.BtnDisabled = 0;
        this.spinnerService.hide();
      }
    } else {
      this.spinnerService.hide();
      this.BtnDisabled = 0;
    }
  }



}

export interface BOQData {
  cmpCode: string,
  plantCode: string,
  projCode: string
}
