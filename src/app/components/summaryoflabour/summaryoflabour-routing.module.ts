import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SummaryoflabourComponent } from './summaryoflabour.component';

const routes: Routes = [{path:'',component:SummaryoflabourComponent,children:[]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SummaryoflabourRoutingModule { }
