import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DatamasterComponent } from './datamaster.component';

const routes: Routes = [{path:'',component:DatamasterComponent,children:[]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatamasterRoutingModule { }
