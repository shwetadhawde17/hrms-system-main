import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MatDialog } from '@angular/material/dialog';

import { NgxSpinnerService } from 'ngx-spinner';
import { ApiCallService } from 'src/app/services/api-call.service';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import { mobileValidation, EmailValidation, ZipValidation, IFSCValidation, panValidation,Accountnumber } from '../../services/validations';
import { ComponentsComponent } from '../components.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-createemployee',
  templateUrl: './createemployee.component.html',
  styleUrls: ['./createemployee.component.scss'],
  providers: [ComponentsComponent],
})
export class CreateemployeeComponent implements OnInit {
  createEmployee: FormGroup;
  submitted: any;
  CompanyList: any;
  PlantList: any;
  CountryList: any;
  StateList: any;
  isActiveTab: number = 0;
  selectedFiles?: FileList;
  selectedFileNames: any;
  isCreate: number = 0;
  updateRow: any = [];
  displayedColumns = ['action', 'cmpCode', 'bpFName', 'bpMName', 'bpLName', 'pan', 'phone', 'email'];
  previews: any;
  dataSource!: MatTableDataSource<BPEMPData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  EmployeeList: any;
  DesignationList: any;
  ViewTableEmp:FormGroup;
  DepartmentList: any;
  deleteRow: any;
  AccountTypeList = [{ "type": "Savings Account", "value": "SA" }, { "type": "Salary Account", "value": "SC" }]
  GenderList = [{ "ID": 0, "Name": "Male" }, { "ID": 0, "Name": "Female" }, { "ID": 0, "Name": "Others" }]
  bloodGroupList = ["O +Ve", "O -Ve", "A +Ve", "A -Ve", "B +Ve", "B -Ve", "AB +Ve", "AB -Ve"]

  constructor(private spinnerService: NgxSpinnerService, private commomFunction: ComponentsComponent, private dialog: MatDialog, private fb: FormBuilder, private apiCallService: ApiCallService, private commonService: CommonService) {
    this.createEmployee = this.fb.group({
      bpCode: [null],
      cmpCode: [null, [Validators.required]],
      plantCode: [null, [Validators.required]],
      actGroupCode: ["EMP"],
      bpFName: [null, [Validators.required,Validators.maxLength(50)]],
      bpMName: [null, [Validators.required,Validators.maxLength(35)]],
      bpLName: [null, [Validators.required,Validators.maxLength(35)]],
      pan: [null, [Validators.required,panValidation]],
      addressLine1: [null,Validators.maxLength(50)],
      addressLine2: [null,Validators.maxLength(50)],
      street: [null, [Validators.required,Validators.maxLength(45)]],
      city: [null, [Validators.required,Validators.maxLength(45)]],
      postalCode: [null, [Validators.required, ZipValidation]],
      email: [null, [Validators.required, EmailValidation]],
      phone: [null, [Validators.required, mobileValidation]],
      stateCode: [null, [Validators.required]],
      countryCode: [null, [Validators.required]],
      status: true,
      createBy: [null],
      createOn: [null],
      updateBy: [null],
      updateOn: [null],
      isDeleted: [null],
      relationPersons: [null],
      details: new FormArray([this.detailsGroup()]),
      Banks: new FormArray([this.addBanksGroup()])
    })
    this.ViewTableEmp=this.fb.group({ cmpCode: [null, [Validators.required]],})
  }
  async ngOnInit() {
    document.body.style.backgroundColor = "#E5E7E9";
    this.CompanyList = await this.commomFunction.getCompany();
    this.CountryList = await this.commomFunction.getCountry();
    this.spinnerService.hide();
  }
  public myError = (controlName: string, errorName: string) => {
    return this.createEmployee.controls[controlName].hasError(errorName);
  }
  // Mat Dialogs.
  @ViewChild('deleteDialog') deleteDialog!: TemplateRef<any>;
  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }
  // Active Deactive matTab change event for Add method or update method
  tabIsActive(value: any, change: any) {
    console.log(value, change);
    if (value != change) {
      this.removefile();
      this.updateRow = null, value = 0
      this.createEmployee.reset();
      while (this.BanksGroupArr.length !== 1) {
        this.BanksGroupArr.removeAt(0)
      }
    }
    this.isActiveTab = value;
    this.isCreate = value;
    this.createEmployee.enable();
  }
  // Table view filter
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  // ADD detail Array 
  get detailsArray(): any {
    return <FormArray>this.createEmployee.get('details') as FormArray;
  }
  public myErrorDetailsArray = (controlName: string, errorName: string, i: any) => {
    return this.detailsArray['controls'][i]['controls'][controlName].hasError(errorName)
  }
  private detailsGroup(): FormGroup {
    return this.fb.group({
      BpCode: [null],
      cmpCode: [null],
      plantCode: [null],
      empGender: [null, [Validators.required]],
      bloodGroup: [null, [Validators.required]],
      emergencyContact: [null, [Validators.required,mobileValidation]],
      dateOfBirth: [null, [Validators.required]],
      employeeStatus: 1,
      education: [null, [Validators.required,Validators.maxLength(25)]],
      employeePhoto: [null],
      descCode: [null, [Validators.required]],
      deptCode: [null, [Validators.required]],
    })
  }
  // Bank Array
  get BanksGroupArr(): any {
    return <FormArray>this.createEmployee.get('Banks') as FormArray;
  }
  public myErrorArray = (controlName: string, errorName: string, i: any) => {
    return this.BanksGroupArr['controls'][i]['controls'][controlName].hasError(errorName)
  }
  private addBanksGroup(): FormGroup {
    return this.fb.group({
      cmpCode: null,
      BpCode: null,
      plantCode: null,
      BankName: [null, [Validators.required,Validators.maxLength(25)]],
      IFSE: [null, [Validators.required, IFSCValidation]],
      AccountType: [null, [Validators.required]],
      MICR: null,
      AccountNumber: [null, [Validators.required,Accountnumber]],
      BranchAddress: [null, [Validators.required,Validators.maxLength(50)]],
      Status: 1 
    })
  }
  // Add Banks Array
  addBanks() {
    this.BanksGroupArr.push(this.addBanksGroup());
  }
  //Delete Form Array
  deletearr(i: any) {
    this.BanksGroupArr.removeAt(i);
  }
  // Create Employee select company to plant get
  async onChangeCmp(data: any) {
    this.PlantList = await this.commomFunction.getPlant(data);
    this.DepartmentList = await this.commomFunction.getDepartmentById(data);
    this.DesignationList = await this.commomFunction.getDesignationById(data);
    
    if (!this.PlantList.length){
       this.createEmployee.controls['plantCode'].setValue(null); 
  }
  }
  // Create Employee select Country to get State
  async onChangeCountry(data: any) {
    this.StateList = await this.commomFunction.getState(data);
  }
  // Add Employee
  addEmployee(row:any) {
    this.isActiveTab = 1;
    
    if (this.createEmployee.invalid) {
      this.submitted = true;
      this.createEmployee.markAllAsTouched()
      return;
    }
    this.createEmployee.patchValue({
      actGroupCode:'EMP',
      status: true
    })
    this.detailsArray.value.forEach((element: any, index: any) => {
      
      (<FormGroup>this.detailsArray.at(index)).patchValue({
        cmpCode: this.createEmployee.value.cmpCode,
        plantCode: this.createEmployee.value.plantCode,
        BpCode: this.createEmployee.value.bpCode,
      });
    });
    this.BanksGroupArr.value.forEach((element: any, index: any) => {
      
      (<FormGroup>this.BanksGroupArr.at(index)).patchValue({
        cmpCode: this.createEmployee.value.cmpCode,
        plantCode: this.createEmployee.value.plantCode,
        BpCode: this.createEmployee.value.bpCode,
        Status: true
      })
    })
    
    // 
    var JSON = this.createEmployee.getRawValue()
    let Api: any = this.apiCallService.addEmployee;
    JSON.createBy = environment.createBy; // Loged IN User ID
    JSON.updateBy = environment.updateBy; // Loged IN User ID
    JSON.details[0].employeePhoto = btoa(this.previews);
    JSON.details = JSON.details[0]
    
    var head = {}
    if (this.isCreate == 1){
       Api = this.apiCallService.updateEmployee, head = { cmpCode: this.updateRow.cmpCode, bpCode: this.updateRow.bpCode } 
    }
    var data = { data: JSON, head: head }
    
    Api(data).subscribe({
      next: async (result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Inserted Succefully!");
          this.updateRow = []
          this.EmployeeList = await this.commomFunction.getEmployee(JSON.cmpCode); 
          this.tabIsActive(0, 0);
          this.removefile();
          if (this.EmployeeList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.EmployeeList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
          else {
            this.EmployeeList = [];
          }
        }
        this.createEmployee.reset();
      }, error: (error: any) => {
        this.commonService.showError(error);
      }
    })
  }
  // update employee
  async editEmployee(row: any, isEdit: string) {
    this.updateRow = row;
    if (this.BanksGroupArr.length > 0) {
      while (this.BanksGroupArr.length !== 0) {
        this.BanksGroupArr.removeAt(0)
      }
    }
    this.PlantList = await this.commomFunction.getPlant(this.updateRow.cmpCode);
    this.StateList = await this.commomFunction.getState(this.updateRow.countryCode);
    this.DepartmentList = await this.commomFunction.getDepartmentById(this.updateRow.cmpCode);
    this.DesignationList = await this.commomFunction.getDesignationById(this.updateRow.cmpCode);
    
    this.createEmployee.patchValue({      
      bpCode: this.updateRow.bpCode,
      cmpCode: this.updateRow.cmpCode,
      plantCode: this.updateRow.plantCode,
      actGroupCode: this.updateRow.actGroupCode,
      bpFName: this.updateRow.bpFName,
      bpMName: this.updateRow.bpMName,
      bpLName: this.updateRow.bpLName,
      pan: this.updateRow.pan,
      addressLine1: this.updateRow.addressLine1,
      addressLine2: this.updateRow.addressLine2,
      street: this.updateRow.street,
      city: this.updateRow.city,
      postalCode: this.updateRow.postalCode,
      email: this.updateRow.email,
      phone: this.updateRow.phone,
      stateCode: this.updateRow.stateCode,
      countryCode: this.updateRow.countryCode,
    });
    
    if (this.updateRow.details) {
      if (this.updateRow.details.employeePhoto) { this.previews = atob(this.updateRow.details.employeePhoto); this.selectedFileNames = "Logo.png"; this.updateRow.details.employeePhoto = null }
      (<FormGroup>this.detailsArray.at(0)).patchValue({
        bpCode: this.updateRow.details.bpCode,
        cmpCode: this.updateRow.details.cmpCode,
        plantCode: this.updateRow.details.plantCode,
        empGender: this.updateRow.details.empGender,
        bloodGroup: this.updateRow.details.bloodGroup,
        emergencyContact: this.updateRow.details.emergencyContact,
        dateOfBirth: this.updateRow.details.dateOfBirth,
        employeeStatus: this.updateRow.details.employeeStatus,
        education: this.updateRow.details.education,
        employeePhoto: this.updateRow.details.employeePhoto,
        descCode: this.updateRow.details.descCode,
        deptCode: this.updateRow.details.deptCode,
      });
    }
    if (this.updateRow.banks.length > 0) {
      this.updateRow.banks.map((item: any, index: any) => {
        this.addBanks();
        (<FormGroup>this.BanksGroupArr.at(index)).patchValue({
          cmpCode: this.createEmployee.value.cmpCode,
          plantCode: this.createEmployee.value.plantCode,
          BpCode: this.createEmployee.value.bpCode,
          Status: true,
          BankName: item.bankName,
          IFSE: item.ifse,
          AccountType: item.accountType,
          MICR: item.micr,
          AccountNumber: item.accountNumber,
          BranchAddress: item.branchAddress,
        })
      })
    }
    this.tabIsActive(1, 1);
    if (isEdit == 'edit') { 
      this.createEmployee.enable(); 
    } 
    else { 
      this.createEmployee.disable();
       this.isCreate = 2; 
    }
  }

 
  
  // open dialog box
  deleteMe(row: any): void {
    this.deleteRow = row;
    this.openDialogWithTemplateRef(this.deleteDialog);
  }
  // confirm delete
  deleteEmployee() {
    var head = { cmpCode: this.deleteRow.cmpCode, bpCode: this.deleteRow.bpCode }
    var data = { head: head, param: null }
    this.apiCallService.deleteBusinessPartner(data).subscribe({
      next: async(result: any) => {
        if (result) {
          this.commonService.showSuccess("Record Deleted Succefully!");
          // const index = this.CompanyList.indexOf(result);
          // if (index > -1) { // only splice array when item is found
          //   this.CompanyList.splice(index, 1); // 2nd parameter means remove one item only
          // }
          this.EmployeeList = await this.commomFunction.getEmployee(this.deleteRow.cmpCode); 
          this.tabIsActive(0, 0);
          this.removefile();
          if (this.EmployeeList.length > 0) {
            // Assign the data to the data source for the table to render
            this.dataSource = new MatTableDataSource(this.EmployeeList);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
        }
      }, error: (error: any) => {       
          this.commonService.showError(error);      
      }
    })
  }
  // up img
  selectFiles(event: any): void {
    this.selectedFileNames = null;
    this.previews = null;
    this.selectedFiles = event.target.files;
    if (this.selectedFiles && this.selectedFiles[0]) {
      const numberOfFiles = this.selectedFiles.length;
      for (let i = 0; i < numberOfFiles; i++) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          this.previews = e.target.result;
        };
        // Convert file to base64
        reader.readAsDataURL(this.selectedFiles[i]);
        this.selectedFileNames = this.selectedFiles[i].name;
      }
    }
  }
  removefile() {
    this.selectedFileNames = null;
    this.previews = null;
  }
  // view employee  select company name view table employee
  async onChangeCompany(data: any) {
    this.spinnerService.show();
    this.EmployeeList = await this.commomFunction.getEmployee(data); //get Employee
    if (this.EmployeeList.length > 0) {
      // Assign the data to the data source for the table to render
      this.dataSource = new MatTableDataSource(this.EmployeeList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinnerService.hide();
    }
    else {
      this.EmployeeList = [];
      this.spinnerService.hide();
    }
  }
}
export interface BPEMPData {
  cmpCode: string,
  bpFName: string,
  bpMName: string,
  bpLName: string,
  pan: string,
  phone: string,
  email: string,
}



