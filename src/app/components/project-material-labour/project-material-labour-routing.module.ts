import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectMaterialLabourComponent } from './project-material-labour.component';

const routes: Routes = [{path:'',component:ProjectMaterialLabourComponent,children:[]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectMaterialLabourRoutingModule { }
